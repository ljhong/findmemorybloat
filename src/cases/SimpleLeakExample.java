package cases;

import java.util.Vector;
import java.util.Date;


public class SimpleLeakExample {
	static Vector<Object> v=new Vector<Object>(100);
	Object outerO=new SimpleObject();
	public static void main(String[] args) {
		SimpleLeakExample s=new SimpleLeakExample();
//		s.foo(2);
//		s.bar(1);
//		s.ter(3);
//		s.saur(1);
		s.fun(1);
		s.fun1(3);
	}
	
	public void foo(int x){
		for(int i=0;i<x;i++){
			Object o=new SimpleObject();
			SimpleLeakExample.v.add(o);
		}
		System.out.println();
	}
	
	public void bar(int x){
		for(int i=0;i<x;i++){
			Object n = new SimpleObject();
			n.hashCode();
		}
	}
	
	public void ter(int x){  //此测试用例会在方法级被判断出对象溢出
		for(int i=0;i<x;i++){
			Object t=new SimpleObject();
			outerO=t;
		}
	}
	
	public Object saur(int x){  
		//该方法不添加return返回时，在处理时，无方法溢出，
		//同时也没有方法内的循环溢出，在方法类判断循环溢出时，找循环的内部变量应该是出现问题的
		Object methO=new SimpleObject();
		for(int i=0;i<x;i++){
			Object sa=new SimpleObject();
			methO=sa;
		}
		return methO;
	}
	
	public void fun(int x){
		long sum=0;
		for(int i=0;i<x;i++){
			sum=sum+i;		
		}

		for(long j=0;j<sum;j++){
			Object o=new SimpleObject();
			v.add(o);
		}
	}
	
	public void fun1(int x){
		for(int i=0;i<x;i++){
			v.add(SimpleLeakExample.a());
		}
	}
	
	static SimpleObject a(){
		SimpleObject o = new SimpleObject();
		return o;
	}
}
