package cases;

public class SimpleObject {
	String s;
	int n;
	
	SimpleObject(){
	}
	public SimpleObject(String s){
		this.s = s;
		this.n = 0;
	}
	SimpleObject(String s,int n){
		this.s = s;
		this.n = n;
	}
	
	public String getS() {
		return s;
	}
	public void setS(String s) {
		this.s = s;
	}	



}
