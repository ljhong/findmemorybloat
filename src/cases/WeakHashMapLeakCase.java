package cases;
import java.util.HashMap;
import java.util.Map;
import java.util.WeakHashMap;
public class WeakHashMapLeakCase {

		
		private static Map cache = new WeakHashMap();
		
		public static void foo(String s,int n){
			if(n>0){
			
			    SimpleObject o = new SimpleObject(s);
			
			    cache.put(s, o);
			}

			
		}
		
		
		public static void main(String []args){
//			for (int i = 0; i< 10000; i ++){
//				String s = String.valueOf(i);
			String s = "abc";
		//	SimpleObject s = new SimpleObject();
				foo(s,5);				
//			}
		}

}
