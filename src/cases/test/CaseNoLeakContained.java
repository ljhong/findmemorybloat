package cases.test;

import java.util.Map;
import java.util.WeakHashMap;

import test.cases.AnotherObject;
import test.cases.SimpleObject;

public class CaseNoLeakContained {
	private static Map<SimpleObject, SimpleObject> cache = new WeakHashMap<SimpleObject, SimpleObject>();
	public static void main(String []args){
		zar();
		fun();
		noleakf();
	}
	public static void zar(){
		SimpleObject keyObj = new SimpleObject("Case3");
		SimpleObject valueObj = new AnotherObject(keyObj,5).getO();
		cache.put(keyObj, valueObj);
	}
	public static void fun(){
		SimpleObject keyObj = new SimpleObject("Case3");		
		AnotherObject o = new AnotherObject();
		SimpleObject valueObj = o.getValueObj1(keyObj);	
//		AnotherObject o = new AnotherObject(keyObj,5);
//		SimpleObject valueObj = o.getValueObj2();	
		cache.put(keyObj, valueObj);	
	}
	public static void noleakf(){
		Map<SimpleObject, SimpleObject> c = new WeakHashMap<SimpleObject, SimpleObject>();
		SimpleObject keyObj = new SimpleObject("000",1);
		SimpleObject valueObj = new SimpleObject("111",3);
		c.put(keyObj, valueObj);
	}
}
