package jqian.leakfinder.core;

import java.io.File;

import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import jqian.testing.MemoryTester;

import soot.DoubleType;
import soot.IntType;
import soot.RefLikeType;
import soot.RefType;
import soot.SootClass;
import soot.SootMethod;
import soot.Type;
import soot.jimple.toolkits.typing.fast.BottomType;

public class GenerateJunitTest {
	
	public static void generateJunitTests(Collection<SootMethod> methods) throws IOException{	
		
		Set<SootMethod> sets = new HashSet<SootMethod>();
		Set<String> methodNames = new HashSet<String>();
		int methodNum = 0;  //对相同方法名的method做标记
		
		for(SootMethod m:methods){
			
			boolean mark = false;
			
			if(!sets.contains(m)){
				
				String methodName = m.getName();
				String testMethodName = methodName;
				
				if(methodNames.contains(methodName)){
					methodNum++;
					testMethodName = testMethodName + methodNum;	
				}
				
				methodNames.add(testMethodName);
				
				//判断下method所属类中构造方法是否为public，若为private或protected型则不能直接创建该类的对象
				SootClass cls=m.getDeclaringClass();
				Collection<SootMethod> constructionMethods = new ArrayList<SootMethod>();
				constructionMethods = cls.getMethods();
				for(SootMethod method:constructionMethods){
					String s=method.getName();
					if(s.equals("<init>")&&(method.isPrivate()))
						mark = true;
					if(s.equals("<init>")&&(method.isProtected()))
						mark = true;
					if(s.equals("<init>")&&(method.isPublic())){
						mark = false;
						break;
					}
				}
				if(mark == true && !(m.isStatic())){
					
				}
				else{
					
				
				
				String clsN=cls.getName();
				
				int index=clsN.lastIndexOf("."); //获取类名
				String clsName=clsN.substring(index+1);
				String testClsName=clsName+"Test";
				
//				String fileName="./src/"+clsN.replace('.', '/')+"Test.java";
				String fileName="./src/test/casesTest/"+testClsName+".java";
				
//				System.out.println(clsN);			
//				System.out.println(clsName);	
//				System.out.println(testClsName);
//				System.out.println(fileName);
				
				File file = new File(fileName);
				if(file.exists()){
					//文件已存在，插入对该方法的测试				
						FileWriter fw = new FileWriter(fileName,true);
						String meth = methodCode(mark,testMethodName,methodName,clsN,m,clsName);
						fw.write(meth);
						fw.close();
						sets.add(m);				
					continue;
				}
				
				if(file.createNewFile()){
					//创建文件，并插入对该方法的测试					
					FileWriter fw = new FileWriter(fileName,true);					
					String outer = outerCode(testClsName,clsName);
					fw.append(outer);
					fw.flush();					
					String meth = methodCode(mark,testMethodName,methodName,clsN,m,clsName);
					fw.append(meth);
					fw.flush();
					fw.close();
					sets.add(m);
				}
			}
		  }
			
		}
		
		Set set=new HashSet();
		for(SootMethod m:methods){
			SootClass cls=m.getDeclaringClass();
			String clsN=cls.getName();
			if(!set.contains(cls)){
				String methodName = m.getName();			
				int index=clsN.lastIndexOf("."); //获取类名
				String clsName=clsN.substring(index+1);
				String fileName="./src/test/casesTest/"+clsName+"Test.java";
				FileWriter fw = new FileWriter(fileName,true);
				String s="}";
				fw.append(s);
				fw.flush();
				fw.close();
				set.add(cls);
			}		
		}
			
	}	
	
	protected static String methodCode(boolean mark,String testMethodName,String methodName,String clsName,SootMethod m,String className){
		
		//若是get方法，默认给该方法加上循环，以更大地显露出它的泄漏特性

		
		String code="";
		
		code = code+"\t@Test\n\tpublic void test"+testMethodName+"(){\n\n";
		
		if(className.equals("String")){
			code = code + "\t\tlong freeMemoryBefore=MemoryTester.getUsedMemory();\n\n";
			code = code + "\t\tString longstr = MemoryTester.getlongstring();\n";
			code = code + "\t\tString usestr;\n\n";
			code = code + "\t\tusestr = longstr."+ methodName + "(";
			
			int paraNum = m.getParameterCount();
			String testFunCode = "";
			
			for(int i = 0; i < paraNum; i++){
				if(i != 0)
					testFunCode += ",";
				Type para = m.getParameterType(i);
				if(para instanceof RefLikeType){
					testFunCode += "null";
				}
				else if(para instanceof IntType){
					testFunCode += "MemoryTester.RandomParaNum";
				}
				else if(para instanceof DoubleType){
					testFunCode += "MemoryTester.RandomParaNum";
				}			
			}
			
			code = code + testFunCode +");\n";
			code = code + "\t\tlongstr = null;\n\n";
			code += "\t\tlong freeMemoryAfter=MemoryTester.getUsedMemory();";
			code += "\t\tlong usedMemory=freeMemoryBefore-freeMemoryAfter;";
			code = code + "\t\tMemoryTester.assertMemoryConsumation(\""+className+"\",\""+testMethodName+"\",usedMemory);\n\t}\n\n";
			
			
		}
		else if(className.contains("OutputStream")){
			//这里的代码需要修改，仅针对了这种特殊情况
			code = code + "\t\ttry {\n";
			code = code + "\t\t\tOutputStream outstream = new java.io.FileOutputStream(\"flow\");\n";
			code = code + "\t\t\tObjectOutputStream out = new ObjectOutputStream(outstream);\n\n";
			code = code + "\t\t\tData o = new Data();\n";
			code = code + "\t\t\tWeakReference<Data> ref = new WeakReference<Data>(o);\n";
			code = code + "\t\t\tout.writeUnshared(o);\no = null;\n\n";
			code = code + "\t\t\tif(ref.get()!=null)\n";
			code = code + "\t\t\t\tMemoryTester.assertMemoryLeak(\"writeUnshared\");\n";
			code = code + "\t\t\telse\n";
			code = code + "\t\t\t\tMemoryTester.assertNoMemoryLeak(\"writeUnshared\");\n";
			code = code + "\t\t} catch (FileNotFoundException ex) {\n";
			code = code + "\t\t\tex.printStackTrace();\n";
			code = code + "\t\t} catch (IOException ex) {\n";
			code = code + "\t\t\tex.printStackTrace();\n\t\t}\n\t}\n\n";
					
		}
		
		else{
			if(mark == false)
				code = code+"\t\t"+clsName+" test=new "+clsName+"();\n\n";
			code += "\t\tlong freeMemoryBefore=MemoryTester.getUsedMemory();\n";		
			//判断测试的方法的参数为何种类型
			int paraNum = m.getParameterCount();
			String testFunCode = null;
			if(mark == false)
				testFunCode = "\t\ttest." + methodName + "(";
			else
				testFunCode = "\t\t" + clsName + "." + methodName + "(";
		
			for(int i = 0; i < paraNum; i++){
				if(i != 0)
					testFunCode += ",";
				Type para = m.getParameterType(i);
				if(para instanceof RefLikeType){
					testFunCode += "null";
				}
				else if(para instanceof IntType){
					testFunCode += "MemoryTester.LARGE_INT";
				}
				else if(para instanceof DoubleType){
					testFunCode += "MemoryTester.LARGE_LONG";
				}
				
			}
			testFunCode += ");\n";
			
			//若是get方法，默认给该方法加上循环，以更大地显露出它的泄漏特性	
			if(methodName.substring(0, 3).equals("get")){
				code = code + "\t\tfor(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)\n";
				code = code + "\t" + testFunCode;
//				code = code + "\t\t\ttest." + methodName+"();\n";
			}
			else
				code = code + testFunCode;
//				code = code +"\t\t"+ "test." + methodName + "(MemoryTester.LARGE_LONG);\n";
		
		
			code += "\t\tlong freeMemoryAfter=MemoryTester.getUsedMemory();\n";
			code += "\t\tlong usedMemory=freeMemoryBefore-freeMemoryAfter;\n\n";
			code = code + "\t\tMemoryTester.assertMemoryConsumation(\""+clsName+"\",\""+testMethodName+"\",usedMemory);\n\t}\n\n";
		}
		return code;
	}
	
	protected static String outerCode(String testClsName,String clsName){
		String code="";
		code+="package test.casesTest;\n\n";
		code+="import jqian.testing.MemoryTester;\n\n";
		code+="import org.junit.Before;\nimport org.junit.Test;\n\n";
		if(clsName.contains("OutputStream")){
			code = code + "import java.io.*;\nimport java.lang.ref.WeakReference;\n\n";
			code = code + "class Data implements Serializable{\n";
			code = code + "\tint d=0;\n}\n\n";			
		}
			
		code = code + "public class "+testClsName+" {\n\n";
		code+="\t@Before\n\tpublic void setUp() throws Exception {\n\t}\n\n";
		return code;
	}


}
