package jqian.leakfinder.core;

import java.util.*;
import java.io.*;


import soot.*;
import soot.jimple.ClassConstant;
import soot.jimple.InstanceInvokeExpr;
import soot.jimple.InvokeExpr;
import soot.jimple.InvokeStmt;
import soot.jimple.Stmt;
import soot.jimple.toolkits.callgraph.CallGraph;
import soot.toolkits.graph.UnitGraph;
import soot.util.Chain;

import test.PtsToTester;
import test.SDGUtil;
import test.Test;

import jqian.sootex.Cache;
import jqian.sootex.dependency.pdg.DepGraphOptions;
import jqian.sootex.dependency.pdg.DependenceEdge;
import jqian.sootex.dependency.pdg.DependenceNode;
import jqian.sootex.dependency.pdg.FormalIn;
import jqian.sootex.dependency.pdg.SDG;
import jqian.sootex.dependency.pdg.PDG;
import jqian.sootex.location.AccessPath;
import jqian.sootex.location.HeapAbstraction;
import jqian.sootex.location.InstanceObject;
import jqian.sootex.location.Location;

import jqian.sootex.ptsto.IPtsToQuery;
import jqian.sootex.ptsto.PtsToHelper;
import jqian.sootex.ptsto.SparkPtsToQuery;
import jqian.sootex.sideeffect.FastEscapeAnalysis;
import jqian.sootex.util.SootUtils;
import jqian.leakfinder.util.*;


/**
 * Core leakFinder engine
 * @author nuaa
 *
 */

public class LeakFinder {
//	FastEscapeAnalysis methodEscapeInfo;
	
	static Set<SootMethod> leakMethods = new HashSet<SootMethod>();
	
	//复制Soot代码，修改Loop类
	protected static Collection<Loop> getLoopContainedMedthod(Collection<SootMethod> methods){
		Collection<Loop> allLoops=new ArrayList<Loop>();
		
		LoopAnalysis la=new LoopAnalysis();
		for(SootMethod m: methods){			
			if(m.isConcrete()){
				Collection<Loop> loops=la.getLoopsContainedNew(m);
				if(loops!=null){
					allLoops.addAll(loops);
				}	
				
			}			
		}	
		return allLoops;
	}
	
	
	protected static Collection<Loop> getInputDependentLoop(Collection<Loop> loops,SDG sdg){
		Collection<Loop> inputDepLoops=new ArrayList<Loop>();
		
		for(Loop loop:loops){
//			SootMethod m=loop.getMethod();
			
			//create PDG for method m
			//TODO 后面还是用SDG，但是限制在单个方法内遍历，主要是要考虑SummaryEdge
//			PDG pdg=sdg.getPDG(m);
			
			if(isInputDepend(sdg,loop)){
				inputDepLoops.add(loop);
			}	
		}
			
		return inputDepLoops;
	}
	
	//添加了一个DependenceEdge是否已在栈中的判断
	protected static boolean isInputDepend(SDG sdg,Loop loop){
		boolean result=false;
		SootMethod m=loop.getMethod();
		PDG pdg=sdg.getPDG(m);
		if(pdg!=null){
		Stmt loopCondition=loop.getHead();
		
		DependenceNode node  = null;
		if(pdg.getStmtBindingNode(loopCondition)!=null)
		   node = pdg.getStmtBindingNode(loopCondition);
		Collection<DependenceEdge> edges=sdg.edgesInto(node);
		
		Set<DependenceEdge> results=new HashSet<DependenceEdge>();
		Stack<DependenceEdge> stack=new Stack<DependenceEdge>();
		Set<DependenceEdge> inStack=new HashSet<DependenceEdge>();
		
		stack.addAll(edges);
		inStack.addAll(edges);
		 
		
		while(!stack.isEmpty()){
			DependenceEdge top=stack.pop();
			inStack.remove(top);
			
			if (!results.add(top)) {
				continue;
			}
			
			DependenceNode d=top.getFrom();
			if(d instanceof FormalIn)
				result=true;
			else{
				Collection<DependenceEdge> nexts=sdg.edgesInto(d);
				if(nexts==null)
					continue;
				
				// TODO: 控制依赖的目标如果在任意一个循环中则继续，否则跳过
				for(DependenceEdge e:nexts){
					if(!results.contains(e)&&!inStack.contains(e)){
						stack.add(e);
						inStack.add(e);
					}
				}
			}
			if(result) break;
		}
		}
		
		return result;
	}
	
	
	protected static Collection<Loop> getNewObjectEscapedLoops(Collection<Loop> loops,FastEscapeAnalysis escape){
		Collection<Loop> results = new ArrayList<Loop>();;
		for(Loop loop: loops){
			LoopEscapeAnalysis loopEsc=new LoopEscapeAnalysis(loop,escape);
			loopEsc.build();
			boolean result=loopEsc.isLoopNewObjectEscaped();	
			if(result){
				results.add(loop);
			}
		}
		return results;
	}
	
	protected static Collection<SootMethod> getLoopMethods(Collection<Loop> instanceEscLoops){
		
		Collection<SootMethod> possiBloatMethods=new ArrayList<SootMethod>();
		for(Loop loop:instanceEscLoops){
			SootMethod m=loop.getMethod();
			possiBloatMethods.add(m);
		}
		return possiBloatMethods;
	}
	
	
	protected static Collection<SootMethod> getNewObjectContainedMethod(Collection<SootMethod> methods,Set<SootMethod> sets){
		
		Collection<SootMethod> newObjCOContainedMeths=new ArrayList<SootMethod>();
		
		for(SootMethod m: methods){			
			if(m.isConcrete()){
				//在这里判断方法内是否有new语句，存在则添加；若无new语句，但是有调用语句也添加
				if(MethodAnalysis.hasNewExpr(m)&&!sets.contains(m)){
					newObjCOContainedMeths.add(m);
				}		
			}			
		}			
		return newObjCOContainedMeths;
	}
	
	protected static Collection<SootMethod> getInstanceEscpMethod(Collection<SootMethod> methods,FastEscapeAnalysis escape){
		Collection<SootMethod> instanceEscpMethods=new ArrayList<SootMethod>();
		
		for(SootMethod m:methods){
			
			String methodName=m.getName();
			//添加了一个判断是否为构造函数的条件
//			if(!(methodName.equals("<init>"))&&m.isConcrete())	{
			if(m.isConcrete())	{
				if(MethodAnalysis.isInstanceEscp(m,escape,leakMethods)){
					//若存在溢出的方法为get方法且为public方法，则为其生成测试用例
					if(!(methodName.equals("<init>"))&&!(methodName.equals("main"))&&m.isPublic())
//					if(methodName.contains("get")&&m.isPublic())
						instanceEscpMethods.add(m);
					leakMethods.add(m);
					
				}	
			}
					
		}
		
		return instanceEscpMethods;
	}
	
	public static Collection<SootMethod> getPotentialBloatMethods(Collection<SootMethod> methods,SDG sdg,CallGraph cg){
		// Get SSA representations
		//Soot load shimple format
		
		//TODO 修改代码，在此处实现对FastEscapeAnalysis类的实例化
		FastEscapeAnalysis escape=new FastEscapeAnalysis(cg);
		escape.build();
		
		Collection<SootMethod> leakMethods=new ArrayList<SootMethod>();
		Set<SootMethod> sets = new HashSet<SootMethod>();
		
		Collection<Loop> loops=getLoopContainedMedthod(methods);
		Collection<Loop> inputDepLoops=getInputDependentLoop(loops,sdg);
		Collection<Loop> instanceEscLoops=getNewObjectEscapedLoops(inputDepLoops,escape);

		leakMethods=getLoopMethods(instanceEscLoops);
		sets.addAll(leakMethods);

		//添加由get的一些方法造成的泄漏

		Collection<SootMethod> newObjContainedMeth=getNewObjectContainedMethod(methods,sets);
		Collection<SootMethod> leakMethodofGet=getInstanceEscpMethod(newObjContainedMeth,escape);
		
		leakMethods.addAll(leakMethodofGet);
	
		return leakMethods;
	}
	
	//add at 14.10.02
	public static boolean hasIntersection(Collection<PointsToSet> keyOpts, PointsToSet valuepts){
		for(PointsToSet pts: keyOpts){
			if(pts.hasNonEmptyIntersection(valuepts))
				return true;
			if(pts.equals(valuepts))
				return true;
			if(pts == valuepts)
				return true;
		}
		return false;
	}
	public static boolean hasRef(Value keyO,Value valueO,SootMethod m,PointsToAnalysis ptsTo,IPtsToQuery pt2Query){
		
		//获得key值的指向集，并获得key对象内部的对象指向集
		Collection<PointsToSet> keyOpts = new ArrayList<PointsToSet>();
	    PointsToSet keypts = ptsTo.reachingObjects((Local)keyO);
	    if(keypts!=null)
	    	keyOpts.add(keypts);
	    Type keyType=((Local)keyO).getType();
	    
	    if(keyType instanceof RefLikeType){
	    	Location ptr = Location.valueToLocation((Local)keyO);
		 //   Set<InstanceObject> pt2Set = pt2Query.getPointTos(m, null,ptr);
		    if (keyType instanceof RefType) {
		    	SootClass cls = ((RefType) keyType).getSootClass();
	            Collection<SootField> fields = Cache.v().getAllInstanceFields(cls);
	            for (SootField f: fields) {
	            	PointsToSet pt = null;
	            	if (f.getType() instanceof RefLikeType) {                            
	            		AccessPath ap = AccessPath.getByRoot(ptr);
	            		ap = ap.appendFieldRef(f);
	            			
	            		Set<?> locs = PtsToHelper.getAccessedLocations(pt2Query, HeapAbstraction.FIELD_SENSITIVE, null, ap);
	            			//  Test.printCollection(locs.iterator(),",");
	            			
	            		pt = testFieldPtsTo(locs,pt2Query);    
	            		if(pt!=null)
	            			keyOpts.add(pt);
	            	}
	            }
	        }
	    } 
	    
	    //获得value值的指向集，以及value对象内部的对象指向集，从而判断是否存在value到key的指向
	    PointsToSet valuepts = ptsTo.reachingObjects((Local)valueO);
	    
	  //  boolean result = valuepts.hasNonEmptyIntersection(keypts);
	    
	    if(valuepts!=null && hasIntersection(keyOpts,valuepts)){
	    	return true;
	    }
	    Type valueType=((Local)valueO).getType();
	    if(valueType instanceof RefLikeType){
	    	Location ptr = Location.valueToLocation((Local)valueO);
	    	if(valueType instanceof RefType){
	    		SootClass cls = ((RefType) valueType).getSootClass();
	            Collection<SootField> fields = Cache.v().getAllInstanceFields(cls);
	            for (SootField f: fields) {
	            	PointsToSet pt = null;
	            	if (f.getType() instanceof RefLikeType) {                            
	            		AccessPath ap = AccessPath.getByRoot(ptr);
	            		ap = ap.appendFieldRef(f);            			
	            		Set<?> locs = PtsToHelper.getAccessedLocations(pt2Query, HeapAbstraction.FIELD_SENSITIVE, null, ap);
	            			
	            		pt = testFieldPtsTo(locs,pt2Query);  
	            		if(pt!=null && hasIntersection(keyOpts,pt))
	            			return true;
	            	}
	            }
	    	}
	    }
	    
	    return false;
	}

		
	public static boolean isLeakMethod(SootMethod m,PointsToAnalysis ptsTo,IPtsToQuery pt2Query){
		boolean result = false;
		Body body = m.getActiveBody();
		
		Collection <Value> weakHMLocs = new ArrayList<Value>();
		for(Local l : body.getLocals()){   //首先找到WeakHashMap型的变量
			Type type = l.getType();
			if(type instanceof RefLikeType){
				if(type.toString().equals("java.util.WeakHashMap")){//若声明的HashMap类，实例化时用WeakHashMap
					Value v = (Value)l;
					weakHMLocs.add(v);
				}
			}
		}
		
		//找出存入WeakHashMap型变量中的key,value变量
		for(Unit u: body.getUnits()){
			if(u instanceof InvokeStmt){
				InvokeExpr v = ((InvokeStmt) u).getInvokeExpr();
				if(v instanceof InstanceInvokeExpr){
					Value base = ((InstanceInvokeExpr) v).getBase();
					if(weakHMLocs.contains(base)){  //找出是WeakHashMap型对象的调用语句
						SootMethod invM= ((InstanceInvokeExpr) v).getMethod();
						
						if(invM.toString().equals  //若调用了WeakHashMap的put方法，则获取key和value值
								("<java.util.WeakHashMap: java.lang.Object put(java.lang.Object,java.lang.Object)>")){
							Value keyO = ((InstanceInvokeExpr) v).getArg(0);
							Value valueO = ((InstanceInvokeExpr) v).getArg(1);
							//判断value对象中是否存在对key的引用
							result = hasRef(keyO,valueO,m,ptsTo,pt2Query); 
						}
					}
				}
			}
		}
		
		return result;
	}
	private static PointsToSet testFieldPtsTo(Collection<?> locs,IPtsToQuery query){

		PointsToSet pt = null;
        for(Iterator<?> it=locs.iterator();it.hasNext();){
            Object obj=it.next();
            if(obj instanceof Location){
            	Location ptr = (Location)obj;
                if(ptr.isPointer()){                	
                	Set<InstanceObject> pt2Set = query.getPointTos(null, null,ptr);
                	pt = ((SparkPtsToQuery) query).getPointToset(ptr);
                } 
            }
        }
        return pt;
    }
	public static Collection<SootMethod> getWeakHashMapLeakMeths(Collection<SootMethod> methods,PointsToAnalysis ptsTo,IPtsToQuery query){
		Collection<SootMethod> leakMethods = new ArrayList<SootMethod>();
		for(SootMethod m: methods){
			if(m.isConcrete()){
				if(isLeakMethod(m,ptsTo,query))
					leakMethods.add(m);    		
			}
		}
		
		return leakMethods;
		
	}
	
    public static void main(String []args){
		
    	String _sdgEntry=null;
    	String mainClass=args[0];
//		String mainClass = "test.cases.SimpleLeakExample";
		
		Properties options = Test.loadConfig("/test/config.xml"); 
       	options.put("entry_class", mainClass);
       	
        //Get SSA representations
      	//Soot load Shimple format
       	Test.loadClasses(true);
       	
       	SootMethod entry=null;	 
		if(_sdgEntry!=null){
			entry = Scene.v().getMethod(_sdgEntry);
		}
		else{
			entry = (SootMethod)Scene.v().getEntryPoints().get(0);
		}
		
		Test.doFastSparkPointsToAnalysis();
       	Test.simplifyCallGraph();
       	
//      Collection<SootMethod> entries = new ArrayList<SootMethod>(1);
//		entries.add(entry);
	        
//		int methodNum = SootUtils.getMethodCount();        
	    CallGraph cg = Scene.v().getCallGraph();
       	
//       	HeapAbstraction locAbstraction = HeapAbstraction.FIELD_SENSITIVE;
	    HeapAbstraction locAbstraction = HeapAbstraction.FIELD_BASED;
    	DepGraphOptions pdgOptions = new DepGraphOptions(true, true, locAbstraction);
       	
       	SDG sdg= SDGUtil.constructSDG(entry,pdgOptions,true, 1);
       	sdg.buildSummaryEdges(entry);
		sdg.connectPDGs();
		sdg.compact();	
		
		// TODO 改成所有方法，而不只是可达的
		List<?> rm = Cache.v().getTopologicalOrder();	
		
	    //14.08.31 add for test
		//14.10.02 add for identifying memory leak of WeakHashMap
//	    PointsToAnalysis ptsTo = Scene.v().getPointsToAnalysis();
//	    Collection<SootMethod> methods = (Collection<SootMethod>)rm;
//	    IPtsToQuery query = new SparkPtsToQuery();
	    
//	    Collection<SootMethod> potentialWeakHashMapLeakMeths = getWeakHashMapLeakMeths(methods,ptsTo,query);
//	    System.out.println();
//	    System.out.println("泄漏的方法是：");
//	    for(SootMethod m:potentialWeakHashMapLeakMeths){
//	    	System.out.println(m.toString());
//	    }

	    
//  自动生成单元测试来找出Java的内存泄漏	    
		Collection<SootMethod> potentialMethods=getPotentialBloatMethods((Collection<SootMethod>)rm,sdg,cg);
		try {
			GenerateJunitTest.generateJunitTests(potentialMethods);
		} catch (IOException e) {
			e.printStackTrace();
		}

	    
    }	
}
