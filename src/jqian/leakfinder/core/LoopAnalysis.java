package jqian.leakfinder.core;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;


import soot.Body;
import soot.Local;
import soot.RefLikeType;
import soot.SootMethod;
import soot.Value;
import soot.ValueBox;
import soot.jimple.AnyNewExpr;
import soot.jimple.DefinitionStmt;
import soot.jimple.IfStmt;
import soot.jimple.InvokeExpr;
import soot.jimple.StaticInvokeExpr;
import soot.jimple.Stmt;
import soot.jimple.internal.JIfStmt;
import soot.jimple.internal.JLtExpr;
import soot.jimple.toolkits.callgraph.CallGraph;
import soot.toolkits.graph.MutableDirectedGraph;

import jqian.leakfinder.util.*;

public class LoopAnalysis {
	
	//TODO 如果有方法调用间接创建对象。FreshAnalysis或者递归扫描所有间接调用的代码
	protected boolean hasNewExpr(List<Stmt> loopStmts){  
		boolean result=false;
		for(Iterator<?> it=loopStmts.iterator();it.hasNext();){
			Stmt u=(Stmt)it.next();
			if(u instanceof DefinitionStmt){
				DefinitionStmt d=(DefinitionStmt)u;
				Value left=d.getLeftOp();
				Value right=d.getRightOp();
				if(left.getType() instanceof RefLikeType){
					if(right instanceof AnyNewExpr|| right instanceof StaticInvokeExpr||right instanceof InvokeExpr){
						result=true;
					}
				}
			}
			
		}
		
		return result;
	}
	
	
	public Collection<Loop> getLoopsContainedNew(SootMethod m){
		//首次考虑分析一个方法中有多个循环，但是循环间不存在嵌套
		Body body = m.retrieveActiveBody();		
		//find loop
		Map options = new HashMap();
		Collection<Loop> loops;Set<Loop> loopsContainNew=new HashSet<Loop>();
		LoopFinder loopFind=new LoopFinder();
		loopFind.parseLoops(body, options);
		loops=loopFind.getLoops();
		for (Iterator<?> it = loops.iterator(); it.hasNext();){
			Loop loop=(Loop)it.next();			
			List<Stmt> loopStatements=loop.getLoopStatements();
			boolean hasNewExpr=hasNewExpr(loopStatements);
			
			if(hasNewExpr){				
				loopsContainNew.add(loop);		
			}							
		}
		return loopsContainNew;		
	}

}
