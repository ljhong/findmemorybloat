/** 
 *  
 * 
 * 	@author Ju Qian {jqian@live.com}
 * 	@date 2011-8-8
 * 	@version
 */
package jqian.leakfinder.core;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import soot.Body;
import soot.Local;
import soot.RefLikeType;
import soot.SootMethod;
import soot.Type;
import soot.Unit;
import soot.Value;
import soot.jimple.AnyNewExpr;
import soot.jimple.CastExpr;
import soot.jimple.ConcreteRef;
import soot.jimple.Constant;
import soot.jimple.DefinitionStmt;
import soot.jimple.IdentityStmt;
import soot.jimple.InstanceInvokeExpr;
import soot.jimple.InvokeExpr;
import soot.jimple.InvokeStmt;
import soot.jimple.ReturnStmt;
import soot.jimple.StaticInvokeExpr;
import soot.jimple.Stmt;
import soot.jimple.ThrowStmt;
import soot.jimple.toolkits.callgraph.CallGraph;
import soot.jimple.toolkits.typing.fast.BottomType;
import soot.shimple.PhiExpr;
import soot.toolkits.graph.HashMutableDirectedGraph;
import soot.toolkits.graph.MutableDirectedGraph;

import jqian.Global;
import jqian.sootex.Cache;
import jqian.sootex.sideeffect.FastEscapeAnalysis;
import jqian.sootex.sideeffect.FreshAnalysis;
import jqian.sootex.util.SootUtils;
import jqian.sootex.util.callgraph.Callees;
import jqian.sootex.util.graph.GraphHelper;
import jqian.util.Utils;
import jqian.leakfinder.util.*;

/**
 * Implement the fresh method and variable analysis provided by Gay@CC 2000 A
 * fresh method return a newly created objects (not exist before method call).
 * NOTE that the object may already bean escaped or also return from parameters.
 * 
 */

public class LoopEscapeAnalysis  {
	private Loop loop;
//	private CallGraph cg;
	FastEscapeAnalysis escape;
	protected Set<Local> _vescaped;

	public LoopEscapeAnalysis(Loop loop,FastEscapeAnalysis escape) {
		 this.loop = loop;
//		 this.cg=cg;
		 this.escape=escape;
	}
	
	public boolean isLoopNewObjectEscaped(){
		// 1. which locals hold new objects?
//		Collection<Local> objectHolders =getLoopNewObjects();
		Collection<Local> objectHolders = getInnerLocals();
		// 2. are these locals escape from the loop
		for(Local local: objectHolders){
			// is method level escape?
			if(isMethodEscaped(local,escape)){
				return true;
			}
						
			if(_vescaped.contains(local)){
				return true;
			}
		}
		
		return false;
	}
	
//	private Collection<Local> getLoopNewObjects(){
//		Collection<Local> results=new ArrayList<Local>();
//		Collection<Stmt> loopStmts=loop.getLoopStatements();
//		for(Stmt s:loopStmts){
//			if(s instanceof DefinitionStmt){
//				DefinitionStmt d=(DefinitionStmt)s;
//				Value left=d.getLeftOp();
//				Value right=d.getRightOp();
//				if(left.getType() instanceof RefLikeType){
//					if(right instanceof AnyNewExpr){
//						Local l=(Local)left;
//						results.add(l);
//					}
//				}
//			}
//		}
//		
//		return results;
//	}
	
	private boolean isMethodEscaped(Local l,FastEscapeAnalysis escape){
//		//TODO
//		Collection<Local> escapedLocals=escape.getEscapedLocals(loop.getMethod());
//		if(escapedLocals.contains(l)){
//			result=true;
//		}	
//		
//		//TODO getReturnedLocals...
//		return result;
		
		//TODO 修改代码，FastEscapeAnalysis对象的实例化不应该放在这里
		boolean result=false;
		
//		FastEscapeAnalysis escape=new FastEscapeAnalysis(cg);
//		escape.build();
		
		Collection<Local> escapedLocals=escape.getEscapedLocals(loop.getMethod());
		if(escapedLocals.contains(l)){
			result=true;
		}	
		return result;

	}
	
	protected MutableDirectedGraph<Object> initVarConnectionGraph(SootMethod m){
		// build a variable connection graph
		HashMutableDirectedGraph varConn = new HashMutableDirectedGraph();
		Body body = m.retrieveActiveBody();
		
		varConn.addNode(Boolean.TRUE);
		varConn.addNode(Boolean.FALSE);
		
		for(Local l: body.getLocals()){
			Type t = l.getType();
			if(t instanceof RefLikeType){
				varConn.addNode(l);
			}
			else if(t instanceof BottomType){
				varConn.addNode(l);
			}
		}
		return varConn;
	} 

	//TODO 方法调用暂未考虑
	protected void escapedAnalysis(Collection<Local> outsideLocals) {
		Set<Local> returns = new HashSet<Local>();

		SootMethod m=loop.getMethod();
		// build a constraint graph
		MutableDirectedGraph<Object> varConn = initVarConnectionGraph(m);
		
		for(Local outsideVar: outsideLocals){
			varConn.addEdge(Boolean.TRUE, outsideVar);
		}
		
		Collection<Stmt> loopStmts=loop.getLoopStatements();
		for(Unit u:loopStmts){
			if (u instanceof ReturnStmt) {
				Value v = ((ReturnStmt) u).getOp();
				if (v.getType() instanceof RefLikeType && v instanceof Local) {
					//returns.add((Local) v);
					varConn.addEdge(Boolean.TRUE, v);
				}
			}  else if (u instanceof ThrowStmt) {
				Value t = ((ThrowStmt) u).getOp();
				varConn.addEdge(Boolean.TRUE, t);
			} else if (u instanceof DefinitionStmt) {
				DefinitionStmt d = (DefinitionStmt) u;
				Value left = d.getLeftOp();
				Value right = d.getRightOp();

				// assignment on non-reference variables
				if (!(left.getType() instanceof RefLikeType)) {
				} 
				else if (left instanceof Local) {
					// 1. l = @param, l = @this
					if (d instanceof IdentityStmt) {
					}
					// 2. "l = new C", "l = constant"
					else if (right instanceof AnyNewExpr
							|| right instanceof Constant) {
					}
					// 3. l = r
					else if (right instanceof Local) {
						varConn.addEdge(left, right);
					}
					// 4. l = (cast)r
					else if (right instanceof CastExpr) {
						CastExpr cast = (CastExpr) right;
						Value op = cast.getOp();
						if (op instanceof Local) {
							varConn.addEdge(left, op);
						}
					}
					// 5. l = r.f, l = r[], l = g
					else if (right instanceof ConcreteRef) {
					} 
					else if (right instanceof InvokeExpr) {
						//TODO
//						handleMethodCallForEscape(varConn, left, right, u);
					}
					// phiExpr
					else if (right instanceof PhiExpr) {
						List<Value> arg=((PhiExpr) right).getValues();
					
						for(Value value:arg){
							varConn.addEdge(left, value);
						}
						//throw new RuntimeException("PhiExpr processing has not implemented");
					} 
					//loop escape
					//else if(outsideLocals.contains(left)&&innerLocals.contains(right)){
					//	varConn.addEdge(Boolean.TRUE, right);
					//}
					else {
						// 2012.2.11 tao modified
						// throw new RuntimeException();
					}
					//
					
				}
				// 6. g = r, l.f = r, l[] = r 
				else if (left instanceof ConcreteRef) {
					if (right instanceof Local) {
						varConn.addEdge(Boolean.TRUE, right);
					}
				}
				// others
				else {
					throw new RuntimeException();
				}
			} else if (u instanceof InvokeStmt) {
				//TODO
//				handleMethodCallForEscape(varConn, null, ((InvokeStmt) u).getInvokeExpr(), u);
			}
			
			
		}
		
		// solve constraints
		Set escaped = GraphHelper.getReachables(varConn, Boolean.TRUE);
		_vescaped = escaped;
	}

	public void build() {

		// 1. Inner locals, Outside Locals
//		Collection<Local> innerLocals=getInnerLocals();
		Collection<Local> outsideLocals=getOutsideLocals();
		
		// outside -> escaped		 
		escapedAnalysis(outsideLocals);
		
	}
	
	private Collection<Local> getOutsideLocals(){
		Collection<Local> results=new ArrayList<Local>();
		Body body=loop.getMethod().getActiveBody();
		Stmt head=loop.getHead();
		int lineNumFrom=SootUtils.getLine(head);
		int lineNumTo=0;		
		for(Stmt s:loop.getLoopStmtsWithoutBackJumpStmts()){
			lineNumTo=SootUtils.getLine(s);
		}
		for(Unit u:body.getUnits()){
			if(u instanceof DefinitionStmt){
				int lineNum=SootUtils.getLine(u);
				if(lineNum<lineNumFrom||lineNum>lineNumTo){ //处理循环外的语句
					DefinitionStmt d = (DefinitionStmt) u;
					Value left = d.getLeftOp();
					Value right = d.getRightOp();
					
					if(!(left.getType() instanceof RefLikeType)){					
					}
					else if(left instanceof Local){
						
						if(d instanceof IdentityStmt){// 1. l = @param, l = @this
							results.add((Local) left);
						} 
						else if(right instanceof AnyNewExpr||right instanceof Constant){
							// 2. "l = new C", "l = constant"
							results.add((Local) left);
						}
						else if(right instanceof Local){  // 3. l = r
							results.add((Local) left);
						}
						else if(right instanceof CastExpr){  // 4. l = (cast)r
							results.add((Local) left);
						}
						else if(right instanceof ConcreteRef){  // 5. l = r.f, l = r[], l = g
							results.add((Local) left);
						}
						else if(right instanceof InvokeExpr){
							results.add((Local) left);
						}
						else{							
						}
					}
					else{						
					}
				}				
			}		
		}
		
		return results;
	}
	
	private Collection<Local> getInnerLocals(){
		Collection<Local> results=new ArrayList<Local>();
		Collection<Stmt> loopStmts=loop.getLoopStatements();
		for(Unit u:loopStmts){
			if (u instanceof DefinitionStmt){
				DefinitionStmt d = (DefinitionStmt) u;
				Value left = d.getLeftOp();
				Value right = d.getRightOp();
				if (!(left.getType() instanceof RefLikeType)) {
				} 
				else if (left instanceof Local) {
					//Loop内部的变量，判断Loop内部变量时考虑的情况不全
					// 1. "l = new C", "l = constant"
					if (right instanceof AnyNewExpr
							|| right instanceof Constant) {
						results.add((Local)left);
					}
					// 2. l = r
					else if (right instanceof Local) {
						results.add((Local)left);
					}
					// 3. 引用型 
					else if(right instanceof StaticInvokeExpr|| right instanceof InvokeExpr){
						results.add((Local)left);
					}
				}
			}
		}
		return results;
	}


	
}
