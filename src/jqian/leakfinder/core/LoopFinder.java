package jqian.leakfinder.core;
import java.util.Map;
import java.util.HashSet;
import java.util.Collection;
import java.lang.String;
import soot.Body;
import jqian.leakfinder.util.*;

public class LoopFinder extends jqian.leakfinder.util.LoopFinder {
	
	public void parseLoops(Body b,Map options){
		internalTransform(b,"",options);
	}
	
	public Collection<Loop> getLoops(){
		return loops();
	}
}
