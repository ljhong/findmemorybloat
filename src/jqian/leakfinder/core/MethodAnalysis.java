package jqian.leakfinder.core;

import java.util.Collection;
import java.util.Set;

import jqian.sootex.sideeffect.FastEscapeAnalysis;

import soot.Body;
import soot.Local;
import soot.RefLikeType;
import soot.Scene;
import soot.SootClass;
import soot.SootMethod;
import soot.Type;
import soot.Unit;
import soot.Value;
import soot.jimple.AnyNewExpr;
import soot.jimple.DefinitionStmt;
import soot.jimple.IdentityStmt;
import soot.jimple.InvokeExpr;
import soot.jimple.InvokeStmt;
import soot.jimple.toolkits.callgraph.CallGraph;

public class MethodAnalysis {
	
	public static boolean hasNewExpr(SootMethod m){
		
		Body body=m.retrieveActiveBody();
		for(Unit u:body.getUnits()){
			if(u instanceof DefinitionStmt){
				DefinitionStmt d=(DefinitionStmt)u;
				Value left=d.getLeftOp();
				Value right=d.getRightOp();
				if(left.getType() instanceof RefLikeType){
					if(right instanceof AnyNewExpr){
						return true;
					}
				}
				if(right instanceof InvokeExpr){
					return true;
				}
			}
			else if(u instanceof InvokeStmt){
				return true;
			}
		}
		
		return false;
	}
	
	public static boolean isInstanceEscp(SootMethod m,FastEscapeAnalysis escape,Set<SootMethod> leakMethods){
		
		Body body=m.retrieveActiveBody();
		Collection<Local> locals=body.getLocals();
		for(Local l:locals){
		
		    //TODO 在这边判断一下局部变量l不是method类的实例
			if(isInstanceofClass(l,m))
				continue;
			
			if(isEscaped(l,m,escape)){
				return true;
			}
			
					
		}
		for(Unit u:body.getUnits()){
			if(u instanceof DefinitionStmt){
				DefinitionStmt d=(DefinitionStmt)u;
				Value left=d.getLeftOp();
				Value right=d.getRightOp();
				if(right instanceof InvokeExpr){
					SootMethod invokeMethod = ((InvokeExpr) right).getMethod();
					if(leakMethods.contains(invokeMethod))
						return true;
				}
			}
			
			if(u instanceof InvokeStmt){
				SootMethod invokeMethod = ((InvokeStmt) u).getInvokeExpr().getMethod();
				if(leakMethods.contains(invokeMethod))
					return true;				
			}
		}
		
		return false;
	}
	
	private static boolean isInstanceofClass(Local l,SootMethod m){
		//以下这个语句可用来判断对象l运行时可能对应的子类
		//对引用变量x，只能用PointsToAnalysis或PtsToQuery先获得它的指向集，根据指向集知道每个x可能运行时绑定的对象可能具有的类型
		//可以先用PointsToAnalysis的到类型集，然后用FastHierachy判断绑定的运行时类型是否可能是method对应类C的子类
//		Set<Type> types = Scene.v().getPointsToAnalysis().reachingObjects(l).possibleTypes();
		String t = l.getType().toString();
		if(t.equals(m.getDeclaringClass().getName()))
			return true;

		return false;
	}
	
	private static boolean isEscaped(Local l,SootMethod m,FastEscapeAnalysis escape){
		
		Collection<Local> escapedLocals=escape.getEscapedLocals(m);
		
		SootClass cls = m.getDeclaringClass();	
		
		if((l.getType() instanceof RefLikeType)&&(escapedLocals.contains(l))){
			return true;
		}	
		return false;
	}
	
	

}
