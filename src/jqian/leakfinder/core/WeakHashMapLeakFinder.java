package jqian.leakfinder.core;

import java.util.*;
import java.io.*;
import jqian.util.JavaClassFinder;

//import com.conref.core.sync.SyncReqAnalyzer;
//import com.conref.util.JavaClassFinder;


import soot.*;
import soot.jimple.AnyNewExpr;
import soot.jimple.ClassConstant;
import soot.jimple.Constant;
import soot.jimple.DefinitionStmt;
import soot.jimple.InstanceInvokeExpr;
import soot.jimple.InvokeExpr;
import soot.jimple.InvokeStmt;
import soot.jimple.Stmt;
import soot.jimple.spark.pag.AllocNode;
import soot.jimple.spark.pag.PAG;
import soot.jimple.spark.sets.PointsToSetInternal;
import soot.jimple.toolkits.callgraph.CallGraph;
import soot.jimple.toolkits.callgraph.CallGraphBuilder;
import soot.jimple.toolkits.pointer.DumbPointerAnalysis;
import soot.jimple.toolkits.pointer.Union;
import soot.toolkits.graph.UnitGraph;
import soot.util.Chain;
import soot.util.Numberable;

import test.PtsToTester;
import test.SDGUtil;
import test.Test;

import jqian.sootex.Cache;
import jqian.sootex.dependency.pdg.DepGraphOptions;
import jqian.sootex.dependency.pdg.DependenceEdge;
import jqian.sootex.dependency.pdg.DependenceNode;
import jqian.sootex.dependency.pdg.FormalIn;
import jqian.sootex.dependency.pdg.SDG;
import jqian.sootex.dependency.pdg.PDG;
import jqian.sootex.location.AccessPath;
import jqian.sootex.location.HeapAbstraction;
import jqian.sootex.location.InstanceObject;
import jqian.sootex.location.Location;

import jqian.sootex.ptsto.IPtsToQuery;
import jqian.sootex.ptsto.PAGHelper;
import jqian.sootex.ptsto.PtsToHelper;
import jqian.sootex.ptsto.SparkPtsToQuery;
import jqian.sootex.ptsto.TypeBasedPointsToAnalysis;
import jqian.sootex.ptsto.TypeBasedPointsToSet;
import jqian.sootex.ptsto.PtsToAdapter.AtomicFilter;
import jqian.sootex.sideeffect.FastEscapeAnalysis;
import jqian.sootex.util.SootUtils;
import jqian.sootex.util.callgraph.CallGraphHelper;
import jqian.leakfinder.util.*;

/**
 * Core leakFinder engine
 * @author nuaa
 *
 */

public class WeakHashMapLeakFinder {
//	FastEscapeAnalysis methodEscapeInfo;
	
	static Set<SootMethod> leakMethods = new HashSet<SootMethod>();	

	//add at 14.10.02
	public static boolean hasIntersection(Set<Numberable> keyObjPts, Numberable valuepts){
		for(Numberable pts: keyObjPts){
//			if(((PointsToSet)pts).hasNonEmptyIntersection(valuepts))
//				return true;
			if(pts.equals(valuepts))
				return true;
			if(pts == valuepts)
				return true;
		}
		return false;
	}
	//add at 14.11.18
	public static Collection<PointsToSet> getAllPointsToSet(Value obj,PointsToSet pt,IPtsToQuery pt2Query){
		Collection<PointsToSet> sets = new ArrayList<PointsToSet>();
		Stack<PointsToSet> stack = new Stack<PointsToSet>();
		stack.push(pt);
		sets.add(pt);
		while(!stack.isEmpty()){
			PointsToSet temp = stack.pop();
			Collection<PointsToSet> pts = getPointsToSet(obj,temp,pt2Query);
			stack.addAll(pts);
			sets.addAll(pts);
		}
		return sets;
		
	}
	public static Collection<PointsToSet> getPointsToSet(Value obj,PointsToSet pt,IPtsToQuery pt2Query){
		Collection<PointsToSet> pts = new ArrayList<PointsToSet>();
		
		Type type = ((Local) obj).getType();
		if(type instanceof RefLikeType){
	    	Location ptr = Location.valueToLocation((Local)obj);
		 //   Set<InstanceObject> pt2Set = pt2Query.getPointTos(m, null,ptr);
		    if (type instanceof RefType) {
		    	SootClass cls = ((RefType) type).getSootClass();
	            Collection<SootField> fields = Cache.v().getAllInstanceFields(cls);
	            for (SootField f: fields) {
	            	PointsToSet temp = null;
	            	if (f.getType() instanceof RefLikeType) {                            
	            		AccessPath ap = AccessPath.getByRoot(ptr);
	            		ap = ap.appendFieldRef(f);
	            			
	            		Set<?> locs = PtsToHelper.getAccessedLocations(pt2Query, HeapAbstraction.FIELD_SENSITIVE, null, ap);
	            			//  Test.printCollection(locs.iterator(),",");
	            			
	            		temp = testFieldPtsTo(locs,pt2Query);    
	            		if(temp!=null)
	            			pts.add(temp);
	            	}
	            }
	        }
	    }
		
		return pts;
		
	}
	
	
	
	public static void reachingClosure(Local local,Set<Numberable> out,PointsToAnalysis ptsTo){
		Set<Numberable> starts = reachingObjects(local,ptsTo);
		reachingClosure(starts,out, null,ptsTo);
	}
	private static void reachingClosure(Set<Numberable> starts,Set<Numberable> out, AtomicFilter filter,PointsToAnalysis ptsTo){		
		Stack<Numberable> stack = new Stack<Numberable>();		
		stack.addAll(starts);	
	
	    while(!stack.isEmpty()){
	    	Numberable n = stack.pop();
	    	out.add(n);
	    	
	    	//if is atomic type which can not be expanded
	    	if(filter!=null && filter.isAtomic(getAbstractObjectType(n))){
	    		continue;
	    	}
	    	
	    	Set<Numberable> reach = reachingObjectOfAllFields(n,ptsTo);
			for(Numberable tgt: reach){
	            if(!out.contains(tgt)){
	            	stack.push(tgt);
	            }		
	        }
	    }	    
	}
	public static Set<Numberable> reachingObjects(Local local,PointsToAnalysis _pta){
		PointsToSet pset = _pta.reachingObjects(local);
		return toHeapNodeSet(pset);
	} 
	public static Set<Numberable> toHeapNodeSet(PointsToSet ptSt) {
		Set result = null;
		if (ptSt instanceof PointsToSetInternal || ptSt instanceof Union) {
			result = new HashSet();
			toHeapNodeSet(ptSt, result);
		}
		else if(ptSt instanceof TypeBasedPointsToSet){
			result = (TypeBasedPointsToSet)ptSt;			 
		}
		 	
		return result;
	}
	public static void toHeapNodeSet(PointsToSet ptSt, Set<Numberable> result) {
		if (ptSt instanceof PointsToSetInternal || ptSt instanceof Union) {
			Set out = result;
			PAGHelper.toAllocNodeSet(ptSt, out);
		}
		else if(ptSt instanceof TypeBasedPointsToSet){
			TypeBasedPointsToSet set = (TypeBasedPointsToSet)ptSt;
			result.addAll(set);
		}
	}
	public static Type getAbstractObjectType(Numberable object){
		if(object instanceof Type){
			return (Type)object;
		}
		else if(object instanceof AllocNode){
			return ((AllocNode)object).getType();
		}
		
		return null;
	}
	public static Set<Numberable> reachingObjectOfAllFields(Numberable object,PointsToAnalysis _pta){
		Set result = null;
		if(_pta instanceof PAG){
			result = PAGHelper.reachingObjectsOfAllFields((AllocNode)object);			
		}
		else if(_pta instanceof TypeBasedPointsToAnalysis){
			TypeBasedPointsToAnalysis tpta = (TypeBasedPointsToAnalysis)_pta;
			result = tpta.reachingObjectsOfAllFields((Type)object);
		}
		
		return result;
	}
	
	
	
	public static boolean hasRef(Value keyO,Value valueO,SootMethod m,PointsToAnalysis ptsTo){
		
		//获得key值的指向集，并获得key对象内部的对象指向集
	 //   PointsToSet keypts = ptsTo.reachingObjects((Local)keyO);
	    Set<Numberable> keyObjPts = new HashSet<Numberable>();
	    reachingClosure((Local)keyO,keyObjPts,ptsTo);
	    
	    //获得value值的指向集，以及value对象内部的对象指向集，从而判断是否存在value到key的指向
	 //   PointsToSet valuepts = ptsTo.reachingObjects((Local)valueO);
	    Set<Numberable> valueObjPts = new HashSet<Numberable>();
	    reachingClosure((Local)valueO,valueObjPts,ptsTo);
	//    Collection<PointsToSet> valueObjPts = getAllPointsToSet(valueO,valuepts,pt2Query);
	    for(Numberable p:keyObjPts){
	    	return hasIntersection(valueObjPts,p);
	    }
    
	    return false;
	}
	
	public static boolean isWeakHashMapType(Type type, Body body){
		boolean result = false;
		if(type.toString().equals("java.util.WeakHashMap")){
		}
		
		if(type.toString().equals("java.util.WeakHashMap")||type.toString().equals("java.util.Map")||type.toString().equals("java.util.HashMap")){
			return true;
		}
//		for(Unit u: body.getUnits()){
//			if(u instanceof )
//		}
		
		
		//	
		return result;
	}
	
	public static Set<Value> getWeakHashMapLocs(SootMethod m){
		Body body = m.getActiveBody();
		Set <Value> weakHMLocs = new HashSet<Value>();
		for(Local l : body.getLocals()){   //首先找到WeakHashMap型的变量
			Type type = l.getType();
			if(type instanceof RefLikeType){
				if(type.toString().equals("java.util.WeakHashMap")){	
				     Value v = (Value)l;
					weakHMLocs.add(v);
				}
			}
		}
		for(Unit u:body.getUnits()){
			if(u instanceof DefinitionStmt){
				DefinitionStmt d = (DefinitionStmt) u;
				Value left = d.getLeftOp();
				Value right = d.getRightOp();
				if ((left.getType() instanceof RefLikeType)){
					if (right instanceof AnyNewExpr) {
						if(right.toString().equals("new java.util.WeakHashMap"))
							weakHMLocs.add(left);
					}
				}
				
			}
		}
		
		
		return weakHMLocs;
	}
	public static boolean isLeakMethod(SootMethod m,PointsToAnalysis ptsTo){
		boolean result = false;
	//	if(m.hasActiveBody()) 
		Body body = m.getActiveBody();
		
		Set <Value> weakHMLocs = getWeakHashMapLocs(m);
//		for(Local l : body.getLocals()){   //首先找到WeakHashMap型的变量
//			Type type = l.getType();
//			if(type instanceof RefLikeType){
//				if(isWeakHashMapType(type,body)){	
//				     Value v = (Value)l;
//					weakHMLocs.add(v);
//				}
//			}
//		}
		
		
		//找出存入WeakHashMap型变量中的key,value变量
		for(Unit u: body.getUnits()){
			if(result == true) return true;
			if(u instanceof InvokeStmt){
				InvokeExpr v = ((InvokeStmt) u).getInvokeExpr();
				if(v instanceof InstanceInvokeExpr){
					Value base = ((InstanceInvokeExpr) v).getBase();
					if(weakHMLocs.contains(base)){  //找出是WeakHashMap型对象的调用语句
						SootMethod invM= ((InstanceInvokeExpr) v).getMethod();
						
			//			if(invM.toString().equals  //若调用了WeakHashMap的put方法，则获取key和value值
			//					("<java.util.WeakHashMap: java.lang.Object put(java.lang.Object,java.lang.Object)>")){
						if(invM.getSubSignature().toString().equals  //若调用了WeakHashMap的put方法，则获取key和value值
								("java.lang.Object put(java.lang.Object,java.lang.Object)")){
							Value keyO = ((InstanceInvokeExpr) v).getArg(0);
							Value valueO = ((InstanceInvokeExpr) v).getArg(1);
							//判断value对象中是否存在对key的引用
						//	result = hasRef(keyO,valueO,m,ptsTo); 
							result = true;
						}
					}
				}
			}
		}
		
		return result;
	}
	private static PointsToSet testFieldPtsTo(Collection<?> locs,IPtsToQuery query){

		PointsToSet pt = null;
        for(Iterator<?> it=locs.iterator();it.hasNext();){
            Object obj=it.next();
            if(obj instanceof Location){
            	Location ptr = (Location)obj;
                if(ptr.isPointer()){                	
                	Set<InstanceObject> pt2Set = query.getPointTos(null, null,ptr);
                	pt = ((SparkPtsToQuery) query).getPointToset(ptr);
                } 
            }
        }
        return pt;
    }
	
	public static Collection<SootMethod> getWeakHashMapLeakMeths(Collection<SootMethod> methods,PointsToAnalysis ptsTo){
		Collection<SootMethod> leakMethods = new ArrayList<SootMethod>();
		for(SootMethod m: methods){
			if(m.isConcrete()){
				if(isLeakMethod(m,ptsTo))
					leakMethods.add(m);    		
			}
		}
		
		return leakMethods;
		
	}
	
	private static boolean hasMainClass(){
		try{
			SootClass c = Scene.v().getMainClass();
			return c!=null;
		}
		catch(Exception e){
			return false;
		}
	}
	public static void doSparkPointsToAnalysis(List<SootMethod> sets){
	
		
		
		
		
		//运行有问题
	/*      //第一遍对main的所有entry points，作指向分析，第二遍将剩余未作分析的方法作指向分析	
	      	List<SootMethod> entrypoints = new ArrayList<SootMethod>();
	      	if(!hasMainClass()){
	      		for(SootMethod m:sets){
	      			if(!m.hasActiveBody()&&m.isConcrete())
	      				entrypoints.add(m);
	      		}
	      		Scene.v().setEntryPoints(entrypoints);
	      	}
	      	else{
	      	    List<SootMethod> curr = Scene.v().getEntryPoints();
	         	entrypoints.addAll(curr);
	      //	Scene.v().setEntryPoints((List<SootMethod>) entrypoints);
	      	    Scene.v().setEntryPoints(EntryPoints.v().application());
	      	}
	      	
			Test.doFastSparkPointsToAnalysis();
	       	Test.simplifyCallGraph();
	       	
	       	List<SootMethod> secondEntryPoints = new ArrayList<SootMethod>();
	       	for(SootMethod m:sets){
	  			if(!m.hasActiveBody()&&m.isConcrete())
	  				secondEntryPoints.add(m);
	  		}
	       	if(!secondEntryPoints.isEmpty()){
	       		Scene.v().setEntryPoints(secondEntryPoints);
	           	Test.doFastSparkPointsToAnalysis();
	           	Test.simplifyCallGraph();
	       	}
	       	
//	       	Scene.v().releaseActiveHierarchy();
//	    	Scene.v().releaseCallGraph();
//	    	Scene.v().releaseFastHierarchy();
//	    	Scene.v().releasePointsToAnalysis();
//	    	Scene.v().releaseReachableMethods();
//	    	Scene.v().releaseSideEffectAnalysis();
	       	
	*/       	
		
	      //将所有不是方法加入entrypoints，再作指向分析  	
		      	List<SootMethod> entrypoints = new ArrayList<SootMethod>();
		      	for(SootMethod m: sets){
		      		if(!m.hasActiveBody()&& m.isConcrete())
		      			entrypoints.add(m);
		      	}
		      	if(!hasMainClass()){
		      		Scene.v().setEntryPoints(entrypoints);
		      	}
		      	else{
		      		SootClass c = Scene.v().getMainClass();
		      	List<SootMethod> curr = Scene.v().getEntryPoints();
		      	entrypoints.addAll(curr);
		      	Scene.v().setEntryPoints((List<SootMethod>) entrypoints);
		      	}
		      	
		      	System.out.println("待分析的方法个数：");
		      	System.out.println(Scene.v().getEntryPoints().size());
		      	
		      	Test.doFastSparkPointsToAnalysis();
		       	Test.simplifyCallGraph();		   		  
	       	
	   
	}

	public static List<SootMethod> getAnalysedMethods(String srcFile,String ext,String entry){
		List<SootMethod> sets = new LinkedList<SootMethod>();
		JavaClassFinder jcf = new JavaClassFinder(srcFile,ext);
    	Set<String> srcs = jcf.listClasses();
    	System.out.println("共有"+srcs.size()+"个类。");
    	Properties options = Test.loadConfig("/test/config.xml"); 
    	Test.setDefaultSootOptions(options, true, true);
    	
    	String entryClass;
    	for(String s: srcs){
    		entryClass = entry + s;
	      	options.put("entry_class", entryClass);
	      	
	      	if (SootUtils.isInternalAnonymousClass(entryClass)) {
				System.out.println("Ignore internal anonymous classes");
				continue;
			}
	      	
	      	Scene.v().addBasicClass(entryClass);
	      	SootClass cls = Scene.v().loadClassAndSupport(entryClass);
	      	cls.setApplicationClass();
	      	sets.addAll(cls.getMethods());
	      	
//	      	SootClass c = Scene.v().getMainClass();
	      	
	      	if(!hasMainClass()){
				if (cls.declaresMethod(Scene.v().getSubSigNumberer().findOrAdd("void main(java.lang.String[])"))) {
					Scene.v().setMainClass(cls);
				}
			}	     	  		
    	}
    	
      	Scene.v().loadNecessaryClasses();
		
		return sets;
	}
    public static void main(String []args){
    	
    	System.out.println("Start Test...");
    	
    	//简单测试cases包中案例
    	String srcFile = "./src/cases/test"; String ext = ".java";
    	String entryClass = "cases.test.";
    	
   // 	String srcFile = "./src/test/cases"; String ext = ".java";
   // 	String entryClass = "test.cases.";
    	
    	//测试JDK中案例
 //   	String srcFile = "C://Program Files/Java/jdk1.6.0_30/jre/lib/rt.jar";
 //   	String ext = ".class";String entryClass = "";
		
    	List<SootMethod> sets = getAnalysedMethods(srcFile,ext,entryClass);
    //	doSparkPointsToAnalysis(sets);   	
       	

    	CallGraphBuilder cgb = new CallGraphBuilder(DumbPointerAnalysis.v());
		cgb.build();
		CallGraphHelper.assureMethodsInCG(sets);
//		System.out.println(Scene.v().getEntryPoints().size());
		Test.doFastSparkPointsToAnalysis();
       	Test.simplifyCallGraph();
  	
    	
/*   	//针对一个main方法开始搜索相关的类
    	String mainClass=args[0];
//		String mainClass = "test.cases.SimpleLeakExample";
		
		Properties options = Test.loadConfig("/test/config.xml"); 
      	options.put("entry_class", mainClass);
       	
        //Get SSA representations
      	//Soot load Shimple format
       	Test.loadClasses(true);
       	
       	SootMethod entry=null;	 
		if(_sdgEntry!=null){
			entry = Scene.v().getMethod(_sdgEntry);
		}
		else{
			entry = (SootMethod)Scene.v().getEntryPoints().get(0);
		}
*/		     	
	    PointsToAnalysis ptsTo = Scene.v().getPointsToAnalysis();
    
	    Collection<SootMethod> potentialWeakHashMapLeakMeths = getWeakHashMapLeakMeths(sets,ptsTo);
	    System.out.println();
	    System.out.println("泄漏的方法是：");
	    
	    String allMethods ="";
	    for(SootMethod m:potentialWeakHashMapLeakMeths){
	    	allMethods = allMethods + "\r\n" + m.toString();
	    	System.out.println(m.toString());
	    }
	    try{
    		File newTextFile = new File("D:leak.txt");
    		FileWriter fw = new FileWriter(newTextFile);
    		fw.write(allMethods);
    		fw.close();
    	}
    	catch(IOException e){
    		e.printStackTrace();
    	}
		System.out.println(Scene.v().getEntryPoints().size());
	    
    }	
}
