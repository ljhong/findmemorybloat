package jqian.leakfinder.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import soot.Body;
import soot.Unit;
import soot.SootMethod;
import soot.jimple.Stmt;
import soot.toolkits.graph.UnitGraph;

public class Loop
{
	protected final Stmt header;
    protected final Stmt backJump;
    protected final List<Stmt> loopStatements;
    protected final UnitGraph g;
    protected Collection<Stmt> loopExists;
 
    Loop(Stmt head, List<Stmt> loopStatements, UnitGraph g)
    {
    	this.header = head;
    	this.g = g; 
    	
    	loopStatements.remove(head);
    	loopStatements.add(0, head);
    	
    	this.backJump = ((Stmt)loopStatements.get(loopStatements.size() - 1));
    	
    	assert (g.getSuccsOf(this.backJump).contains(head));
    	
    	this.loopStatements = loopStatements;
    }
    
    public Stmt getHead()
    {
    	return this.header;
    }
    
    public Stmt getBackJumpStmt()
    {
    	return this.backJump;
    }
    
    public List<Stmt> getLoopStatements()
    {
    	return this.loopStatements;    	
    }

    public Collection<Stmt> getLoopExits()
    {
    	Iterator i$;
    	if (this.loopExists == null) {
    		this.loopExists = new HashSet();
    		for (i$ = this.loopStatements.iterator(); i$.hasNext(); ) {Stmt s = (Stmt)i$.next();
    		for (Unit succ : this.g.getSuccsOf(s))
    			if (!(this.loopStatements.contains(succ)))
    				this.loopExists.add(s);
    		}
    	}   
    	
    	return this.loopExists;
   }
 
    public Collection<Stmt> targetsOfLoopExit(Stmt loopExit)
    {
    	assert (getLoopExits().contains(loopExit));
//      List succs = this.g.getSuccsOf(loopExit);
    	Collection res = new HashSet();
    	for (Unit u : this.g.getSuccsOf(loopExit)) {
    		Stmt s = (Stmt)u;
    		res.add(s);
    	}
    	
    	res.removeAll(this.loopStatements);
    	return res;
    }
    
    public UnitGraph getUnitGraph(){  //add by hong
    	return g;
    }
    
    public SootMethod getMethod(){  //add by hong
    	Body b=this.g.getBody();
    	SootMethod m=b.getMethod();
    	return m;
    }
    
    public List<Stmt> getLoopStmtsWithoutBackJumpStmts(){  //add by hong
    	List<Stmt> stmts=new ArrayList<Stmt>();
    	for(int i=0;i<loopStatements.size() - 1;i++){
    		stmts.add((Stmt)loopStatements.get(i));
    	}
    	return stmts;
    }

    public boolean loopsForever()
    {
    	return getLoopExits().isEmpty();
    }

    public boolean hasSingleExit()
    {
    	return (getLoopExits().size() == 1);
    }

    public int hashCode()
    {
    	int prime = 31;
    	int result = 1;
    	result = 31 * result + ((this.header == null) ? 0 : this.header.hashCode());
    	result = 31 * result + ((this.loopStatements == null) ? 0 : this.loopStatements.hashCode());
    	return result;
    }

    public boolean equals(Object obj)
    {
    	if (this == obj)
    		return true;
    	if (obj == null)
    		return false;
    	if (super.getClass() != obj.getClass())
    		return false;
    	Loop other = (Loop)obj;
    	if (this.header == null)
    		if (other.header != null)
    			return false;
    		else if (!(this.header.equals(other.header)))
    			return false;
    	if (this.loopStatements == null)
    		if (other.loopStatements != null)
    			return false;
    		else if (!(this.loopStatements.equals(other.loopStatements)))
    			return false;
    	return true;
    	}
    }

