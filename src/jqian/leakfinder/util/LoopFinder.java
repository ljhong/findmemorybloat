package jqian.leakfinder.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Stack;
import soot.Body;
import soot.BodyTransformer;
import soot.PatchingChain;
import soot.jimple.Stmt;
import soot.toolkits.graph.ExceptionalUnitGraph;
import soot.toolkits.graph.MHGDominatorsFinder;
import soot.toolkits.graph.UnitGraph;
 
public class LoopFinder extends BodyTransformer
{
	private UnitGraph g;
	private HashMap<Stmt, List<Stmt>> loops;
	
	public Collection<Loop> loops()
	{
		Collection result = new HashSet();
		for (Map.Entry entry : this.loops.entrySet()) {
			result.add(new Loop((Stmt)entry.getKey(), (List)entry.getValue(), this.g));
		}
		return result;
   }

	protected void internalTransform(Body b, String phaseName, Map options)
	{
		this.g = new ExceptionalUnitGraph(b);
		MHGDominatorsFinder a = new MHGDominatorsFinder(this.g);

		this.loops = new HashMap();

		Iterator stmtsIt = b.getUnits().iterator();
		while (stmtsIt.hasNext()) {
			Stmt s = (Stmt)stmtsIt.next();

			List succs = this.g.getSuccsOf(s);
			Collection dominaters = a.getDominators(s);

			ArrayList headers = new ArrayList();

			Iterator succsIt = succs.iterator();
			while (succsIt.hasNext()) {
				Stmt succ = (Stmt)succsIt.next();
				if (dominaters.contains(succ))
				{
					headers.add(succ);
				}
			}

			Iterator headersIt = headers.iterator();
			while (headersIt.hasNext()) {
				Stmt header = (Stmt)headersIt.next();
				List loopBody = getLoopBodyFor(header, s);
				
				if (this.loops.containsKey(header))
				{
					List lb1 = (List)this.loops.get(header);
					this.loops.put(header, union(lb1, loopBody));
				}
				else {
					this.loops.put(header, loopBody);
				}
			}
		}
	}

	private List<Stmt> getLoopBodyFor(Stmt header, Stmt node)
	{
		ArrayList loopBody = new ArrayList();
		Stack stack = new Stack();

		loopBody.add(header);
		stack.push(node);

		while (!(stack.isEmpty())) {
			Stmt next = (Stmt)stack.pop();
			if (!(loopBody.contains(next)))
			{
				loopBody.add(0, next);
				
				Iterator it = this.g.getPredsOf(next).iterator();
				while (it.hasNext()) {
					stack.push(it.next());
				}
			}
		}
		
		assert (((node == header) && (loopBody.size() == 1)) || (loopBody.get(loopBody.size() - 2) == node));
		assert (loopBody.get(loopBody.size() - 1) == header);
		
		return loopBody;
	}

	private List<Stmt> union(List<Stmt> l1, List<Stmt> l2) {
		Iterator it = l2.iterator();
		while (it.hasNext()) {
			Stmt next = (Stmt)it.next();
			if (!(l1.contains(next))) {
				l1.add(next);
			}
		}
		return l1;
	}
}


