package jqian.testing;

import java.lang.ref.WeakReference;

public class MemoryTester {
	public static final int LARGE_LOOPNUM = 80000;
	public static final int LARGE_INT = 8000;
	public static final int LARGE_LONG = 8000;
	public static final long maxAllowed = 100;
	public static final int RandomParaNum = 20;
	
	public static final int buffersize= 256 * 1024;
	private static final String originalString = "longstring";
	public String Long_String="";

	
	public static long getUsedMemory(){
		//FIXME 为什么这里gc一下，会出现内存泄漏，不gc的话就不会出现内存泄漏
		Runtime.getRuntime().gc();
		Runtime.getRuntime().gc();
		Runtime.getRuntime().gc();
		return Runtime.getRuntime().freeMemory()/1024;
	}

	public static boolean assertMemoryConsumation(String hint,String m, long consumed){
	//	System.out.println("SimpleLeakExample类的fun方法，测试执行时消耗的内存是："+usedMemory+"KB");
		if(consumed>maxAllowed)
		{
			System.out.println("测试"+hint+"类，执行测试方法"+ m +"，测试执行时消耗的内存是："+consumed+"KB，可能会造成内存泄漏");
			return true;
		}
		else{
			System.out.println("测试"+hint+"类，执行测试方法"+ m +"，测试执行时消耗的内存是："+consumed+"KB，不会造成内存泄漏");
			return false;
		}
		
	}
	
	public static void assertArgumentNotLeaked(WeakReference ref){
		Runtime.getRuntime().gc();
		Runtime.getRuntime().gc();
		if(ref.get()!=null)
			System.out.println("测试出该方法存在泄漏");
		else 
			System.out.println("测试出方法不存在泄漏");
	}
	
	public static void doSth(Object o){
		o = null;
	}
	
	public static WeakReference prepareArgument(Object o){
		return new WeakReference<Object>(o);
	}
	
	public static String getlongstring(){
		StringBuffer strbuf = new StringBuffer(buffersize);
		int count = (buffersize/originalString.length());
		for(int i = 0; i< count; i++){
			strbuf.append(originalString);
		}
		return strbuf.toString();
	}
	
	public static void assertMemoryLeak(String methodName){
		System.out.println("测试出方法"+methodName+"存在泄漏");
	}
	
	public static void assertNoMemoryLeak(String methodName){
		System.out.println("测试出方法"+methodName+"不存在泄漏");
	}

	
}
