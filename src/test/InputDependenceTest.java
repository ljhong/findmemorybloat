package test;

import java.util.*;
import java.io.*;

import soot.*;
import soot.jimple.Stmt;
import soot.jimple.toolkits.callgraph.CallGraph;
import soot.toolkits.graph.UnitGraph;

import test.SDGUtil;
import test.Test;

import jqian.sootex.Cache;
import jqian.sootex.dependency.pdg.DepGraphOptions;
import jqian.sootex.dependency.pdg.DependenceEdge;
import jqian.sootex.dependency.pdg.DependenceNode;
import jqian.sootex.dependency.pdg.FormalIn;
import jqian.sootex.dependency.pdg.SDG;
import jqian.sootex.dependency.pdg.PDG;
import jqian.sootex.location.HeapAbstraction;

import jqian.sootex.sideeffect.FastEscapeAnalysis;
import jqian.sootex.util.SootUtils;
import jqian.leakfinder.core.LoopAnalysis;
import jqian.leakfinder.core.LoopEscapeAnalysis;
import jqian.leakfinder.util.*;

public class InputDependenceTest {
	
	protected static Collection<Loop> getLoopContainedMedthod(Collection<SootMethod> methods){
		Collection<Loop> allLoops=new ArrayList<Loop>();
		
		LoopAnalysis la=new LoopAnalysis();
		for(SootMethod m: methods){			
			if(m.isConcrete()){
				Collection<Loop> loops=la.getLoopsContainedNew(m);
				if(loops!=null){
					allLoops.addAll(loops);
				}					
			}			
		}	
		return allLoops;
	}
	
	protected static Collection<Loop> getInputDependentLoop(Collection<Loop> loops,SDG sdg){
		Collection<Loop> inputDepLoops=new ArrayList<Loop>();
		
		for(Loop loop:loops){			
			if(isInputDepend(sdg,loop)){
				inputDepLoops.add(loop);
			}	
		}
			
		return inputDepLoops;
	}
	
	protected static boolean isInputDepend(SDG sdg,Loop loop){
		boolean result=false;
		SootMethod m=loop.getMethod();
		PDG pdg=sdg.getPDG(m);
		Stmt loopCondition=loop.getHead();
		DependenceNode node = pdg.getStmtBindingNode(loopCondition);
		Collection<DependenceEdge> edges=sdg.edgesInto(node);
		
		Set<DependenceEdge> results=new HashSet<DependenceEdge>();
		Stack<DependenceEdge> stack=new Stack<DependenceEdge>();
		Set<DependenceEdge> inStack=new HashSet<DependenceEdge>();
		
		stack.addAll(edges);
		inStack.addAll(edges);
		
		while(!stack.isEmpty()){
			DependenceEdge top=stack.pop();
			inStack.remove(top);
			
			if (!results.add(top)) {
				continue;
			}
			
			DependenceNode d=top.getFrom();
			if(d instanceof FormalIn)
				result=true;
			else{
				Collection<DependenceEdge> nexts=sdg.edgesInto(d);
				if(nexts==null)
					continue;
				for(DependenceEdge e:nexts){
					if(!results.contains(e)&&!inStack.contains(e)){
						stack.add(e);
						inStack.add(e);
					}
				}
			}
			if(result) break;
		}
		
		return result;
	}
	
	protected static void displayInputDepLoops(Collection<Loop> loops){
		for(Loop loop:loops){
			System.out.println(loop.getMethod().getName()+":"+loop.getLoopStatements());
		}
	}
	
	public static void main(String []args){
		String _sdgEntry=null;
		String mainClass = "test.cases.InputDependenceCases";
		
		Properties options = Test.loadConfig("/test/config.xml"); 
       	options.put("entry_class", mainClass);
       	
        //Get SSA representations
      	//Soot load Shimple format
       	Test.loadClasses(true);
       	
       	SootMethod entry=null;	 
		if(_sdgEntry!=null){
			entry = Scene.v().getMethod(_sdgEntry);
		}
		else{
			entry = (SootMethod)Scene.v().getEntryPoints().get(0);
		}
		
		Test.doFastSparkPointsToAnalysis();
       	Test.simplifyCallGraph();
       	
//      Collection<SootMethod> entries = new ArrayList<SootMethod>(1);
//		entries.add(entry);
	        
//		int methodNum = SootUtils.getMethodCount();        
	    CallGraph cg = Scene.v().getCallGraph();
       	
       	HeapAbstraction locAbstraction = HeapAbstraction.FIELD_SENSITIVE;
    	DepGraphOptions pdgOptions = new DepGraphOptions(true, false, locAbstraction);
       	
       	SDG sdg= SDGUtil.constructSDG(entry,pdgOptions,true, 2);
       	sdg.buildSummaryEdges(entry);
		sdg.connectPDGs();
		sdg.compact();
	
	    // TODO 改成所有方法，而不只是可达的
		List<?> rm = Cache.v().getTopologicalOrder();
		
		Collection<Loop> loops=getLoopContainedMedthod((Collection<SootMethod>)rm);
		Collection<Loop> inputDepLoops=getInputDependentLoop(loops,sdg);
		
		displayInputDepLoops(inputDepLoops);
	    }

}
