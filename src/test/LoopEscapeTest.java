package test;

import java.util.*;
import java.io.*;


import soot.*;
import soot.jimple.Stmt;
import soot.jimple.toolkits.callgraph.CallGraph;
import soot.toolkits.graph.UnitGraph;

import test.SDGUtil;
import test.Test;

import jqian.sootex.Cache;
import jqian.sootex.dependency.pdg.DepGraphOptions;
import jqian.sootex.dependency.pdg.DependenceEdge;
import jqian.sootex.dependency.pdg.DependenceNode;
import jqian.sootex.dependency.pdg.FormalIn;
import jqian.sootex.dependency.pdg.SDG;
import jqian.sootex.dependency.pdg.PDG;
import jqian.sootex.location.HeapAbstraction;

import jqian.sootex.sideeffect.FastEscapeAnalysis;
import jqian.sootex.util.SootUtils;
import jqian.leakfinder.core.LoopAnalysis;
import jqian.leakfinder.core.LoopEscapeAnalysis;
import jqian.leakfinder.util.*;

public class LoopEscapeTest {
	
	protected static Collection<Loop> getLoopContainedMedthod(Collection<SootMethod> methods){
		Collection<Loop> allLoops=new ArrayList<Loop>();
		
		LoopAnalysis la=new LoopAnalysis();
		for(SootMethod m: methods){			
			if(m.isConcrete()){
				Collection<Loop> loops=la.getLoopsContainedNew(m);
				if(loops!=null){
					allLoops.addAll(loops);
				}	
				
			}			
		}	
		return allLoops;
	}
	
	protected static Collection<Loop> getNewObjectEscapedLoops(Collection<Loop> loops,FastEscapeAnalysis escape){
		Collection<Loop> results = new ArrayList<Loop>();;
		for(Loop loop: loops){
			LoopEscapeAnalysis loopEsc=new LoopEscapeAnalysis(loop,escape);
			loopEsc.build();
			boolean result=loopEsc.isLoopNewObjectEscaped();	
			if(result){
				results.add(loop);
			}
		}
		return results;
	}
	
	protected static void displayInstanceEscpLoops(Collection<Loop> loops){
		for(Loop loop:loops){
			System.out.println(loop.getMethod().getName()+":"+loop.getLoopStatements());
		}
	}
	
	public static void main(String []args){
		String _sdgEntry=null;
		String mainClass = "test.cases.LoopEscapeCases";
		
		Properties options = Test.loadConfig("/test/config.xml"); 
       	options.put("entry_class", mainClass);
       	
        //Get SSA representations
      	//Soot load Shimple format
       	Test.loadClasses(true);
       	
       	SootMethod entry=null;	 
		if(_sdgEntry!=null){
			entry = Scene.v().getMethod(_sdgEntry);
		}
		else{
			entry = (SootMethod)Scene.v().getEntryPoints().get(0);
		}
		
		Test.doFastSparkPointsToAnalysis();
       	Test.simplifyCallGraph();
       	
//      Collection<SootMethod> entries = new ArrayList<SootMethod>(1);
//		entries.add(entry);
	        
//		int methodNum = SootUtils.getMethodCount();        
	    CallGraph cg = Scene.v().getCallGraph();
       	
       	HeapAbstraction locAbstraction = HeapAbstraction.FIELD_SENSITIVE;
    	DepGraphOptions pdgOptions = new DepGraphOptions(true, false, locAbstraction);
       	
       	SDG sdg= SDGUtil.constructSDG(entry,pdgOptions,true, 2);
       	sdg.buildSummaryEdges(entry);
		sdg.connectPDGs();
		sdg.compact();
	
	    // TODO 改成所有方法，而不只是可达的
		List<?> rm = Cache.v().getTopologicalOrder();
		
		Collection<Loop> loops=getLoopContainedMedthod((Collection<SootMethod>)rm);
		
		FastEscapeAnalysis escape=new FastEscapeAnalysis(cg);
		escape.build();
		
		
		Collection<Loop> instanceEscLoops=getNewObjectEscapedLoops(loops,escape);
		
		displayInstanceEscpLoops(instanceEscLoops);
	    }

}
