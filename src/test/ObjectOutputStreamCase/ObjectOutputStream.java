package test.ObjectOutputStreamCase;

import java.io.Externalizable;
import java.io.IOException;
import java.io.NotSerializableException;
import java.io.ObjectStreamClass;
import java.io.Serializable;
import java.util.Arrays;

public class ObjectOutputStream{
	
//	private final HandleTable handles=null;
	
	private int size;
	/* size threshold determining when to expand hash spine */
	private int threshold;
	/* factor for computing size threshold */
	/* maps hash value -> candidate handle value */
	private int[] spine;
	/* maps handle value -> next candidate handle value */
	private int[] next;
	/* maps handle value -> associated object */
	private Object[] objs;
	
//	 public ObjectOutputStream() {
//		 SecurityManager sm = System.getSecurityManager();
//	 }
	 
	 public void writeUnshared(Object obj) throws IOException {
			 writeObject0(obj, true);

		
	 }
	 
	   private void writeObject0(Object obj, boolean unshared) {
				    int h;
				    
				    Object orig = obj;
				    Class cl = obj.getClass();
				    ObjectStreamClass desc = null;			

					writeOrdinaryObject(obj, desc, unshared);

			    }
	   
	   private void writeOrdinaryObject(Object obj, 
			     ObjectStreamClass desc, 
			     boolean unshared) 
{
//  if (extendedDebugInfo) {
//  debugInfoStack.push(
//	(depth == 1 ? "root " : "") + "object (class \"" + 
//	obj.getClass().getName() + "\", " + obj.toString() + ")");
//  }
//  try {
//  desc.checkSerialize();
//
//  bout.writeByte(TC_OBJECT);
//  writeClassDesc(desc, false);
  assign(unshared ? null : obj);
//  if (desc.isExternalizable() && !desc.isProxy()) {
//	writeExternalData((Externalizable) obj);
//  } else {
//	writeSerialData(obj, desc);
//  }
//} finally {
//	    if (extendedDebugInfo) {
//	debugInfoStack.pop();
//  }  
//  }
}
	   int assign(Object obj) {
//   	    if (size >= next.length) {
 //  		growEntries();
//   	    }
 //  	    if (size >= threshold) {
//   		growSpine();
//   	    }
   	    insert(obj, size);
   	    return size++;
   	}
	   
	   private void insert(Object obj, int handle) {
   	    int index = hash(obj) % spine.length;
   	    objs[handle] = obj;
   	    next[handle] = spine[index];
   	    spine[index] = handle;
   	}
	   
	   private int hash(Object obj) {
   	    return System.identityHashCode(obj) & 0x7FFFFFFF;
   	}
	   
	   
	 
	 
}