package test;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import soot.RefLikeType;
import soot.Scene;
import soot.SootMethod;
import soot.jimple.toolkits.callgraph.CallGraph;

import jqian.sootex.Cache;
import jqian.sootex.util.SootUtils;
import jqian.sootex.dependency.pdg.DepGraphOptions;
import jqian.sootex.dependency.pdg.PDG;
import jqian.sootex.dependency.pdg.SDG;
import jqian.sootex.location.HeapAbstraction;
import jqian.sootex.ptsto.IPtsToQuery;
import jqian.leakfinder.core.LeakFinder;
import jqian.leakfinder.core.LoopAnalysis;
public class SimpleLoopTest {
	
	static void testSDG(String entryMethodSignature, DepGraphOptions _options, boolean showPDGs) {
    	SootMethod main = Scene.v().getMethod(entryMethodSignature);    	
		SDG sdg = SDGUtil.constructSDG(main,_options,true, 2);
		sdg.buildSummaryEdges(main);
		sdg.connectPDGs();
		sdg.compact();
    	
		//show PDG of each method
		if(showPDGs){
			List<?> rm = Cache.v().getTopologicalOrder();             
	    	for(Iterator<?> it = rm.iterator();it.hasNext();){
	    	     SootMethod m = (SootMethod)it.next();
	    	     PDG pdg = sdg.getPDG(m);
	    	     if(pdg!=null){	    	    	 
	    	    	 String filename = "./output/dot/pdg_"+m.hashCode()+".dot";		
	    	    	 SDGUtil.showDependenceGraph(pdg,filename);   	
	    	     }
	    	}
		}		
    	
	
		SDGUtil.showDependenceGraph(sdg,"./output/dot/sdg.dot");			
		//DependenceGraph dep = sdg.toJavaStmtDepGraph();
		//SDGUtil.showDependenceGraph(dep,"./output/dot/javasdg.dot"); 		
    } 
	public static void main(String[] args){
		//String mainClass = PDG_MAIN_CLASS;
		String _sdgEntry=null;
		String mainClass = "test.cases.SimpleLeakExample";
		
		Properties options = Test.loadConfig("/test/config.xml"); 
       	options.put("entry_class", mainClass);
       	
       	Test.loadClasses(true);
       	
       	SootMethod entry=null;	 
		if(_sdgEntry!=null){
			entry = Scene.v().getMethod(_sdgEntry);
		}
		else{
			entry = (SootMethod)Scene.v().getEntryPoints().get(0);
		}
       	
       	Test.doFastSparkPointsToAnalysis();
       	Test.simplifyCallGraph();
       	
//      Collection<SootMethod> entries = new ArrayList<SootMethod>(1);
//		entries.add(entry);
	        
//		int methodNum = SootUtils.getMethodCount();        
//	    CallGraph cg = Scene.v().getCallGraph();
       	
       	HeapAbstraction locAbstraction = HeapAbstraction.FIELD_SENSITIVE;
    	DepGraphOptions pdgOptions = new DepGraphOptions(true, false, locAbstraction);
       	
       	SDG sdg= SDGUtil.constructSDG(entry,pdgOptions,true, 2);
       	sdg.buildSummaryEdges(entry);
		sdg.connectPDGs();
		sdg.compact();
		
	    
	    LeakFinder lf=new LeakFinder();
	    
//       	LoopAnalysis analy=new LoopAnalysis(cg);
	    List<?> rm = Cache.v().getTopologicalOrder();
//	    Collection<SootMethod> potentialMethods=lf.getPotentialBloatMethods((Collection<SootMethod>)rm,sdg);
	    
       	
		
		
	}
	public void setEntry(){
		SootUtils.resetSoot();
	}
	
	public void getMethod(){
		
	}

}
