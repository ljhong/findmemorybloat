package test.StringCase;

import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.Arrays;


//import java.io.ObjectStreamClass;
//import java.io.ObjectStreamField;
//import java.io.UnsupportedEncodingException;
//import java.nio.charset.Charset;
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.Comparator;
//import java.util.Formatter;
//import java.util.Locale;
//import java.util.regex.Matcher;
//import java.util.regex.Pattern;
//import java.util.regex.PatternSyntaxException;


public final class String
//    implements java.io.Serializable, Comparable<String>, CharSequence
{
    /** The value is used for character storage. */
    private final char value[];

    /** The offset is the first index of the storage that is used. */
    private final int offset;

    /** The count is the number of characters in the String. */
    private final int count;

    /** Cache the hash code for the string */
//    private int hash; // Default to 0

 //   private static final long serialVersionUID = -6849794470754667710L;

//    private static final ObjectStreamField[] serialPersistentFields =
//        new ObjectStreamField[0];

    public String() {
	this.offset = 0;
	this.count = 0;
	this.value = new char[0];
    }

    public String(String original) {
	int size = original.count;
	char[] originalValue = original.value;
	char[] v;
  	if (originalValue.length > size) {
 	   
            int off = original.offset;
            v = Arrays.copyOfRange(originalValue, off, off+size);
 	} else {
 	    
	    v = originalValue;
 	}
	this.offset = 0;
	this.count = size;
	this.value = v;
    }

   
    public String(char value[]) {
	this.offset = 0;
	this.count = value.length;
	this.value = StringValue.from(value);
    }

  
    public String(char value[], int offset, int count) {
        if (offset < 0) {
            throw new StringIndexOutOfBoundsException(offset);
        }
        if (count < 0) {
            throw new StringIndexOutOfBoundsException(count);
        }
        // Note: offset or count might be near -1>>>1.
        if (offset > value.length - count) {
            throw new StringIndexOutOfBoundsException(offset + count);
        }
        this.offset = 0;
        this.count = count;
        this.value = Arrays.copyOfRange(value, offset, offset+count);
    }

  
    public String(int[] codePoints, int offset, int count) {
        if (offset < 0) {
            throw new StringIndexOutOfBoundsException(offset);
        }
        if (count < 0) {
            throw new StringIndexOutOfBoundsException(count);
        }
        // Note: offset or count might be near -1>>>1.
        if (offset > codePoints.length - count) {
            throw new StringIndexOutOfBoundsException(offset + count);
        }

	int expansion = 0;
	int margin = 1;
	char[] v = new char[count + margin];
	int x = offset;
	int j = 0;
	for (int i = 0; i < count; i++) {
	    int c = codePoints[x++];
	    if (c < 0) {
		throw new IllegalArgumentException();
	    }
	    if (margin <= 0 && (j+1) >= v.length) {
		if (expansion == 0) {
		    expansion = (((-margin + 1) * count) << 10) / i;
		    expansion >>= 10;
		    if (expansion <= 0) {
			expansion = 1;
		    }
		} else {
		    expansion *= 2;
		}
                int newLen = Math.min(v.length+expansion, count*2);
		margin = (newLen - v.length) - (count - i);
                v = Arrays.copyOf(v, newLen);
	    }
	    if (c < Character.MIN_SUPPLEMENTARY_CODE_POINT) {
		v[j++] = (char) c;
	    } else if (c <= Character.MAX_CODE_POINT) {
		Character.toSurrogates(c, v, j);
		j += 2;
		margin--;
	    } else {
		throw new IllegalArgumentException();
	    }
	}
	this.offset = 0;
	this.value = v;
	this.count = j;
    }

    @Deprecated
    public String(byte ascii[], int hibyte, int offset, int count) {
	checkBounds(ascii, offset, count);
        char value[] = new char[count];

        if (hibyte == 0) {
            for (int i = count ; i-- > 0 ;) {
                value[i] = (char) (ascii[i + offset] & 0xff);
            }
        } else {
            hibyte <<= 8;
            for (int i = count ; i-- > 0 ;) {
                value[i] = (char) (hibyte | (ascii[i + offset] & 0xff));
            }
        }
	this.offset = 0;
	this.count = count;
	this.value = value;
    }

    @Deprecated
    public String(byte ascii[], int hibyte) {
        this(ascii, hibyte, 0, ascii.length);
    }

    private static void checkBounds(byte[] bytes, int offset, int length) {
	if (length < 0)
	    throw new StringIndexOutOfBoundsException(length);
	if (offset < 0)
	    throw new StringIndexOutOfBoundsException(offset);
	if (offset > bytes.length - length)
	    throw new StringIndexOutOfBoundsException(offset + length);
    }

    public String(byte bytes[], int offset, int length, String charsetName)
	throws UnsupportedEncodingException
    {
	if (charsetName == null)
	    throw new NullPointerException("charsetName");
	checkBounds(bytes, offset, length);
	char[] v = StringCoding.decode(charsetName, bytes, offset, length);
	this.offset = 0;
	this.count = v.length;
	this.value = v;
    }

    public String(byte bytes[], int offset, int length, Charset charset) {
	if (charset == null)
	    throw new NullPointerException("charset");
	checkBounds(bytes, offset, length);
	char[] v = StringCoding.decode(charset, bytes, offset, length);
	this.offset = 0;
	this.count = v.length;
	this.value = v;
    }

   
    public String(byte bytes[], String charsetName)
	throws UnsupportedEncodingException
    {
	this(bytes, 0, bytes.length, charsetName);
    }

  
    public String(byte bytes[], Charset charset) {
	this(bytes, 0, bytes.length, charset);
    }

    public String(byte bytes[], int offset, int length) {
	checkBounds(bytes, offset, length);
	char[] v  = StringCoding.decode(bytes, offset, length);
	this.offset = 0;
	this.count = v.length;
	this.value = v;
    }

    public String(byte bytes[]) {
	this(bytes, 0, bytes.length);
    }

    public String(StringBuffer buffer) {
        String result = buffer.toString();
        this.value = result.value;
        this.count = result.count;
        this.offset = result.offset;
    }

   
    public String(StringBuilder builder) {
        String result = builder.toString();
        this.value = result.value;
        this.count = result.count;
        this.offset = result.offset;
    }


    // Package private constructor which shares value array for speed.
    String(int offset, int count, char value[]) {
	this.value = value;
	this.offset = offset;
	this.count = count;
    }

    public String substring(int beginIndex) {
	return substring(beginIndex, count);
    }

 
    public String substring(int beginIndex, int endIndex) {
	if (beginIndex < 0) {
	    throw new StringIndexOutOfBoundsException(beginIndex);
	}
	if (endIndex > count) {
	    throw new StringIndexOutOfBoundsException(endIndex);
	}
	if (beginIndex > endIndex) {
	    throw new StringIndexOutOfBoundsException(endIndex - beginIndex);
	}
	return ((beginIndex == 0) && (endIndex == count)) ? this :
	    new String(offset + beginIndex, endIndex - beginIndex, value);
    }

}