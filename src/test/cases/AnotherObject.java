package test.cases;

public class AnotherObject {
	private SimpleObject o;
	public SimpleObject getO() {
		return o;
	}
	public void setO(SimpleObject o) {
		this.o = o;
	}
	private int n;
	public AnotherObject(){
		
	}
	public AnotherObject(SimpleObject o,int n){
		this.o = o;
		this.n = n;
	}
	
	public SimpleObject getValueObj1(SimpleObject obj){
		this.o = obj;
		return getO();
	}
	
	public SimpleObject getValueObj2(){
		return o;
	}
}
