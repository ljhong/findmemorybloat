package test.cases;

public class Case2 {
	Object outerObj=new SimpleObject();
	
	public static void main(String []args){
		Case2 t=new Case2();
		t.getOuterObj1();
		t.getOuterObj2();
		t.getOuterObj3();
		test.temp.Case1.getObjects1();   //调用别的类的方法
	}

	public void getOuterObj1(){
		SimpleObject o=new SimpleObject();
		outerObj=o;
	}
	
	public void getOuterObj2(){
		int i=5;
		if(i>0){
			SimpleObject o=new SimpleObject();
			outerObj=o;
		}
	}
	
	public SimpleObject getOuterObj3(){
		SimpleObject o=new SimpleObject();
		outerObj=o;
		return o;
	}
}
