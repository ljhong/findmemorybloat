package test.cases;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Vector;

public class Case4 {
	
	static Vector<Object> v=new Vector<Object>(100);
	Object outer=new SimpleObject();
	
	//Que:若方法有多个参数，那么在测试时如何给出各个参数值
	public static void main(String []args){
		Case4 test = new Case4();
		test.fun1(3);
//		test.fun2(3);
//		test.fun3(3);
//		test.fun4(3);
//		test.fun5(3);
//		test.fun6(3);   //找不出此泄漏方法
//		test.fun7(3);
//		test.fun8(3);
//		test.fun9(3);   //
//		test.fun10(3);  //
//		test.fun11(3);  
//		test.fun12(3);  
	}
	
	public void fun1(int x){
		for(int i=0;i<x;i++){
			Object o = new SimpleObject();
			v.add(o);
		}
	}
	
	public void fun2(int x){
		for(int i = 0;i <x;i++){
			Object o = new SimpleObject();
			o.hashCode();
		}
	}
	
	public void fun3(int x){
		for(int i=0;i<x;i++){
			Object o = new SimpleObject();
			outer = o;
		}
	}
	
	public void fun4(int x){
		int i=0;
		if(i<x){
			Object o = new SimpleObject();
			v.add(o);
		}
	}
	
	public void fun5(int x){
		int num=20;
		for(int i=0;i<x;i++){
			if(i>num){
				SimpleObject o = new SimpleObject();
				v.add(o);
			}
		}
	}
	
	public void fun6(int x){
		int num=100;
		for(int i=0;i<num;i++){
			for(int j=0;j<x;j++){
				SimpleObject o = new SimpleObject();
				v.add(o);
			}		   
		}
	}
	
	public void fun7(int x){
		int num=100;
		for(int i=0;i<x;i++){
			for(int j=0;j<num;j++){
				SimpleObject o = new SimpleObject();
				v.add(o);
			}		   
		}
	}
	
	public void fun8(int x){
		Object outerObj = new SimpleObject();
		for(int i = 0;i < x; i ++){
			SimpleObject o = new SimpleObject();
			outerObj = o;
		}
		
	}
	
	//如何测试循环溢出的案例
	public void fun9(int x){
		Collection<SimpleObject> outerObjs = new ArrayList<SimpleObject>(100);
		for(int i = 0;i<x;i++){
			SimpleObject o = new SimpleObject();
			outerObjs.add(o);
		}
	}
	
	//对于这个方法的话，有时可以检测出泄漏情况，有时又不行，跟生成SDG有关
	public void fun10(int x){
		long sum=0;
		for(int i=0;i<x;i++){
			sum = sum + 2;
		}
		for(int i=0;i<sum;i++){
			SimpleObject o = new SimpleObject();
			v.add(o);
		}
	}
	
	public void fun11(int x){
		for(int i=0;i<x;i++){
			v.add(Case4.a());
		}
	}
	
	public void fun12(int x){
		for(int i=0;i<x;i++){
			outer = Case4.a();
		}
	}
	
	static SimpleObject a(){
		SimpleObject o = new SimpleObject();
		return o;
	}
}
