package test.cases;

import java.util.ArrayList;
import java.util.Collection;

public class Case5 {
	
	static Collection<SimpleObject> c=new ArrayList<SimpleObject>();
	
	public static void main(String []args){
		Case5 test = new Case5();
		test.getObject1();
		test.getObject2();
		test.getObject3(new SimpleObject());
		test.getObject4();
	}
	
	public SimpleObject getObject1(){
		Case5 t=new Case5();
		SimpleObject o = new SimpleObject();
		t.dosetO();
		return o;
	}
	
	public SimpleObject getObject2(){
		return getObject3(null);
	}
	
	public SimpleObject getObject3(SimpleObject resource){
		Case5 t = new Case5();
		SimpleObject o = new SimpleObject();
		t.dosetO();
		return o;	
	}
	
	private void dosetO(){
		SimpleObject o = new SimpleObject();
		c.add(o);
	}
	
	public SimpleObject getObject4(){
		return getObject4(null);
	}
	
	public SimpleObject getObject4(String str){
		Case5 t = new Case5();
		SimpleObject o = new SimpleObject();
		t.dosetO();
		return o;
	}

}
