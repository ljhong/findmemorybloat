package test.cases;

import jqian.testing.MemoryTester;

//此用例模仿subString方法的泄漏

//TODO 数组型 的考虑   不一定是get方法的溢出，考虑如何生成测试用例，不加是get方法这一条件，可能会为多个方法生成测试用例，
//有的可能是内部调用的一些,不需要去测试，而本用例存在泄漏的方法subObjects()就不是泄漏方法，如何找到呢?
//解决的方法：可通过判断主函数有调用什么，则对其进行判断
//但是此种解决方法不适用于对整个JDK用例的测试，当测试整个JDK用例时，采用检测每个方法，不是通过入口函数main来查找检测

public class Case6 {
	private static SimpleObject value[] = new SimpleObject[10000000];
	
	public Case6(SimpleObject v[]){
		this.value = v;
	}	
	private Case6(){
	}
	
	public static void main(String []args){
		for(int i= 0; i< 10000000; i++)
			value[i]= new SimpleObject();
		Case6 t = new Case6(value);
		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
		{
			t.getObjects1();
		}
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;
		MemoryTester.assertMemoryConsumation("String","subStringr",usedMemory);
		
//		t.subObjects();
	}
	
	public static void getObjects1(){  //相当于substring方法
		
		new Case6(value);
	}
	
	public void subObjects(){
		new Case6(value);
	}

}
