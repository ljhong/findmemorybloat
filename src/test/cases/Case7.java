package test.cases;

import java.util.ArrayList;
import java.util.Collection;

public class Case7 {
	
	static Collection<SimpleObject> c=new ArrayList<SimpleObject>();
	
	private Case7(){
		
	}
	protected Case7(String str){
		
	}
	public static void main(String []args){
		Case7 test = new Case7();
		test.getObject1();
	}
	
	public static SimpleObject getObject1(){
		Case7 t=new Case7();
		SimpleObject o = new SimpleObject();
		t.dosetO();
		return o;
	}
	
	private static void dosetO(){
		SimpleObject o = new SimpleObject();
		c.add(o);
	}

}
