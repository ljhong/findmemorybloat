package test.cases;

public class InputDependenceCases {
	public static void main(String[] args) {
		InputDependenceCases s=new InputDependenceCases();
		s.foo(2);
		s.fun(1);
		s.meth1(1);
		s.meth2();
	}
	
	public void foo(int x){
		for(int i=0;i<x;i++){
			SimpleObject o=new SimpleObject();
			o.hashCode();
		}
	}
	
	public void fun(int x){
		int sum=0;
		for(int i=0;i<x;i++){
			sum=sum+x;
		}
		for(int j=0;j<sum;j++){
			SimpleObject o=new SimpleObject();
			o.hashCode();
		}
	}
	
	public void meth1(int x){
		for(int i=0;i<100;i++){
			SimpleObject o=new SimpleObject();
			o.hashCode();
		}
	}
	
	public void meth2(){
		for(int i=0;i<100;i++){
			SimpleObject o=new SimpleObject();
			o.hashCode();
		}
	}

}
