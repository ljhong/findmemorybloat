package test.cases;
import java.util.Vector;
import java.util.Date;
public class LoopEscapeCases {
	static Vector<Object> v=new Vector<Object>();
	Object outerO=new SimpleObject();
	public static void main(String[] args) {
		LoopEscapeCases s=new LoopEscapeCases();
		s.foo(2);
		s.bar(1);
		s.ter(3);
		s.saur(1);
		s.fun(1);
	}
	
	public void foo(int x){
		for(int i=0;i<x;i++){
			Object o=new SimpleObject();
			v.add(o);
		}
	}
	
	public void bar(int x){
		for(int i=0;i<x;i++){
			Object n = new SimpleObject();
			n.hashCode();
		}
	}
	
	public void ter(int x){  //此测试用例会在方法级被判断出对象溢出
		for(int i=0;i<x;i++){
			Object t=new SimpleObject();
			outerO=t;
		}
	}
	
	public Object saur(int x){  
		Object methO=new SimpleObject();
		for(int i=0;i<x;i++){
			Object sa=new SimpleObject();
			methO=sa;
		}
		return methO;
	}
	
	public void fun(int x){
		int sum=0;
		for(int i=0;i<x;i++){
			sum=sum+i;		
		}
		for(int j=0;j<sum;j++){
			Object o=new SimpleObject();
			v.add(o);
		}
	}
}
