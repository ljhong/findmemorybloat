package test.cases;

import java.io.File;
import java.util.Enumeration;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.zip.ZipEntry;

import jqian.testing.MemoryTester;

public class Main2 {

		  public void test(File file) throws Exception {

		    JarFile jarFile = null;
		    Enumeration<JarEntry> enumeration = null;
		    try {
		      jarFile = new JarFile(file);
		      enumeration = jarFile.entries();
		      do {
		        if (!enumeration.hasMoreElements()) {
		          break;
		        }
		        ZipEntry zipentry = enumeration.nextElement();
		      } while (true);
		    } finally {
		      jarFile.close();
		    }
		  }

		  public static void main(String... s) throws Exception {

		    final Main2 r = new Main2();
		    final File jarFile = new File( " c:\\tmp\\test.jar " );
		    
		    long freeMemoryBefore=MemoryTester.getUsedMemory();
			
		    for (int i = 0; i < 10; i++) {
		      Thread t = new Thread(new Runnable() {

		        @Override
		        public void run() {
		          while (true) {
		            try {
		              r.test(jarFile);
		            } catch (Exception e) {
		              e.printStackTrace();
		            }
		          }
		        }
		      });

		      t.start();
		    }
		    
		    long freeMemoryAfter=MemoryTester.getUsedMemory();
			long usedMemory=freeMemoryBefore-freeMemoryAfter;
			MemoryTester.assertMemoryConsumation("String","subStringr",usedMemory);
		    
		  }
		  
}
