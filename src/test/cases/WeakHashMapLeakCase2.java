package test.cases;

import java.util.Map;
import java.util.WeakHashMap;

public class WeakHashMapLeakCase2 {
	
	private static WeakHashMap cache = new WeakHashMap();
	
	public static void main(String []args){
		SimpleObject oldO = new SimpleObject("case2");
		ter(oldO);
	}
	
	public static void ter(SimpleObject keyObj){
//		String s = keyObj.getS();
		SimpleObject valueObj = new SimpleObject(keyObj.getS(),5);
		cache.put(keyObj, valueObj);
	}


}
