package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class ASCIICaseInsensitiveComparatorTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testcompare(){

		sun.misc.ASCIICaseInsensitiveComparator test=new sun.misc.ASCIICaseInsensitiveComparator();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.compare(null,null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.misc.ASCIICaseInsensitiveComparator","compare",usedMemory);
	}

