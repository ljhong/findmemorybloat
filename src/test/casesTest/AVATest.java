package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class AVATest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testhashCode95(){

		sun.security.x509.AVA test=new sun.security.x509.AVA();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.hashCode();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.security.x509.AVA","hashCode95",usedMemory);
	}

	@Test
	public void testtoRFC2253String(){

		sun.security.x509.AVA test=new sun.security.x509.AVA();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.toRFC2253String(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.security.x509.AVA","toRFC2253String",usedMemory);
	}

