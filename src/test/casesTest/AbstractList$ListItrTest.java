package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class AbstractList$ListItrTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testset102(){

		java.util.AbstractList$ListItr test=new java.util.AbstractList$ListItr();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.set(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.AbstractList$ListItr","set102",usedMemory);
	}

