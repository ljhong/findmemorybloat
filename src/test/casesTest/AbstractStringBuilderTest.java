package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class AbstractStringBuilderTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testappend(){

		java.lang.AbstractStringBuilder test=new java.lang.AbstractStringBuilder();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.append(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.lang.AbstractStringBuilder","append",usedMemory);
	}

	@Test
	public void testappend3(){

		java.lang.AbstractStringBuilder test=new java.lang.AbstractStringBuilder();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.append(MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.lang.AbstractStringBuilder","append3",usedMemory);
	}

}	@Test
	public void testappend(){

		java.lang.AbstractStringBuilder test=new java.lang.AbstractStringBuilder();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.append(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.lang.AbstractStringBuilder","append",usedMemory);
	}

	@Test
	public void testappend4(){

		java.lang.AbstractStringBuilder test=new java.lang.AbstractStringBuilder();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.append(MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.lang.AbstractStringBuilder","append4",usedMemory);
	}

}	@Test
	public void testappend(){

		java.lang.AbstractStringBuilder test=new java.lang.AbstractStringBuilder();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.append(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.lang.AbstractStringBuilder","append",usedMemory);
	}

	@Test
	public void testappend5(){

		java.lang.AbstractStringBuilder test=new java.lang.AbstractStringBuilder();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.append(MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.lang.AbstractStringBuilder","append5",usedMemory);
	}

	@Test
	public void testappend17(){

		java.lang.AbstractStringBuilder test=new java.lang.AbstractStringBuilder();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.append();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.lang.AbstractStringBuilder","append17",usedMemory);
	}

	@Test
	public void testappend26(){

		java.lang.AbstractStringBuilder test=new java.lang.AbstractStringBuilder();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.append(null,MemoryTester.LARGE_INT,MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.lang.AbstractStringBuilder","append26",usedMemory);
	}

	@Test
	public void testappend28(){

		java.lang.AbstractStringBuilder test=new java.lang.AbstractStringBuilder();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.append();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.lang.AbstractStringBuilder","append28",usedMemory);
	}

	@Test
	public void testappend48(){

		java.lang.AbstractStringBuilder test=new java.lang.AbstractStringBuilder();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.append(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.lang.AbstractStringBuilder","append48",usedMemory);
	}

	@Test
	public void testcharAt96(){

		java.lang.AbstractStringBuilder test=new java.lang.AbstractStringBuilder();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.charAt(MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.lang.AbstractStringBuilder","charAt96",usedMemory);
	}

	@Test
	public void testappend156(){

		java.lang.AbstractStringBuilder test=new java.lang.AbstractStringBuilder();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.append();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.lang.AbstractStringBuilder","append156",usedMemory);
	}

	@Test
	public void testappend163(){

		java.lang.AbstractStringBuilder test=new java.lang.AbstractStringBuilder();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.append();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.lang.AbstractStringBuilder","append163",usedMemory);
	}

	@Test
	public void testappendCodePoint(){

		java.lang.AbstractStringBuilder test=new java.lang.AbstractStringBuilder();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.appendCodePoint(MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.lang.AbstractStringBuilder","appendCodePoint",usedMemory);
	}

	@Test
	public void testsetLength(){

		java.lang.AbstractStringBuilder test=new java.lang.AbstractStringBuilder();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.setLength(MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.lang.AbstractStringBuilder","setLength",usedMemory);
	}

	@Test
	public void testdelete(){

		java.lang.AbstractStringBuilder test=new java.lang.AbstractStringBuilder();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.delete(MemoryTester.LARGE_INT,MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.lang.AbstractStringBuilder","delete",usedMemory);
	}

	@Test
	public void testappend(){

		java.lang.AbstractStringBuilder test=new java.lang.AbstractStringBuilder();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.append(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.lang.AbstractStringBuilder","append",usedMemory);
	}

	@Test
	public void testappend5(){

		java.lang.AbstractStringBuilder test=new java.lang.AbstractStringBuilder();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.append(MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.lang.AbstractStringBuilder","append5",usedMemory);
	}

}