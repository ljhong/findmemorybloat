package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class AlgorithmCheckerTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testcheck(){

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		sun.security.provider.certpath.AlgorithmChecker.check(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.security.provider.certpath.AlgorithmChecker","check",usedMemory);
	}

