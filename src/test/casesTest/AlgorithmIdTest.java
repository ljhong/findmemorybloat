package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class AlgorithmIdTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testparse217(){

		sun.security.x509.AlgorithmId test=new sun.security.x509.AlgorithmId();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.parse(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.security.x509.AlgorithmId","parse217",usedMemory);
	}

