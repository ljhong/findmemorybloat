package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class AlgorithmParametersTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testgetInstance207(){

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			java.security.AlgorithmParameters.getInstance(null,null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.security.AlgorithmParameters","getInstance207",usedMemory);
	}

	@Test
	public void testgetInstance215(){

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			java.security.AlgorithmParameters.getInstance(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.security.AlgorithmParameters","getInstance215",usedMemory);
	}

