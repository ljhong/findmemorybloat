package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class AllPermissionCollectionTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testadd276(){

		java.security.AllPermissionCollection test=new java.security.AllPermissionCollection();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.add(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.security.AllPermissionCollection","add276",usedMemory);
	}

	@Test
	public void testelements301(){

		java.security.AllPermissionCollection test=new java.security.AllPermissionCollection();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.elements();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.security.AllPermissionCollection","elements301",usedMemory);
	}

