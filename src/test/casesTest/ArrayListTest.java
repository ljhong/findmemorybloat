package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class ArrayListTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testensureCapacity(){

		java.util.ArrayList test=new java.util.ArrayList();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.ensureCapacity(MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.ArrayList","ensureCapacity",usedMemory);
	}

	@Test
	public void testadd(){

		java.util.ArrayList test=new java.util.ArrayList();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.add(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.ArrayList","add",usedMemory);
	}

}	@Test
	public void testget38(){

		java.util.ArrayList test=new java.util.ArrayList();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			test.get(MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.ArrayList","get38",usedMemory);
	}

	@Test
	public void testensureCapacity(){

		java.util.ArrayList test=new java.util.ArrayList();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.ensureCapacity(MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.ArrayList","ensureCapacity",usedMemory);
	}

	@Test
	public void testtoArray81(){

		java.util.ArrayList test=new java.util.ArrayList();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.toArray();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.ArrayList","toArray81",usedMemory);
	}

	@Test
	public void testaddAll(){

		java.util.ArrayList test=new java.util.ArrayList();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.addAll(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.ArrayList","addAll",usedMemory);
	}

	@Test
	public void testtoArray82(){

		java.util.ArrayList test=new java.util.ArrayList();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.toArray(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.ArrayList","toArray82",usedMemory);
	}

	@Test
	public void testset99(){

		java.util.ArrayList test=new java.util.ArrayList();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.set(MemoryTester.LARGE_INT,null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.ArrayList","set99",usedMemory);
	}

	@Test
	public void testadd110(){

		java.util.ArrayList test=new java.util.ArrayList();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.add(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.ArrayList","add110",usedMemory);
	}

	@Test
	public void testremove177(){

		java.util.ArrayList test=new java.util.ArrayList();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.remove(MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.ArrayList","remove177",usedMemory);
	}

	@Test
	public void testadd197(){

		java.util.ArrayList test=new java.util.ArrayList();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.add(MemoryTester.LARGE_INT,null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.ArrayList","add197",usedMemory);
	}

	@Test
	public void testensureCapacity(){

		java.util.ArrayList test=new java.util.ArrayList();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.ensureCapacity(MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.ArrayList","ensureCapacity",usedMemory);
	}

	@Test
	public void testadd(){

		java.util.ArrayList test=new java.util.ArrayList();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.add(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.ArrayList","add",usedMemory);
	}

}