package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class ArraysTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testcopyOf(){

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		java.util.Arrays.copyOf(null,MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.Arrays","copyOf",usedMemory);
	}

}	@Test
	public void testcopyOf(){

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		java.util.Arrays.copyOf(null,MemoryTester.LARGE_INT,null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.Arrays","copyOf",usedMemory);
	}

	@Test
	public void testcopyOf1(){

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		java.util.Arrays.copyOf(null,MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.Arrays","copyOf1",usedMemory);
	}

	@Test
	public void testcopyOf6(){

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		java.util.Arrays.copyOf(null,MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.Arrays","copyOf6",usedMemory);
	}

}	@Test
	public void testcopyOf(){

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		java.util.Arrays.copyOf(null,MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.Arrays","copyOf",usedMemory);
	}

	@Test
	public void testcopyOf10(){

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		java.util.Arrays.copyOf(null,MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.Arrays","copyOf10",usedMemory);
	}

	@Test
	public void testcopyOf30(){

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		java.util.Arrays.copyOf(null,MemoryTester.LARGE_INT,null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.Arrays","copyOf30",usedMemory);
	}

	@Test
	public void testcopyOf31(){

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		java.util.Arrays.copyOf(null,MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.Arrays","copyOf31",usedMemory);
	}

	@Test
	public void testsort(){

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		java.util.Arrays.sort(null,null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.Arrays","sort",usedMemory);
	}

	@Test
	public void testasList(){

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		java.util.Arrays.asList(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.Arrays","asList",usedMemory);
	}

	@Test
	public void testcopyOf322(){

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		java.util.Arrays.copyOf(null,MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.Arrays","copyOf322",usedMemory);
	}

	@Test
	public void testcopyOf(){

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		java.util.Arrays.copyOf(null,MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.Arrays","copyOf",usedMemory);
	}

	@Test
	public void testcopyOf1(){

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		java.util.Arrays.copyOf(null,MemoryTester.LARGE_INT,null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.Arrays","copyOf1",usedMemory);
	}

	@Test
	public void testcopyOf2(){

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		java.util.Arrays.copyOf(null,MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.Arrays","copyOf2",usedMemory);
	}

}