package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class AtomicReferenceFieldUpdater$AtomicReferenceFieldUpdaterImplTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testcompareAndSet(){

		java.util.concurrent.atomic.AtomicReferenceFieldUpdater$AtomicReferenceFieldUpdaterImpl test=new java.util.concurrent.atomic.AtomicReferenceFieldUpdater$AtomicReferenceFieldUpdaterImpl();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.compareAndSet(null,null,null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.concurrent.atomic.AtomicReferenceFieldUpdater$AtomicReferenceFieldUpdaterImpl","compareAndSet",usedMemory);
	}

