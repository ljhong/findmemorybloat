package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class AttributedString$AttributeMapTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testentrySet122(){

		java.text.AttributedString$AttributeMap test=new java.text.AttributedString$AttributeMap();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.entrySet();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.text.AttributedString$AttributeMap","entrySet122",usedMemory);
	}

	@Test
	public void testget123(){

		java.text.AttributedString$AttributeMap test=new java.text.AttributedString$AttributeMap();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			test.get(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.text.AttributedString$AttributeMap","get123",usedMemory);
	}

