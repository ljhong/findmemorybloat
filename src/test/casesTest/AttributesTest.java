package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class AttributesTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testget136(){

		java.util.jar.Attributes test=new java.util.jar.Attributes();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			test.get(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.jar.Attributes","get136",usedMemory);
	}

	@Test
	public void testput190(){

		java.util.jar.Attributes test=new java.util.jar.Attributes();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.put(null,null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.jar.Attributes","put190",usedMemory);
	}

	@Test
	public void testputValue(){

		java.util.jar.Attributes test=new java.util.jar.Attributes();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.putValue(null,null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.jar.Attributes","putValue",usedMemory);
	}

