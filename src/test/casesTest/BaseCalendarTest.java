package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class BaseCalendarTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testgetCalendarDateFromFixedDate(){

		sun.util.calendar.BaseCalendar test=new sun.util.calendar.BaseCalendar();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			test.getCalendarDateFromFixedDate(null,);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.util.calendar.BaseCalendar","getCalendarDateFromFixedDate",usedMemory);
	}

