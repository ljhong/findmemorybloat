package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class BasicPermissionCollectionTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testadd275(){

		java.security.BasicPermissionCollection test=new java.security.BasicPermissionCollection();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.add(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.security.BasicPermissionCollection","add275",usedMemory);
	}

	@Test
	public void testelements298(){

		java.security.BasicPermissionCollection test=new java.security.BasicPermissionCollection();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.elements();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.security.BasicPermissionCollection","elements298",usedMemory);
	}

	@Test
	public void testimplies315(){

		java.security.BasicPermissionCollection test=new java.security.BasicPermissionCollection();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.implies(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.security.BasicPermissionCollection","implies315",usedMemory);
	}

