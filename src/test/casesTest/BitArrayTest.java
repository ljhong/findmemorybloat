package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class BitArrayTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testset229(){

		sun.security.util.BitArray test=new sun.security.util.BitArray();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.set(MemoryTester.LARGE_INT,);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.security.util.BitArray","set229",usedMemory);
	}

	@Test
	public void testget230(){

		sun.security.util.BitArray test=new sun.security.util.BitArray();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			test.get(MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.security.util.BitArray","get230",usedMemory);
	}

	@Test
	public void testtoBooleanArray(){

		sun.security.util.BitArray test=new sun.security.util.BitArray();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.toBooleanArray();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.security.util.BitArray","toBooleanArray",usedMemory);
	}

