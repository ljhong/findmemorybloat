package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class BufferTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testposition(){

		java.nio.Buffer test=new java.nio.Buffer();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.position(MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.nio.Buffer","position",usedMemory);
	}

	@Test
	public void testlimit(){

		java.nio.Buffer test=new java.nio.Buffer();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.limit(MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.nio.Buffer","limit",usedMemory);
	}

