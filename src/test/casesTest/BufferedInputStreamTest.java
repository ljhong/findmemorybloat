package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class BufferedInputStreamTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testavailable63(){

		java.io.BufferedInputStream test=new java.io.BufferedInputStream();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.available();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.io.BufferedInputStream","available63",usedMemory);
	}

	@Test
	public void testclose70(){

		java.io.BufferedInputStream test=new java.io.BufferedInputStream();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.close();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.io.BufferedInputStream","close70",usedMemory);
	}

