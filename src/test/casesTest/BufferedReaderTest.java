package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class BufferedReaderTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testready284(){

		java.io.BufferedReader test=new java.io.BufferedReader();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.ready();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.io.BufferedReader","ready284",usedMemory);
	}

	@Test
	public void testread286(){

		java.io.BufferedReader test=new java.io.BufferedReader();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.read(null,MemoryTester.LARGE_INT,MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.io.BufferedReader","read286",usedMemory);
	}

	@Test
	public void testread287(){

		java.io.BufferedReader test=new java.io.BufferedReader();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.read();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.io.BufferedReader","read287",usedMemory);
	}

