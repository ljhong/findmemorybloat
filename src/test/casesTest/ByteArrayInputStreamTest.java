package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class ByteArrayInputStreamTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testread(){

		java.io.ByteArrayInputStream test=new java.io.ByteArrayInputStream();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.read(null,MemoryTester.LARGE_INT,MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.io.ByteArrayInputStream","read",usedMemory);
	}

