package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class ByteBufferTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testarrayOffset(){

		java.nio.ByteBuffer test=new java.nio.ByteBuffer();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.arrayOffset();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.nio.ByteBuffer","arrayOffset",usedMemory);
	}

	@Test
	public void testarray(){

		java.nio.ByteBuffer test=new java.nio.ByteBuffer();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.array();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.nio.ByteBuffer","array",usedMemory);
	}

	@Test
	public void testwrap234(){

		java.nio.ByteBuffer test=new java.nio.ByteBuffer();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.wrap(null,MemoryTester.LARGE_INT,MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.nio.ByteBuffer","wrap234",usedMemory);
	}

	@Test
	public void testwrap235(){

		java.nio.ByteBuffer test=new java.nio.ByteBuffer();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.wrap(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.nio.ByteBuffer","wrap235",usedMemory);
	}

	@Test
	public void testallocate246(){

		java.nio.ByteBuffer test=new java.nio.ByteBuffer();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.allocate(MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.nio.ByteBuffer","allocate246",usedMemory);
	}

	@Test
	public void testput248(){

		java.nio.ByteBuffer test=new java.nio.ByteBuffer();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.put(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.nio.ByteBuffer","put248",usedMemory);
	}

	@Test
	public void testtoString252(){

		java.nio.ByteBuffer test=new java.nio.ByteBuffer();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.toString();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.nio.ByteBuffer","toString252",usedMemory);
	}

