package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class CRC32Test {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testupdate(){

		java.util.zip.CRC32 test=new java.util.zip.CRC32();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.update(null,MemoryTester.LARGE_INT,MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.zip.CRC32","update",usedMemory);
	}

