package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class CalendarUtilsTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testsprintf0d(){

		sun.util.calendar.CalendarUtils test=new sun.util.calendar.CalendarUtils();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.sprintf0d(null,MemoryTester.LARGE_INT,MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.util.calendar.CalendarUtils","sprintf0d",usedMemory);
	}

