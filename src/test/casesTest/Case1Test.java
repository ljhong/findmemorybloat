package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class Case1Test {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testgetObjects1(){

		test.temp.Case1 test=new test.temp.Case1();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			test.getObjects1();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("test.cases.Case1","getObjects1",usedMemory);
	}


}	@Test
	public void testgetObjects4(){

		test.temp.Case1 test=new test.temp.Case1();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			test.getObjects4();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("test.cases.Case1","getObjects4",usedMemory);
	}

}