package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class Case4Test {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testfun7(){

		test.cases.Case4 test=new test.cases.Case4();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.fun7(MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("test.cases.Case4","fun7",usedMemory);
	}
	
	@Test
	public void testsubString(){
//		int buffersize = MemoryTester.buffersize;
//		StringBuffer strbuf = new StringBuffer(buffersize);
		
//		String test = MemoryTester.getlongstring();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++){
			String test =  MemoryTester.getlongstring();
			String str = test.substring(0,10);
		}
		
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;
		
		MemoryTester.assertMemoryConsumation("java.lang.String","subString",usedMemory);
		
	}

}