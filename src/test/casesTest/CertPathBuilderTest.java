package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class CertPathBuilderTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testgetInstance261(){

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			java.security.cert.CertPathBuilder.getInstance(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.security.cert.CertPathBuilder","getInstance261",usedMemory);
	}

