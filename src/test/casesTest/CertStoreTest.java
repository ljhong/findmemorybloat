package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class CertStoreTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testgetInstance265(){

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			java.security.cert.CertStore.getInstance(null,null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.security.cert.CertStore","getInstance265",usedMemory);
	}

