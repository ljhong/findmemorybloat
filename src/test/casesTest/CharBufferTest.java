package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class CharBufferTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testwrap(){

		java.nio.CharBuffer test=new java.nio.CharBuffer();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.wrap(null,MemoryTester.LARGE_INT,MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.nio.CharBuffer","wrap",usedMemory);
	}

	@Test
	public void testallocate(){

		java.nio.CharBuffer test=new java.nio.CharBuffer();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.allocate(MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.nio.CharBuffer","allocate",usedMemory);
	}

	@Test
	public void testwrap232(){

		java.nio.CharBuffer test=new java.nio.CharBuffer();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.wrap(null,MemoryTester.LARGE_INT,MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.nio.CharBuffer","wrap232",usedMemory);
	}

	@Test
	public void testwrap233(){

		java.nio.CharBuffer test=new java.nio.CharBuffer();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.wrap(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.nio.CharBuffer","wrap233",usedMemory);
	}

	@Test
	public void testarray236(){

		java.nio.CharBuffer test=new java.nio.CharBuffer();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.array();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.nio.CharBuffer","array236",usedMemory);
	}

	@Test
	public void testarrayOffset237(){

		java.nio.CharBuffer test=new java.nio.CharBuffer();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.arrayOffset();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.nio.CharBuffer","arrayOffset237",usedMemory);
	}

	@Test
	public void testput241(){

		java.nio.CharBuffer test=new java.nio.CharBuffer();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.put(null,MemoryTester.LARGE_INT,MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.nio.CharBuffer","put241",usedMemory);
	}

	@Test
	public void testput242(){

		java.nio.CharBuffer test=new java.nio.CharBuffer();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.put(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.nio.CharBuffer","put242",usedMemory);
	}

