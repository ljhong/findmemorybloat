package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class CharTrieTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testgetSurrogateValue(){

		sun.text.normalizer.CharTrie test=new sun.text.normalizer.CharTrie();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			test.getSurrogateValue(,);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.text.normalizer.CharTrie","getSurrogateValue",usedMemory);
	}

	@Test
	public void testputIndexData(){

		sun.text.normalizer.CharTrie test=new sun.text.normalizer.CharTrie();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.putIndexData(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.text.normalizer.CharTrie","putIndexData",usedMemory);
	}

