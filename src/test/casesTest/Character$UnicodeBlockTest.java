package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class Character$UnicodeBlockTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testforName324(){

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		java.lang.Character$UnicodeBlock.forName(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.lang.Character$UnicodeBlock","forName324",usedMemory);
	}

