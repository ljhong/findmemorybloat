package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class CharsetTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testforName(){

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		java.nio.charset.Charset.forName(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.nio.charset.Charset","forName",usedMemory);
	}

	@Test
	public void testisSupported(){

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		java.nio.charset.Charset.isSupported(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.nio.charset.Charset","isSupported",usedMemory);
	}

	@Test
	public void testdefaultCharset(){

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		java.nio.charset.Charset.defaultCharset();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.nio.charset.Charset","defaultCharset",usedMemory);
	}

