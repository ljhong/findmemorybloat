package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class ClassLoaderTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testgetSystemClassLoader(){

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			java.lang.ClassLoader.getSystemClassLoader();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.lang.ClassLoader","getSystemClassLoader",usedMemory);
	}

