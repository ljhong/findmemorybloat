package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class CoderResultTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testmalformedForLength(){

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		java.nio.charset.CoderResult.malformedForLength(MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.nio.charset.CoderResult","malformedForLength",usedMemory);
	}

