package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class CollectionCertStoreParametersTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testclone262(){

		java.security.cert.CollectionCertStoreParameters test=new java.security.cert.CollectionCertStoreParameters();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.clone();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.security.cert.CollectionCertStoreParameters","clone262",usedMemory);
	}

