package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class Collections$EmptySet$1Test {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testnext94(){

		java.util.Collections$EmptySet$1 test=new java.util.Collections$EmptySet$1();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.next();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.Collections$EmptySet$1","next94",usedMemory);
	}

	@Test
	public void testremove228(){

		java.util.Collections$EmptySet$1 test=new java.util.Collections$EmptySet$1();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.remove();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.Collections$EmptySet$1","remove228",usedMemory);
	}

