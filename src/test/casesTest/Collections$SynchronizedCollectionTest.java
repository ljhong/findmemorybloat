package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class Collections$SynchronizedCollectionTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testiterator130(){

		java.util.Collections$SynchronizedCollection test=new java.util.Collections$SynchronizedCollection();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.iterator();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.Collections$SynchronizedCollection","iterator130",usedMemory);
	}

	@Test
	public void testsize134(){

		java.util.Collections$SynchronizedCollection test=new java.util.Collections$SynchronizedCollection();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.size();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.Collections$SynchronizedCollection","size134",usedMemory);
	}

