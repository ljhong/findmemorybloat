package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class Collections$SynchronizedMapTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testput318(){

		java.util.Collections$SynchronizedMap test=new java.util.Collections$SynchronizedMap();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.put(null,null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.Collections$SynchronizedMap","put318",usedMemory);
	}

