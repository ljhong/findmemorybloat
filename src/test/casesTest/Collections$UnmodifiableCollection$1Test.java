package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class Collections$UnmodifiableCollection$1Test {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testnext149(){

		java.util.Collections$UnmodifiableCollection$1 test=new java.util.Collections$UnmodifiableCollection$1();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.next();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.Collections$UnmodifiableCollection$1","next149",usedMemory);
	}

