package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class Collections$UnmodifiableCollectionTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testiterator143(){

		java.util.Collections$UnmodifiableCollection test=new java.util.Collections$UnmodifiableCollection();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.iterator();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.Collections$UnmodifiableCollection","iterator143",usedMemory);
	}

