package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class Collections$UnmodifiableListTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testget172(){

		java.util.Collections$UnmodifiableList test=new java.util.Collections$UnmodifiableList();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			test.get(MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.Collections$UnmodifiableList","get172",usedMemory);
	}

