package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class Collections$UnmodifiableMap$UnmodifiableEntrySet$1Test {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testnext150(){

		java.util.Collections$UnmodifiableMap$UnmodifiableEntrySet$1 test=new java.util.Collections$UnmodifiableMap$UnmodifiableEntrySet$1();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.next();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.Collections$UnmodifiableMap$UnmodifiableEntrySet$1","next150",usedMemory);
	}

	@Test
	public void testnext151(){

		java.util.Collections$UnmodifiableMap$UnmodifiableEntrySet$1 test=new java.util.Collections$UnmodifiableMap$UnmodifiableEntrySet$1();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.next();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.Collections$UnmodifiableMap$UnmodifiableEntrySet$1","next151",usedMemory);
	}

