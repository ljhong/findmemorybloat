package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class Collections$UnmodifiableMap$UnmodifiableEntrySet$UnmodifiableEntryTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testgetValue(){

		java.util.Collections$UnmodifiableMap$UnmodifiableEntrySet$UnmodifiableEntry test=new java.util.Collections$UnmodifiableMap$UnmodifiableEntrySet$UnmodifiableEntry();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			test.getValue();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.Collections$UnmodifiableMap$UnmodifiableEntrySet$UnmodifiableEntry","getValue",usedMemory);
	}

	@Test
	public void testgetKey(){

		java.util.Collections$UnmodifiableMap$UnmodifiableEntrySet$UnmodifiableEntry test=new java.util.Collections$UnmodifiableMap$UnmodifiableEntrySet$UnmodifiableEntry();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			test.getKey();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.Collections$UnmodifiableMap$UnmodifiableEntrySet$UnmodifiableEntry","getKey",usedMemory);
	}

