package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class Collections$UnmodifiableMap$UnmodifiableEntrySetTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testiterator144(){

		java.util.Collections$UnmodifiableMap$UnmodifiableEntrySet test=new java.util.Collections$UnmodifiableMap$UnmodifiableEntrySet();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.iterator();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.Collections$UnmodifiableMap$UnmodifiableEntrySet","iterator144",usedMemory);
	}

