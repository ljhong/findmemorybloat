package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class Collections$UnmodifiableMapTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testentrySet139(){

		java.util.Collections$UnmodifiableMap test=new java.util.Collections$UnmodifiableMap();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.entrySet();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.Collections$UnmodifiableMap","entrySet139",usedMemory);
	}

