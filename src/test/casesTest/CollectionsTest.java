package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class CollectionsTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testsort109(){

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		java.util.Collections.sort(null,null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.Collections","sort109",usedMemory);
	}

	@Test
	public void testunmodifiableMap(){

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		java.util.Collections.unmodifiableMap(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.Collections","unmodifiableMap",usedMemory);
	}

	@Test
	public void testsingleton(){

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		java.util.Collections.singleton(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.Collections","singleton",usedMemory);
	}

	@Test
	public void testsingletonList(){

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		java.util.Collections.singletonList(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.Collections","singletonList",usedMemory);
	}

	@Test
	public void testenumeration(){

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		java.util.Collections.enumeration(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.Collections","enumeration",usedMemory);
	}

