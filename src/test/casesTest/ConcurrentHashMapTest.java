package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class ConcurrentHashMapTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testget84(){

		java.util.concurrent.ConcurrentHashMap test=new java.util.concurrent.ConcurrentHashMap();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			test.get(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.concurrent.ConcurrentHashMap","get84",usedMemory);
	}

	@Test
	public void testputIfAbsent(){

		java.util.concurrent.ConcurrentHashMap test=new java.util.concurrent.ConcurrentHashMap();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.putIfAbsent(null,null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.concurrent.ConcurrentHashMap","putIfAbsent",usedMemory);
	}

	@Test
	public void testput(){

		java.util.concurrent.ConcurrentHashMap test=new java.util.concurrent.ConcurrentHashMap();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.put(null,null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.concurrent.ConcurrentHashMap","put",usedMemory);
	}

	@Test
	public void testremove(){

		java.util.concurrent.ConcurrentHashMap test=new java.util.concurrent.ConcurrentHashMap();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.remove(null,null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.concurrent.ConcurrentHashMap","remove",usedMemory);
	}

	@Test
	public void testremove173(){

		java.util.concurrent.ConcurrentHashMap test=new java.util.concurrent.ConcurrentHashMap();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.remove(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.concurrent.ConcurrentHashMap","remove173",usedMemory);
	}

