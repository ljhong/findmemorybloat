package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class DataInputStreamTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testreadFully(){

		java.io.DataInputStream test=new java.io.DataInputStream();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.readFully(null,MemoryTester.LARGE_INT,MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.io.DataInputStream","readFully",usedMemory);
	}

