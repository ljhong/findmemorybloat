package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class DebugTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testprintln(){

		sun.security.util.Debug test=new sun.security.util.Debug();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.println(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.security.util.Debug","println",usedMemory);
	}

	@Test
	public void testgetInstance308(){

		sun.security.util.Debug test=new sun.security.util.Debug();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			test.getInstance(null,null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.security.util.Debug","getInstance308",usedMemory);
	}

	@Test
	public void testgetInstance309(){

		sun.security.util.Debug test=new sun.security.util.Debug();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			test.getInstance(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.security.util.Debug","getInstance309",usedMemory);
	}

