package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class DerInputBufferTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testgetGeneralizedTime(){

		sun.security.util.DerInputBuffer test=new sun.security.util.DerInputBuffer();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			test.getGeneralizedTime(MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.security.util.DerInputBuffer","getGeneralizedTime",usedMemory);
	}

	@Test
	public void testgetInteger(){

		sun.security.util.DerInputBuffer test=new sun.security.util.DerInputBuffer();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			test.getInteger(MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.security.util.DerInputBuffer","getInteger",usedMemory);
	}

