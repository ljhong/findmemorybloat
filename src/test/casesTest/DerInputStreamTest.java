package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class DerInputStreamTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testgetBytes(){

		sun.security.util.DerInputStream test=new sun.security.util.DerInputStream();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			test.getBytes(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.security.util.DerInputStream","getBytes",usedMemory);
	}

	@Test
	public void testgetGeneralizedTime193(){

		sun.security.util.DerInputStream test=new sun.security.util.DerInputStream();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			test.getGeneralizedTime();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.security.util.DerInputStream","getGeneralizedTime193",usedMemory);
	}

	@Test
	public void testgetOID216(){

		sun.security.util.DerInputStream test=new sun.security.util.DerInputStream();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			test.getOID();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.security.util.DerInputStream","getOID216",usedMemory);
	}

	@Test
	public void testgetDerValue(){

		sun.security.util.DerInputStream test=new sun.security.util.DerInputStream();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			test.getDerValue();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.security.util.DerInputStream","getDerValue",usedMemory);
	}

	@Test
	public void testgetInteger219(){

		sun.security.util.DerInputStream test=new sun.security.util.DerInputStream();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			test.getInteger();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.security.util.DerInputStream","getInteger219",usedMemory);
	}

	@Test
	public void testgetOctetString(){

		sun.security.util.DerInputStream test=new sun.security.util.DerInputStream();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			test.getOctetString();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.security.util.DerInputStream","getOctetString",usedMemory);
	}

	@Test
	public void testgetBigInteger(){

		sun.security.util.DerInputStream test=new sun.security.util.DerInputStream();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			test.getBigInteger();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.security.util.DerInputStream","getBigInteger",usedMemory);
	}

	@Test
	public void testsubStream(){

		sun.security.util.DerInputStream test=new sun.security.util.DerInputStream();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.subStream(MemoryTester.LARGE_INT,);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.security.util.DerInputStream","subStream",usedMemory);
	}

	@Test
	public void testgetSequence(){

		sun.security.util.DerInputStream test=new sun.security.util.DerInputStream();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			test.getSequence(MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.security.util.DerInputStream","getSequence",usedMemory);
	}

	@Test
	public void testgetSet(){

		sun.security.util.DerInputStream test=new sun.security.util.DerInputStream();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			test.getSet(MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.security.util.DerInputStream","getSet",usedMemory);
	}

	@Test
	public void testtoByteArray254(){

		sun.security.util.DerInputStream test=new sun.security.util.DerInputStream();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.toByteArray();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.security.util.DerInputStream","toByteArray254",usedMemory);
	}

