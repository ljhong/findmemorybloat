package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class DerValueTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testgetOID(){

		sun.security.util.DerValue test=new sun.security.util.DerValue();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			test.getOID();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.security.util.DerValue","getOID",usedMemory);
	}

	@Test
	public void testgetDataBytes(){

		sun.security.util.DerValue test=new sun.security.util.DerValue();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			test.getDataBytes();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.security.util.DerValue","getDataBytes",usedMemory);
	}

	@Test
	public void testgetUTF8String(){

		sun.security.util.DerValue test=new sun.security.util.DerValue();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			test.getUTF8String();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.security.util.DerValue","getUTF8String",usedMemory);
	}

	@Test
	public void testgetPrintableString(){

		sun.security.util.DerValue test=new sun.security.util.DerValue();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			test.getPrintableString();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.security.util.DerValue","getPrintableString",usedMemory);
	}

	@Test
	public void testgetT61String(){

		sun.security.util.DerValue test=new sun.security.util.DerValue();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			test.getT61String();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.security.util.DerValue","getT61String",usedMemory);
	}

	@Test
	public void testgetIA5String(){

		sun.security.util.DerValue test=new sun.security.util.DerValue();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			test.getIA5String();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.security.util.DerValue","getIA5String",usedMemory);
	}

	@Test
	public void testgetBMPString(){

		sun.security.util.DerValue test=new sun.security.util.DerValue();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			test.getBMPString();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.security.util.DerValue","getBMPString",usedMemory);
	}

	@Test
	public void testgetGeneralString(){

		sun.security.util.DerValue test=new sun.security.util.DerValue();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			test.getGeneralString();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.security.util.DerValue","getGeneralString",usedMemory);
	}

	@Test
	public void testgetAsString(){

		sun.security.util.DerValue test=new sun.security.util.DerValue();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			test.getAsString();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.security.util.DerValue","getAsString",usedMemory);
	}

	@Test
	public void testtoString(){

		sun.security.util.DerValue test=new sun.security.util.DerValue();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.toString();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.security.util.DerValue","toString",usedMemory);
	}

	@Test
	public void testhashCode(){

		sun.security.util.DerValue test=new sun.security.util.DerValue();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.hashCode();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.security.util.DerValue","hashCode",usedMemory);
	}

	@Test
	public void testencode(){

		sun.security.util.DerValue test=new sun.security.util.DerValue();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.encode(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.security.util.DerValue","encode",usedMemory);
	}

	@Test
	public void testtoByteArray45(){

		sun.security.util.DerValue test=new sun.security.util.DerValue();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.toByteArray();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.security.util.DerValue","toByteArray45",usedMemory);
	}

	@Test
	public void testtoDerInputStream(){

		sun.security.util.DerValue test=new sun.security.util.DerValue();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.toDerInputStream();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.security.util.DerValue","toDerInputStream",usedMemory);
	}

	@Test
	public void testgetUnalignedBitString(){

		sun.security.util.DerValue test=new sun.security.util.DerValue();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			test.getUnalignedBitString();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.security.util.DerValue","getUnalignedBitString",usedMemory);
	}

	@Test
	public void testgetOctetString255(){

		sun.security.util.DerValue test=new sun.security.util.DerValue();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			test.getOctetString();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.security.util.DerValue","getOctetString255",usedMemory);
	}

	@Test
	public void testgetInteger257(){

		sun.security.util.DerValue test=new sun.security.util.DerValue();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			test.getInteger();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.security.util.DerValue","getInteger257",usedMemory);
	}

