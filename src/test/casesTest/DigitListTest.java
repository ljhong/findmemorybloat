package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class DigitListTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testset(){

		java.text.DigitList test=new java.text.DigitList();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.set(,,MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.text.DigitList","set",usedMemory);
	}

