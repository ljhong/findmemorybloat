package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class DownloadManagerTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testgetKernelJREDir(){

		sun.jkernel.DownloadManager test=new sun.jkernel.DownloadManager();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			test.getKernelJREDir();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.jkernel.DownloadManager","getKernelJREDir",usedMemory);
	}

	@Test
	public void testgetDebugProperty(){

		sun.jkernel.DownloadManager test=new sun.jkernel.DownloadManager();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			test.getDebugProperty();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.jkernel.DownloadManager","getDebugProperty",usedMemory);
	}

	@Test
	public void testdownloadFile(){

		sun.jkernel.DownloadManager test=new sun.jkernel.DownloadManager();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.downloadFile(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.jkernel.DownloadManager","downloadFile",usedMemory);
	}

	@Test
	public void testisCurrentThreadDownloading(){

		sun.jkernel.DownloadManager test=new sun.jkernel.DownloadManager();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.isCurrentThreadDownloading();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.jkernel.DownloadManager","isCurrentThreadDownloading",usedMemory);
	}

	@Test
	public void testgetBootClassPathEntryForResource(){

		sun.jkernel.DownloadManager test=new sun.jkernel.DownloadManager();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			test.getBootClassPathEntryForResource(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.jkernel.DownloadManager","getBootClassPathEntryForResource",usedMemory);
	}

	@Test
	public void testgetBootClassPathEntryForClass(){

		sun.jkernel.DownloadManager test=new sun.jkernel.DownloadManager();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			test.getBootClassPathEntryForClass(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.jkernel.DownloadManager","getBootClassPathEntryForClass",usedMemory);
	}

