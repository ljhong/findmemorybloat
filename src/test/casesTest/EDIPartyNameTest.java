package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class EDIPartyNameTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testconstrains117(){

		sun.security.x509.EDIPartyName test=new sun.security.x509.EDIPartyName();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.constrains(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.security.x509.EDIPartyName","constrains117",usedMemory);
	}

