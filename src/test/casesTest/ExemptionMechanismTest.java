package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class ExemptionMechanismTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testgetInstance293(){

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			javax.crypto.ExemptionMechanism.getInstance(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("javax.crypto.ExemptionMechanism","getInstance293",usedMemory);
	}

