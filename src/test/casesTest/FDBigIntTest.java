package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class FDBigIntTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testmult(){

		sun.misc.FDBigInt test=new sun.misc.FDBigInt();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.mult(MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.misc.FDBigInt","mult",usedMemory);
	}

	@Test
	public void testmult160(){

		sun.misc.FDBigInt test=new sun.misc.FDBigInt();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.mult(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.misc.FDBigInt","mult160",usedMemory);
	}

	@Test
	public void testlshiftMe(){

		sun.misc.FDBigInt test=new sun.misc.FDBigInt();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.lshiftMe(MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.misc.FDBigInt","lshiftMe",usedMemory);
	}

	@Test
	public void testnormalizeMe(){

		sun.misc.FDBigInt test=new sun.misc.FDBigInt();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.normalizeMe();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.misc.FDBigInt","normalizeMe",usedMemory);
	}

	@Test
	public void testquoRemIteration(){

		sun.misc.FDBigInt test=new sun.misc.FDBigInt();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.quoRemIteration(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.misc.FDBigInt","quoRemIteration",usedMemory);
	}

	@Test
	public void testadd162(){

		sun.misc.FDBigInt test=new sun.misc.FDBigInt();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.add(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.misc.FDBigInt","add162",usedMemory);
	}

