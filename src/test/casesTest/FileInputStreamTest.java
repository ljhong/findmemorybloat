package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class FileInputStreamTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testclose(){

		java.io.FileInputStream test=new java.io.FileInputStream();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.close();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.io.FileInputStream","close",usedMemory);
	}

