package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class FilePermissionCollectionTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testadd273(){

		java.io.FilePermissionCollection test=new java.io.FilePermissionCollection();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.add(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.io.FilePermissionCollection","add273",usedMemory);
	}

	@Test
	public void testelements296(){

		java.io.FilePermissionCollection test=new java.io.FilePermissionCollection();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.elements();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.io.FilePermissionCollection","elements296",usedMemory);
	}

	@Test
	public void testimplies313(){

		java.io.FilePermissionCollection test=new java.io.FilePermissionCollection();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.implies(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.io.FilePermissionCollection","implies313",usedMemory);
	}

