package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class FloatingDecimalTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testappendTo(){

		sun.misc.FloatingDecimal test=new sun.misc.FloatingDecimal();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.appendTo(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.misc.FloatingDecimal","appendTo",usedMemory);
	}

