package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class GZIPInputStreamTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testread169(){

		java.util.zip.GZIPInputStream test=new java.util.zip.GZIPInputStream();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.read(null,MemoryTester.LARGE_INT,MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.zip.GZIPInputStream","read169",usedMemory);
	}

