package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class GeneralSubtreesTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testadd258(){

		sun.security.x509.GeneralSubtrees test=new sun.security.x509.GeneralSubtrees();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.add(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.security.x509.GeneralSubtrees","add258",usedMemory);
	}

