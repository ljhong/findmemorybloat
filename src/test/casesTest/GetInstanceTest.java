package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class GetInstanceTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testgetService203(){

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			sun.security.jca.GetInstance.getService(null,null,null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.security.jca.GetInstance","getService203",usedMemory);
	}

	@Test
	public void testcheckSuperClass(){

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		sun.security.jca.GetInstance.checkSuperClass(null,null,null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.security.jca.GetInstance","checkSuperClass",usedMemory);
	}

	@Test
	public void testgetInstance205(){

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			sun.security.jca.GetInstance.getInstance(null,null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.security.jca.GetInstance","getInstance205",usedMemory);
	}

	@Test
	public void testgetInstance206(){

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			sun.security.jca.GetInstance.getInstance(null,null,null,null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.security.jca.GetInstance","getInstance206",usedMemory);
	}

	@Test
	public void testgetService208(){

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			sun.security.jca.GetInstance.getService(null,null,null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.security.jca.GetInstance","getService208",usedMemory);
	}

	@Test
	public void testgetInstance209(){

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			sun.security.jca.GetInstance.getInstance(null,null,null,null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.security.jca.GetInstance","getInstance209",usedMemory);
	}

	@Test
	public void testgetInstance214(){

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			sun.security.jca.GetInstance.getInstance(null,null,null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.security.jca.GetInstance","getInstance214",usedMemory);
	}

	@Test
	public void testgetServices220(){

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			sun.security.jca.GetInstance.getServices(null,null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.security.jca.GetInstance","getServices220",usedMemory);
	}

	@Test
	public void testgetServices222(){

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			sun.security.jca.GetInstance.getServices(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.security.jca.GetInstance","getServices222",usedMemory);
	}

	@Test
	public void testgetInstance263(){

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			sun.security.jca.GetInstance.getInstance(null,null,null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.security.jca.GetInstance","getInstance263",usedMemory);
	}

	@Test
	public void testgetInstance264(){

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			sun.security.jca.GetInstance.getInstance(null,null,null,null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.security.jca.GetInstance","getInstance264",usedMemory);
	}

