package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class GregorianCalendarTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testsetTimeZone(){

		java.util.GregorianCalendar test=new java.util.GregorianCalendar();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.setTimeZone(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.GregorianCalendar","setTimeZone",usedMemory);
	}

