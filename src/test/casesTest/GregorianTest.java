package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class GregorianTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testnewCalendarDate(){

		sun.util.calendar.Gregorian test=new sun.util.calendar.Gregorian();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.newCalendarDate(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.util.calendar.Gregorian","newCalendarDate",usedMemory);
	}

	@Test
	public void testnewCalendarDate192(){

		sun.util.calendar.Gregorian test=new sun.util.calendar.Gregorian();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.newCalendarDate(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.util.calendar.Gregorian","newCalendarDate192",usedMemory);
	}

