package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class HashMap$HashIteratorTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testremove200(){

		java.util.HashMap$HashIterator test=new java.util.HashMap$HashIterator();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.remove();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.HashMap$HashIterator","remove200",usedMemory);
	}

