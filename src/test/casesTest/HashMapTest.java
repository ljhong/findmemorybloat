package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class HashMapTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testentrySet(){

		java.util.HashMap test=new java.util.HashMap();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.entrySet();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.HashMap","entrySet",usedMemory);
	}

	@Test
	public void testkeySet(){

		java.util.HashMap test=new java.util.HashMap();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.keySet();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.HashMap","keySet",usedMemory);
	}

	@Test
	public void testvalues(){

		java.util.HashMap test=new java.util.HashMap();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.values();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.HashMap","values",usedMemory);
	}

	@Test
	public void testremove201(){

		java.util.HashMap test=new java.util.HashMap();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.remove(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.HashMap","remove201",usedMemory);
	}

