package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class HashSetTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testiterator89(){

		java.util.HashSet test=new java.util.HashSet();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.iterator();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.HashSet","iterator89",usedMemory);
	}

	@Test
	public void testadd121(){

		java.util.HashSet test=new java.util.HashSet();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.add(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.HashSet","add121",usedMemory);
	}

