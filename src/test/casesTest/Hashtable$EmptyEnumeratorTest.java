package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class Hashtable$EmptyEnumeratorTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testnextElement300(){

		java.util.Hashtable$EmptyEnumerator test=new java.util.Hashtable$EmptyEnumerator();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.nextElement();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.Hashtable$EmptyEnumerator","nextElement300",usedMemory);
	}

