package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class Hashtable$EmptyIteratorTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testnext131(){

		java.util.Hashtable$EmptyIterator test=new java.util.Hashtable$EmptyIterator();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.next();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.Hashtable$EmptyIterator","next131",usedMemory);
	}

