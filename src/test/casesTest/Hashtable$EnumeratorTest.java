package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class Hashtable$EnumeratorTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testnextElement132(){

		java.util.Hashtable$Enumerator test=new java.util.Hashtable$Enumerator();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.nextElement();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.Hashtable$Enumerator","nextElement132",usedMemory);
	}

	@Test
	public void testnext133(){

		java.util.Hashtable$Enumerator test=new java.util.Hashtable$Enumerator();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.next();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.Hashtable$Enumerator","next133",usedMemory);
	}

