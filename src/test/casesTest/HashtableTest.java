package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class HashtableTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testentrySet126(){

		java.util.Hashtable test=new java.util.Hashtable();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.entrySet();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.Hashtable","entrySet126",usedMemory);
	}

	@Test
	public void testequals152(){

		java.util.Hashtable test=new java.util.Hashtable();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.equals(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.Hashtable","equals152",usedMemory);
	}

	@Test
	public void testput170(){

		java.util.Hashtable test=new java.util.Hashtable();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.put(null,null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.Hashtable","put170",usedMemory);
	}

	@Test
	public void testremove186(){

		java.util.Hashtable test=new java.util.Hashtable();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.remove(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.Hashtable","remove186",usedMemory);
	}

	@Test
	public void testelements299(){

		java.util.Hashtable test=new java.util.Hashtable();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.elements();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.Hashtable","elements299",usedMemory);
	}

