package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class ICUDataTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testgetRequiredStream(){

		sun.text.normalizer.ICUData test=new sun.text.normalizer.ICUData();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			test.getRequiredStream(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.text.normalizer.ICUData","getRequiredStream",usedMemory);
	}

