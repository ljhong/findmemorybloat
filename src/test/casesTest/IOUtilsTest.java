package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class IOUtilsTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testreadFully218(){

		sun.misc.IOUtils test=new sun.misc.IOUtils();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.readFully(null,MemoryTester.LARGE_INT,);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.misc.IOUtils","readFully218",usedMemory);
	}

