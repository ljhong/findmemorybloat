package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class IPAddressUtilTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testconvertFromIPv4MappedAddress(){

		sun.net.util.IPAddressUtil test=new sun.net.util.IPAddressUtil();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.convertFromIPv4MappedAddress(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.net.util.IPAddressUtil","convertFromIPv4MappedAddress",usedMemory);
	}

	@Test
	public void testtextToNumericFormatV6(){

		sun.net.util.IPAddressUtil test=new sun.net.util.IPAddressUtil();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.textToNumericFormatV6(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.net.util.IPAddressUtil","textToNumericFormatV6",usedMemory);
	}

