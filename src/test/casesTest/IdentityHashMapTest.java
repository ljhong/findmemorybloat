package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class IdentityHashMapTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testremove227(){

		java.util.IdentityHashMap test=new java.util.IdentityHashMap();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.remove(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.IdentityHashMap","remove227",usedMemory);
	}

	@Test
	public void testput292(){

		java.util.IdentityHashMap test=new java.util.IdentityHashMap();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.put(null,null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.IdentityHashMap","put292",usedMemory);
	}

