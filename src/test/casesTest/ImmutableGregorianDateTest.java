package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class ImmutableGregorianDateTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testsetNormalizedYear(){

		sun.util.calendar.ImmutableGregorianDate test=new sun.util.calendar.ImmutableGregorianDate();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.setNormalizedYear(MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.util.calendar.ImmutableGregorianDate","setNormalizedYear",usedMemory);
	}

	@Test
	public void testsetMonth(){

		sun.util.calendar.ImmutableGregorianDate test=new sun.util.calendar.ImmutableGregorianDate();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.setMonth(MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.util.calendar.ImmutableGregorianDate","setMonth",usedMemory);
	}

