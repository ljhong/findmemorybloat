package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class Inet6AddressTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testgetHostAddress(){

		java.net.Inet6Address test=new java.net.Inet6Address();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			test.getHostAddress();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.net.Inet6Address","getHostAddress",usedMemory);
	}

