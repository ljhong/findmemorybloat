package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class InetAddressTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testgetAllByName(){

		java.net.InetAddress test=new java.net.InetAddress();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			test.getAllByName(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.net.InetAddress","getAllByName",usedMemory);
	}

	@Test
	public void testgetByName256(){

		java.net.InetAddress test=new java.net.InetAddress();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			test.getByName(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.net.InetAddress","getByName256",usedMemory);
	}

	@Test
	public void testgetByAddress(){

		java.net.InetAddress test=new java.net.InetAddress();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			test.getByAddress(null,null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.net.InetAddress","getByAddress",usedMemory);
	}

	@Test
	public void testgetByAddress310(){

		java.net.InetAddress test=new java.net.InetAddress();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			test.getByAddress(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.net.InetAddress","getByAddress310",usedMemory);
	}

