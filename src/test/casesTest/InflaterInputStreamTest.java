package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class InflaterInputStreamTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testclose66(){

		java.util.zip.InflaterInputStream test=new java.util.zip.InflaterInputStream();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.close();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.zip.InflaterInputStream","close66",usedMemory);
	}

	@Test
	public void testread75(){

		java.util.zip.InflaterInputStream test=new java.util.zip.InflaterInputStream();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.read(null,MemoryTester.LARGE_INT,MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.zip.InflaterInputStream","read75",usedMemory);
	}

	@Test
	public void testread165(){

		java.util.zip.InflaterInputStream test=new java.util.zip.InflaterInputStream();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.read();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.zip.InflaterInputStream","read165",usedMemory);
	}

