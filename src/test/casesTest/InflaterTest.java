package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class InflaterTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testgetBytesWritten(){

		java.util.zip.Inflater test=new java.util.zip.Inflater();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			test.getBytesWritten();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.zip.Inflater","getBytesWritten",usedMemory);
	}

	@Test
	public void testsetInput(){

		java.util.zip.Inflater test=new java.util.zip.Inflater();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.setInput(null,MemoryTester.LARGE_INT,MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.zip.Inflater","setInput",usedMemory);
	}

	@Test
	public void testreset(){

		java.util.zip.Inflater test=new java.util.zip.Inflater();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.reset();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.zip.Inflater","reset",usedMemory);
	}

	@Test
	public void testend(){

		java.util.zip.Inflater test=new java.util.zip.Inflater();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.end();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.zip.Inflater","end",usedMemory);
	}

	@Test
	public void testinflate(){

		java.util.zip.Inflater test=new java.util.zip.Inflater();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.inflate(null,MemoryTester.LARGE_INT,MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.zip.Inflater","inflate",usedMemory);
	}

	@Test
	public void testgetBytesRead(){

		java.util.zip.Inflater test=new java.util.zip.Inflater();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			test.getBytesRead();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.zip.Inflater","getBytesRead",usedMemory);
	}

