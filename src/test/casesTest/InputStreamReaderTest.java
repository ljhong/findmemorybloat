package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class InputStreamReaderTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testread282(){

		java.io.InputStreamReader test=new java.io.InputStreamReader();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.read();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.io.InputStreamReader","read282",usedMemory);
	}

	@Test
	public void testready283(){

		java.io.InputStreamReader test=new java.io.InputStreamReader();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.ready();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.io.InputStreamReader","ready283",usedMemory);
	}

	@Test
	public void testread285(){

		java.io.InputStreamReader test=new java.io.InputStreamReader();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.read(null,MemoryTester.LARGE_INT,MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.io.InputStreamReader","read285",usedMemory);
	}

