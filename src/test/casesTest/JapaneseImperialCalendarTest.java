package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class JapaneseImperialCalendarTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testsetTimeZone24(){

		java.util.JapaneseImperialCalendar test=new java.util.JapaneseImperialCalendar();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.setTimeZone(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.JapaneseImperialCalendar","setTimeZone24",usedMemory);
	}

