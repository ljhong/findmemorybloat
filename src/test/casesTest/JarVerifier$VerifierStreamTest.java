package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class JarVerifier$VerifierStreamTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testread279(){

		java.util.jar.JarVerifier$VerifierStream test=new java.util.jar.JarVerifier$VerifierStream();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.read(null,MemoryTester.LARGE_INT,MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.jar.JarVerifier$VerifierStream","read279",usedMemory);
	}

