package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class Launcher$AppClassLoaderTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testloadClass(){

		sun.misc.Launcher$AppClassLoader test=new sun.misc.Launcher$AppClassLoader();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.loadClass(null,);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.misc.Launcher$AppClassLoader","loadClass",usedMemory);
	}

