package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class LevelTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testparse(){

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		java.util.logging.Level.parse(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.logging.Level","parse",usedMemory);
	}

