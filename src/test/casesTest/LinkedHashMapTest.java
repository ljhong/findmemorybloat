package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class LinkedHashMapTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testget195(){

		java.util.LinkedHashMap test=new java.util.LinkedHashMap();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			test.get(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.LinkedHashMap","get195",usedMemory);
	}

	@Test
	public void testclear(){

		java.util.LinkedHashMap test=new java.util.LinkedHashMap();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.clear();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.LinkedHashMap","clear",usedMemory);
	}

