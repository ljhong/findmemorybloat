package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class LinkedList$ListItrTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testnext36(){

		java.util.LinkedList$ListItr test=new java.util.LinkedList$ListItr();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.next();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.LinkedList$ListItr","next36",usedMemory);
	}

	@Test
	public void testset107(){

		java.util.LinkedList$ListItr test=new java.util.LinkedList$ListItr();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.set(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.LinkedList$ListItr","set107",usedMemory);
	}

