package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class LinkedListTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testlistIterator(){

		java.util.LinkedList test=new java.util.LinkedList();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.listIterator(MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.LinkedList","listIterator",usedMemory);
	}

	@Test
	public void testget40(){

		java.util.LinkedList test=new java.util.LinkedList();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			test.get(MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.LinkedList","get40",usedMemory);
	}

	@Test
	public void testtoArray80(){

		java.util.LinkedList test=new java.util.LinkedList();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.toArray();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.LinkedList","toArray80",usedMemory);
	}

	@Test
	public void testset100(){

		java.util.LinkedList test=new java.util.LinkedList();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.set(MemoryTester.LARGE_INT,null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.LinkedList","set100",usedMemory);
	}

	@Test
	public void testremove179(){

		java.util.LinkedList test=new java.util.LinkedList();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.remove(MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.LinkedList","remove179",usedMemory);
	}

	@Test
	public void testadd268(){

		java.util.LinkedList test=new java.util.LinkedList();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.add(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.LinkedList","add268",usedMemory);
	}

