package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class LocaleData$LocaleDataResourceBundleControlTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testgetFallbackLocale(){

		sun.util.resources.LocaleData$LocaleDataResourceBundleControl test=new sun.util.resources.LocaleData$LocaleDataResourceBundleControl();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			test.getFallbackLocale(null,null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.util.resources.LocaleData$LocaleDataResourceBundleControl","getFallbackLocale",usedMemory);
	}

	@Test
	public void testgetCandidateLocales182(){

		sun.util.resources.LocaleData$LocaleDataResourceBundleControl test=new sun.util.resources.LocaleData$LocaleDataResourceBundleControl();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			test.getCandidateLocales(null,null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.util.resources.LocaleData$LocaleDataResourceBundleControl","getCandidateLocales182",usedMemory);
	}

