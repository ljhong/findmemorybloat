package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class LocaleDataMetaInfoTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testgetSupportedLocaleString(){

		sun.util.LocaleDataMetaInfo test=new sun.util.LocaleDataMetaInfo();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			test.getSupportedLocaleString(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.util.LocaleDataMetaInfo","getSupportedLocaleString",usedMemory);
	}

