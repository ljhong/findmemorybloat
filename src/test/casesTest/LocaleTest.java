package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class LocaleTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testgetDefault(){

		java.util.Locale test=new java.util.Locale();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			test.getDefault();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.Locale","getDefault",usedMemory);
	}

	@Test
	public void testtoString175(){

		java.util.Locale test=new java.util.Locale();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.toString();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.Locale","toString175",usedMemory);
	}

