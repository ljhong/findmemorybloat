package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class LockSupportTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testunpark(){

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		java.util.concurrent.locks.LockSupport.unpark(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.concurrent.locks.LockSupport","unpark",usedMemory);
	}

	@Test
	public void testpark(){

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		java.util.concurrent.locks.LockSupport.park(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.concurrent.locks.LockSupport","park",usedMemory);
	}

