package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;
import java.util.logging.Logger;

public class LoggerTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testgetLogger188(){

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			Logger.getAnonymousLogger();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.logging.Logger","getLogger188",usedMemory);
	}
}

