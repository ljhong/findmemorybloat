package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class MalformedInputExceptionTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testgetMessage(){

		java.nio.charset.MalformedInputException test=new java.nio.charset.MalformedInputException();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			test.getMessage();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.nio.charset.MalformedInputException","getMessage",usedMemory);
	}

