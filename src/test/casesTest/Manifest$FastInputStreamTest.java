package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class Manifest$FastInputStreamTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testreadLine(){

		java.util.jar.Manifest$FastInputStream test=new java.util.jar.Manifest$FastInputStream();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.readLine(null,MemoryTester.LARGE_INT,MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.jar.Manifest$FastInputStream","readLine",usedMemory);
	}

	@Test
	public void testreadLine189(){

		java.util.jar.Manifest$FastInputStream test=new java.util.jar.Manifest$FastInputStream();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.readLine(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.jar.Manifest$FastInputStream","readLine189",usedMemory);
	}

