package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class ManifestEntryVerifierTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testupdate79(){

		sun.security.util.ManifestEntryVerifier test=new sun.security.util.ManifestEntryVerifier();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.update(null,MemoryTester.LARGE_INT,MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.security.util.ManifestEntryVerifier","update79",usedMemory);
	}

