package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class ManifestTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testgetAttributes(){

		java.util.jar.Manifest test=new java.util.jar.Manifest();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			test.getAttributes(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.jar.Manifest","getAttributes",usedMemory);
	}

	@Test
	public void testread191(){

		java.util.jar.Manifest test=new java.util.jar.Manifest();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.read(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.jar.Manifest","read191",usedMemory);
	}

