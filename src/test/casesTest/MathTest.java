package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class MathTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testfloor161(){

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		java.lang.Math.floor(MemoryTester.LARGE_LONG);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.lang.Math","floor161",usedMemory);
	}

