package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class NetscapeCertTypeExtensionTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testget231(){

		sun.security.x509.NetscapeCertTypeExtension test=new sun.security.x509.NetscapeCertTypeExtension();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			test.get(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.security.x509.NetscapeCertTypeExtension","get231",usedMemory);
	}

