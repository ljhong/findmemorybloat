package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class NormalizerImplTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testconvert(){

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		sun.text.normalizer.NormalizerImpl.convert(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.text.normalizer.NormalizerImpl","convert",usedMemory);
	}

	@Test
	public void testgetNorm32FromSurrogatePair(){

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			sun.text.normalizer.NormalizerImpl.getNorm32FromSurrogatePair(,);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.text.normalizer.NormalizerImpl","getNorm32FromSurrogatePair",usedMemory);
	}

	@Test
	public void testdecompose(){

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		sun.text.normalizer.NormalizerImpl.decompose(null,MemoryTester.LARGE_INT,MemoryTester.LARGE_INT,null,MemoryTester.LARGE_INT,MemoryTester.LARGE_INT,,null,null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.text.normalizer.NormalizerImpl","decompose",usedMemory);
	}

	@Test
	public void testisTrueStarter(){

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		sun.text.normalizer.NormalizerImpl.isTrueStarter(,MemoryTester.LARGE_INT,MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.text.normalizer.NormalizerImpl","isTrueStarter",usedMemory);
	}

	@Test
	public void testcompose(){

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		sun.text.normalizer.NormalizerImpl.compose(null,MemoryTester.LARGE_INT,MemoryTester.LARGE_INT,null,MemoryTester.LARGE_INT,MemoryTester.LARGE_INT,MemoryTester.LARGE_INT,null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.text.normalizer.NormalizerImpl","compose",usedMemory);
	}

	@Test
	public void testaddPropertyStarts60(){

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		sun.text.normalizer.NormalizerImpl.addPropertyStarts(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.text.normalizer.NormalizerImpl","addPropertyStarts60",usedMemory);
	}

