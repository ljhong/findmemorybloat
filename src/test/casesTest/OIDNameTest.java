package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class OIDNameTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testconstrains118(){

		sun.security.x509.OIDName test=new sun.security.x509.OIDName();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.constrains(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.security.x509.OIDName","constrains118",usedMemory);
	}

