package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class ObjectIdentifierTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testtoString98(){

		sun.security.util.ObjectIdentifier test=new sun.security.util.ObjectIdentifier();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.toString();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.security.util.ObjectIdentifier","toString98",usedMemory);
	}

