package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

import java.io.*;
import java.lang.ref.WeakReference;

class Data1 implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	int d=0;
}

public class ObjectOutputStreamTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test(expected = java.io.NotSerializableException.class)
	public void testwriteUnshared() throws Exception{

//		try {
//			OutputStream outstream = new java.io.FileOutputStream("flow");
//			ObjectOutputStream out = new ObjectOutputStream(outstream);
//			Data1 o = new Data1();
//			WeakReference<Object> ref = new WeakReference<Object>(o);
			ObjectOutputStream fixture = new ObjectOutputStream(new ByteArrayOutputStream());

			Object obj = new Data1();
			WeakReference<Object> ref = MemoryTester.prepareArgument(obj);
			fixture.writeUnshared(obj);
			MemoryTester.doSth(obj);

            MemoryTester.assertArgumentNotLeaked(ref);
//           o = null;
//			if(ref.get()!=null)
//				MemoryTester.assertMemoryLeak("writeUnshared");
//			else
//				MemoryTester.assertNoMemoryLeak("writeUnshared");
//		} 
//		catch (FileNotFoundException ex) {
//			ex.printStackTrace();
//		} 
//		catch (IOException ex) {
//			ex.printStackTrace();
//		}

	}


}