package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

import java.io.*;
import java.lang.ref.WeakReference;

class Data implements Serializable{
    int d=0;
}

public class ObjectOutputStreamTestInJunit {
	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testgetObjects1(){
		try {
			OutputStream outstream = new java.io.FileOutputStream("flow");
			ObjectOutputStream out = new ObjectOutputStream(outstream);
			
			Data o = new Data();
			WeakReference<Data> ref = new WeakReference<Data>(o);
	        out.writeUnshared(o);
	        o = null;
	        
	        if(ref.get()!=null){
             	MemoryTester.assertMemoryLeak("writeUnshared");
            }
	        else
	        	MemoryTester.assertNoMemoryLeak("writeUnshared");
	        
		} catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
	}

}
