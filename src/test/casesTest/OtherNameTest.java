package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class OtherNameTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testconstrains119(){

		sun.security.x509.OtherName test=new sun.security.x509.OtherName();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.constrains(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.security.x509.OtherName","constrains119",usedMemory);
	}

