package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class PKIXParametersTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testclone260(){

		java.security.cert.PKIXParameters test=new java.security.cert.PKIXParameters();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.clone();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.security.cert.PKIXParameters","clone260",usedMemory);
	}

