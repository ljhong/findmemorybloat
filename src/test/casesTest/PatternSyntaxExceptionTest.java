package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class PatternSyntaxExceptionTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testgetMessage19(){

		java.util.regex.PatternSyntaxException test=new java.util.regex.PatternSyntaxException();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			test.getMessage();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.regex.PatternSyntaxException","getMessage19",usedMemory);
	}

