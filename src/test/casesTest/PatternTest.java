package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class PatternTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testcompile(){

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		java.util.regex.Pattern.compile(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.regex.Pattern","compile",usedMemory);
	}

