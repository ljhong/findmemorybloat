package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class PermissionsHashTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testadd272(){

		java.security.PermissionsHash test=new java.security.PermissionsHash();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.add(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.security.PermissionsHash","add272",usedMemory);
	}

	@Test
	public void testelements295(){

		java.security.PermissionsHash test=new java.security.PermissionsHash();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.elements();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.security.PermissionsHash","elements295",usedMemory);
	}

	@Test
	public void testimplies312(){

		java.security.PermissionsHash test=new java.security.PermissionsHash();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.implies(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.security.PermissionsHash","implies312",usedMemory);
	}

