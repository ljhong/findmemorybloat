package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class PrintStreamTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testwrite12(){

		java.io.PrintStream test=new java.io.PrintStream();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.write(null,MemoryTester.LARGE_INT,MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.io.PrintStream","write12",usedMemory);
	}

