package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class ProcessEnvironment$CheckedEntrySet$1Test {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testnext147(){

		java.lang.ProcessEnvironment$CheckedEntrySet$1 test=new java.lang.ProcessEnvironment$CheckedEntrySet$1();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.next();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.lang.ProcessEnvironment$CheckedEntrySet$1","next147",usedMemory);
	}

	@Test
	public void testnext148(){

		java.lang.ProcessEnvironment$CheckedEntrySet$1 test=new java.lang.ProcessEnvironment$CheckedEntrySet$1();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.next();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.lang.ProcessEnvironment$CheckedEntrySet$1","next148",usedMemory);
	}

