package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class ProcessEnvironment$CheckedEntrySetTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testiterator142(){

		java.lang.ProcessEnvironment$CheckedEntrySet test=new java.lang.ProcessEnvironment$CheckedEntrySet();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.iterator();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.lang.ProcessEnvironment$CheckedEntrySet","iterator142",usedMemory);
	}

