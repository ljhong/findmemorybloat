package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class PropertiesTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testload(){

		java.util.Properties test=new java.util.Properties();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.load(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.Properties","load",usedMemory);
	}

