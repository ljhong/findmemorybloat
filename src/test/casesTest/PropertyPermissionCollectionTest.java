package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class PropertyPermissionCollectionTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testadd277(){

		java.util.PropertyPermissionCollection test=new java.util.PropertyPermissionCollection();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.add(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.PropertyPermissionCollection","add277",usedMemory);
	}

	@Test
	public void testelements302(){

		java.util.PropertyPermissionCollection test=new java.util.PropertyPermissionCollection();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.elements();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.PropertyPermissionCollection","elements302",usedMemory);
	}

	@Test
	public void testimplies316(){

		java.util.PropertyPermissionCollection test=new java.util.PropertyPermissionCollection();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.implies(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.PropertyPermissionCollection","implies316",usedMemory);
	}

