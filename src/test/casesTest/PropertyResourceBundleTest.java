package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class PropertyResourceBundleTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testhandleGetObject(){

		java.util.PropertyResourceBundle test=new java.util.PropertyResourceBundle();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.handleGetObject(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.PropertyResourceBundle","handleGetObject",usedMemory);
	}

