package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class Provider$ServiceTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testnewInstance204(){

		java.security.Provider$Service test=new java.security.Provider$Service();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.newInstance(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.security.Provider$Service","newInstance204",usedMemory);
	}

	@Test
	public void testgetAttribute(){

		java.security.Provider$Service test=new java.security.Provider$Service();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			test.getAttribute(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.security.Provider$Service","getAttribute",usedMemory);
	}

