package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class ProviderList$ServiceList$1Test {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testhasNext(){

		sun.security.jca.ProviderList$ServiceList$1 test=new sun.security.jca.ProviderList$ServiceList$1();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.hasNext();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.security.jca.ProviderList$ServiceList$1","hasNext",usedMemory);
	}

	@Test
	public void testnext212(){

		sun.security.jca.ProviderList$ServiceList$1 test=new sun.security.jca.ProviderList$ServiceList$1();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.next();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.security.jca.ProviderList$ServiceList$1","next212",usedMemory);
	}

	@Test
	public void testnext213(){

		sun.security.jca.ProviderList$ServiceList$1 test=new sun.security.jca.ProviderList$ServiceList$1();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.next();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.security.jca.ProviderList$ServiceList$1","next213",usedMemory);
	}

