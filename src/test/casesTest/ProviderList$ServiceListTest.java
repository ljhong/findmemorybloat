package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class ProviderList$ServiceListTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testiterator211(){

		sun.security.jca.ProviderList$ServiceList test=new sun.security.jca.ProviderList$ServiceList();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.iterator();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.security.jca.ProviderList$ServiceList","iterator211",usedMemory);
	}

