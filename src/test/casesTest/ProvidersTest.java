package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class ProvidersTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testgetThreadProviderList(){

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			sun.security.jca.Providers.getThreadProviderList();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.security.jca.Providers","getThreadProviderList",usedMemory);
	}

	@Test
	public void testgetProviderList(){

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			sun.security.jca.Providers.getProviderList();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.security.jca.Providers","getProviderList",usedMemory);
	}

