package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class PushbackInputStreamTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testavailable61(){

		java.io.PushbackInputStream test=new java.io.PushbackInputStream();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.available();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.io.PushbackInputStream","available61",usedMemory);
	}

	@Test
	public void testclose69(){

		java.io.PushbackInputStream test=new java.io.PushbackInputStream();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.close();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.io.PushbackInputStream","close69",usedMemory);
	}

	@Test
	public void testread74(){

		java.io.PushbackInputStream test=new java.io.PushbackInputStream();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.read(null,MemoryTester.LARGE_INT,MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.io.PushbackInputStream","read74",usedMemory);
	}

	@Test
	public void testunread(){

		java.io.PushbackInputStream test=new java.io.PushbackInputStream();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.unread(null,MemoryTester.LARGE_INT,MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.io.PushbackInputStream","unread",usedMemory);
	}

	@Test
	public void testread166(){

		java.io.PushbackInputStream test=new java.io.PushbackInputStream();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.read();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.io.PushbackInputStream","read166",usedMemory);
	}

