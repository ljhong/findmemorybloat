package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class RDNTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testtoRFC2253String111(){

		sun.security.x509.RDN test=new sun.security.x509.RDN();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.toRFC2253String();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.security.x509.RDN","toRFC2253String111",usedMemory);
	}

	@Test
	public void testhashCode113(){

		sun.security.x509.RDN test=new sun.security.x509.RDN();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.hashCode();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.security.x509.RDN","hashCode113",usedMemory);
	}

	@Test
	public void testequals115(){

		sun.security.x509.RDN test=new sun.security.x509.RDN();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.equals(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.security.x509.RDN","equals115",usedMemory);
	}

