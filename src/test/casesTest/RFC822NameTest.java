package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class RFC822NameTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testparseName(){

		sun.security.x509.RFC822Name test=new sun.security.x509.RFC822Name();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.parseName(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.security.x509.RFC822Name","parseName",usedMemory);
	}

