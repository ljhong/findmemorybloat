package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class ReentrantLockTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testunlock(){

		java.util.concurrent.locks.ReentrantLock test=new java.util.concurrent.locks.ReentrantLock();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.unlock();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.concurrent.locks.ReentrantLock","unlock",usedMemory);
	}

