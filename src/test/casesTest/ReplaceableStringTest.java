package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class ReplaceableStringTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testcharAt47(){

		sun.text.normalizer.ReplaceableString test=new sun.text.normalizer.ReplaceableString();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.charAt(MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.text.normalizer.ReplaceableString","charAt47",usedMemory);
	}

