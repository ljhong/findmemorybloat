package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class ResourceBundle$CacheKeyTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testclone85(){

		java.util.ResourceBundle$CacheKey test=new java.util.ResourceBundle$CacheKey();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.clone();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.ResourceBundle$CacheKey","clone85",usedMemory);
	}

