package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class ResourceBundleTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testgetBundle(){

		java.util.ResourceBundle test=new java.util.ResourceBundle();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			test.getBundle(null,null,null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.ResourceBundle","getBundle",usedMemory);
	}

	@Test
	public void testgetObject(){

		java.util.ResourceBundle test=new java.util.ResourceBundle();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			test.getObject(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.ResourceBundle","getObject",usedMemory);
	}

	@Test
	public void testgetString(){

		java.util.ResourceBundle test=new java.util.ResourceBundle();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			test.getString(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.ResourceBundle","getString",usedMemory);
	}

	@Test
	public void testgetBundle187(){

		java.util.ResourceBundle test=new java.util.ResourceBundle();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			test.getBundle(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.ResourceBundle","getBundle187",usedMemory);
	}

