package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class RuleCharacterIteratorTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testgetPos(){

		sun.text.normalizer.RuleCharacterIterator test=new sun.text.normalizer.RuleCharacterIterator();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			test.getPos(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.text.normalizer.RuleCharacterIterator","getPos",usedMemory);
	}

	@Test
	public void testjumpahead(){

		sun.text.normalizer.RuleCharacterIterator test=new sun.text.normalizer.RuleCharacterIterator();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.jumpahead(MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.text.normalizer.RuleCharacterIterator","jumpahead",usedMemory);
	}

	@Test
	public void testnext57(){

		sun.text.normalizer.RuleCharacterIterator test=new sun.text.normalizer.RuleCharacterIterator();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.next(MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.text.normalizer.RuleCharacterIterator","next57",usedMemory);
	}

	@Test
	public void testsetPos(){

		sun.text.normalizer.RuleCharacterIterator test=new sun.text.normalizer.RuleCharacterIterator();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.setPos(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.text.normalizer.RuleCharacterIterator","setPos",usedMemory);
	}

