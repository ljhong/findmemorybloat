package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class SecurityManagerTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testcheckPackageAccess(){

		java.lang.SecurityManager test=new java.lang.SecurityManager();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.checkPackageAccess(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.lang.SecurityManager","checkPackageAccess",usedMemory);
	}

	@Test
	public void testcheckPropertyAccess(){

		java.lang.SecurityManager test=new java.lang.SecurityManager();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.checkPropertyAccess(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.lang.SecurityManager","checkPropertyAccess",usedMemory);
	}

	@Test
	public void testcheckLink(){

		java.lang.SecurityManager test=new java.lang.SecurityManager();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.checkLink(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.lang.SecurityManager","checkLink",usedMemory);
	}

	@Test
	public void testcheckWrite(){

		java.lang.SecurityManager test=new java.lang.SecurityManager();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.checkWrite(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.lang.SecurityManager","checkWrite",usedMemory);
	}

