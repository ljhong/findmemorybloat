package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class SequenceInputStreamTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testread73(){

		java.io.SequenceInputStream test=new java.io.SequenceInputStream();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.read(null,MemoryTester.LARGE_INT,MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.io.SequenceInputStream","read73",usedMemory);
	}

	@Test
	public void testread168(){

		java.io.SequenceInputStream test=new java.io.SequenceInputStream();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.read();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.io.SequenceInputStream","read168",usedMemory);
	}

