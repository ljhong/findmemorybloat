package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class SocketPermissionCollectionTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testadd274(){

		java.net.SocketPermissionCollection test=new java.net.SocketPermissionCollection();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.add(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.net.SocketPermissionCollection","add274",usedMemory);
	}

	@Test
	public void testelements297(){

		java.net.SocketPermissionCollection test=new java.net.SocketPermissionCollection();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.elements();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.net.SocketPermissionCollection","elements297",usedMemory);
	}

	@Test
	public void testimplies314(){

		java.net.SocketPermissionCollection test=new java.net.SocketPermissionCollection();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.implies(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.net.SocketPermissionCollection","implies314",usedMemory);
	}

