package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class SocketPermissionTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testimplies(){

		java.net.SocketPermission test=new java.net.SocketPermission();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.implies(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.net.SocketPermission","implies",usedMemory);
	}

