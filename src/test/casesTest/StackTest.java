package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class StackTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testpush(){

		java.util.Stack test=new java.util.Stack();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.push(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.Stack","push",usedMemory);
	}

	@Test
	public void testpeek(){

		java.util.Stack test=new java.util.Stack();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.peek();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.Stack","peek",usedMemory);
	}

	@Test
	public void testpop(){

		java.util.Stack test=new java.util.Stack();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.pop();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.Stack","pop",usedMemory);
	}

