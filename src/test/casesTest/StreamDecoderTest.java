package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class StreamDecoderTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testforInputStreamReader(){

		sun.nio.cs.StreamDecoder test=new sun.nio.cs.StreamDecoder();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.forInputStreamReader(null,null,null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.nio.cs.StreamDecoder","forInputStreamReader",usedMemory);
	}

	@Test
	public void testread280(){

		sun.nio.cs.StreamDecoder test=new sun.nio.cs.StreamDecoder();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.read(null,MemoryTester.LARGE_INT,MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.nio.cs.StreamDecoder","read280",usedMemory);
	}

	@Test
	public void testread281(){

		sun.nio.cs.StreamDecoder test=new sun.nio.cs.StreamDecoder();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.read();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.nio.cs.StreamDecoder","read281",usedMemory);
	}

	@Test
	public void testready(){

		sun.nio.cs.StreamDecoder test=new sun.nio.cs.StreamDecoder();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.ready();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.nio.cs.StreamDecoder","ready",usedMemory);
	}

