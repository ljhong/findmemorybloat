package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class StreamTokenizerTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testnextToken288(){

		java.io.StreamTokenizer test=new java.io.StreamTokenizer();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.nextToken();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.io.StreamTokenizer","nextToken288",usedMemory);
	}

