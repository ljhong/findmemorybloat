package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class StrictMathTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testfloor(){

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		java.lang.StrictMath.floor(MemoryTester.LARGE_LONG);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.lang.StrictMath","floor",usedMemory);
	}

