package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class StringBufferTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testappend2(){

		java.lang.StringBuffer test=new java.lang.StringBuffer();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.append(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.lang.StringBuffer","append2",usedMemory);
	}

	@Test
	public void testappend3(){

		java.lang.StringBuffer test=new java.lang.StringBuffer();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.append(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.lang.StringBuffer","append3",usedMemory);
	}

	@Test
	public void testappend15(){

		java.lang.StringBuffer test=new java.lang.StringBuffer();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.append(MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.lang.StringBuffer","append15",usedMemory);
	}

	@Test
	public void testappend18(){

		java.lang.StringBuffer test=new java.lang.StringBuffer();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.append();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.lang.StringBuffer","append18",usedMemory);
	}

	@Test
	public void testappend27(){

		java.lang.StringBuffer test=new java.lang.StringBuffer();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.append(null,MemoryTester.LARGE_INT,MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.lang.StringBuffer","append27",usedMemory);
	}

	@Test
	public void testappend29(){

		java.lang.StringBuffer test=new java.lang.StringBuffer();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.append();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.lang.StringBuffer","append29",usedMemory);
	}

	@Test
	public void testcharAt(){

		java.lang.StringBuffer test=new java.lang.StringBuffer();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.charAt(MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.lang.StringBuffer","charAt",usedMemory);
	}

	@Test
	public void testappend49(){

		java.lang.StringBuffer test=new java.lang.StringBuffer();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.append(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.lang.StringBuffer","append49",usedMemory);
	}

