package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class StringBuilderTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testappend1(){

		java.lang.StringBuilder test=new java.lang.StringBuilder();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.append(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.lang.StringBuilder","append1",usedMemory);
	}

	@Test
	public void testappend2(){

		java.lang.StringBuilder test=new java.lang.StringBuilder();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.append(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.lang.StringBuilder","append2",usedMemory);
	}

	@Test
	public void testappend4(){

		java.lang.StringBuilder test=new java.lang.StringBuilder();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.append(MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.lang.StringBuilder","append4",usedMemory);
	}

}	@Test
	public void testappend2(){

		java.lang.StringBuilder test=new java.lang.StringBuilder();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.append(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.lang.StringBuilder","append2",usedMemory);
	}

	@Test
	public void testappend3(){

		java.lang.StringBuilder test=new java.lang.StringBuilder();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.append(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.lang.StringBuilder","append3",usedMemory);
	}

	@Test
	public void testappend5(){

		java.lang.StringBuilder test=new java.lang.StringBuilder();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.append(MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.lang.StringBuilder","append5",usedMemory);
	}

}	@Test
	public void testappend1(){

		java.lang.StringBuilder test=new java.lang.StringBuilder();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.append(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.lang.StringBuilder","append1",usedMemory);
	}

	@Test
	public void testappend4(){

		java.lang.StringBuilder test=new java.lang.StringBuilder();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.append(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.lang.StringBuilder","append4",usedMemory);
	}

	@Test
	public void testappend6(){

		java.lang.StringBuilder test=new java.lang.StringBuilder();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.append(MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.lang.StringBuilder","append6",usedMemory);
	}

	@Test
	public void testappend7(){

		java.lang.StringBuilder test=new java.lang.StringBuilder();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.append(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.lang.StringBuilder","append7",usedMemory);
	}

	@Test
	public void testappend55(){

		java.lang.StringBuilder test=new java.lang.StringBuilder();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.append();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.lang.StringBuilder","append55",usedMemory);
	}

	@Test
	public void testappend76(){

		java.lang.StringBuilder test=new java.lang.StringBuilder();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.append();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.lang.StringBuilder","append76",usedMemory);
	}

	@Test
	public void testcharAt97(){

		java.lang.StringBuilder test=new java.lang.StringBuilder();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.charAt(MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.lang.StringBuilder","charAt97",usedMemory);
	}

	@Test
	public void testappend157(){

		java.lang.StringBuilder test=new java.lang.StringBuilder();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.append();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.lang.StringBuilder","append157",usedMemory);
	}

	@Test
	public void testappend159(){

		java.lang.StringBuilder test=new java.lang.StringBuilder();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.append(null,MemoryTester.LARGE_INT,MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.lang.StringBuilder","append159",usedMemory);
	}

	@Test
	public void testappend164(){

		java.lang.StringBuilder test=new java.lang.StringBuilder();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.append();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.lang.StringBuilder","append164",usedMemory);
	}

	@Test
	public void testappendCodePoint319(){

		java.lang.StringBuilder test=new java.lang.StringBuilder();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.appendCodePoint(MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.lang.StringBuilder","appendCodePoint319",usedMemory);
	}

	@Test
	public void testsetLength320(){

		java.lang.StringBuilder test=new java.lang.StringBuilder();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.setLength(MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.lang.StringBuilder","setLength320",usedMemory);
	}

	@Test
	public void testdelete321(){

		java.lang.StringBuilder test=new java.lang.StringBuilder();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.delete(MemoryTester.LARGE_INT,MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.lang.StringBuilder","delete321",usedMemory);
	}

	@Test
	public void testappend3(){

		java.lang.StringBuilder test=new java.lang.StringBuilder();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.append(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.lang.StringBuilder","append3",usedMemory);
	}

	@Test
	public void testappend4(){

		java.lang.StringBuilder test=new java.lang.StringBuilder();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.append(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.lang.StringBuilder","append4",usedMemory);
	}

	@Test
	public void testappend6(){

		java.lang.StringBuilder test=new java.lang.StringBuilder();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.append(MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.lang.StringBuilder","append6",usedMemory);
	}

}