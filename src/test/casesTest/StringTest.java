package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class StringTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testsubstring(){

		long freeMemoryBefore=MemoryTester.getUsedMemory();

		String longstr = MemoryTester.getlongstring();
		String usestr;

		usestr = longstr.substring(MemoryTester.RandomParaNum,MemoryTester.RandomParaNum);
		longstr = null;

		long freeMemoryAfter=MemoryTester.getUsedMemory();		
		long usedMemory=freeMemoryBefore-freeMemoryAfter;		
		MemoryTester.assertMemoryConsumation("String","substring",usedMemory);
	}

	@Test
	public void testsubstring5(){

		long freeMemoryBefore=MemoryTester.getUsedMemory();

		String longstr = MemoryTester.getlongstring();
		String usestr;

		usestr = longstr.substring(MemoryTester.RandomParaNum);
		longstr = null;

		long freeMemoryAfter=MemoryTester.getUsedMemory();		
		long usedMemory=freeMemoryBefore-freeMemoryAfter;		
		MemoryTester.assertMemoryConsumation("String","substring5",usedMemory);
	}

}