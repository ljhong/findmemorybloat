package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class StringTestInJunit {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testgetObjects1(){

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		String longstr = MemoryTester.getlongstring();

		String subString = longstr.substring(0);
		longstr = null;

		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("test.cases.Case1","getObjects1",usedMemory);
	}


}