package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class StringTokenizerTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testcountTokens(){

		java.util.StringTokenizer test=new java.util.StringTokenizer();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.countTokens();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.StringTokenizer","countTokens",usedMemory);
	}

	@Test
	public void testhasMoreTokens(){

		java.util.StringTokenizer test=new java.util.StringTokenizer();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.hasMoreTokens();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.StringTokenizer","hasMoreTokens",usedMemory);
	}

	@Test
	public void testhasMoreElements(){

		java.util.StringTokenizer test=new java.util.StringTokenizer();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.hasMoreElements();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.StringTokenizer","hasMoreElements",usedMemory);
	}

	@Test
	public void testnextToken(){

		java.util.StringTokenizer test=new java.util.StringTokenizer();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.nextToken();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.StringTokenizer","nextToken",usedMemory);
	}

