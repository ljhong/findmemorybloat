package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class SubList$1Test {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testnext44(){

		java.util.SubList$1 test=new java.util.SubList$1();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.next();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.SubList$1","next44",usedMemory);
	}

	@Test
	public void testset108(){

		java.util.SubList$1 test=new java.util.SubList$1();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.set(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.SubList$1","set108",usedMemory);
	}

