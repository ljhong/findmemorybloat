package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class SubListTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testsize(){

		java.util.SubList test=new java.util.SubList();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.size();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.SubList","size",usedMemory);
	}

	@Test
	public void testlistIterator34(){

		java.util.SubList test=new java.util.SubList();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.listIterator(MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.SubList","listIterator34",usedMemory);
	}

	@Test
	public void testget41(){

		java.util.SubList test=new java.util.SubList();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			test.get(MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.SubList","get41",usedMemory);
	}

	@Test
	public void testset101(){

		java.util.SubList test=new java.util.SubList();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.set(MemoryTester.LARGE_INT,null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.SubList","set101",usedMemory);
	}

	@Test
	public void testremove180(){

		java.util.SubList test=new java.util.SubList();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.remove(MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.SubList","remove180",usedMemory);
	}

