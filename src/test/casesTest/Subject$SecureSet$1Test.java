package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class Subject$SecureSet$1Test {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testnext135(){

		javax.security.auth.Subject$SecureSet$1 test=new javax.security.auth.Subject$SecureSet$1();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.next();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("javax.security.auth.Subject$SecureSet$1","next135",usedMemory);
	}

