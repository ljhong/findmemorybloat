package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class Subject$SecureSetTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testiterator129(){

		javax.security.auth.Subject$SecureSet test=new javax.security.auth.Subject$SecureSet();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.iterator();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("javax.security.auth.Subject$SecureSet","iterator129",usedMemory);
	}

