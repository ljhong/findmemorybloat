package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class SunJCE_dTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testadd278(){

		javax.crypto.SunJCE_d test=new javax.crypto.SunJCE_d();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.add(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("javax.crypto.SunJCE_d","add278",usedMemory);
	}

	@Test
	public void testelements306(){

		javax.crypto.SunJCE_d test=new javax.crypto.SunJCE_d();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.elements();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("javax.crypto.SunJCE_d","elements306",usedMemory);
	}

