package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class SunJCE_fTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testnewPermissionCollection(){

		javax.crypto.SunJCE_f test=new javax.crypto.SunJCE_f();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.newPermissionCollection();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("javax.crypto.SunJCE_f","newPermissionCollection",usedMemory);
	}

