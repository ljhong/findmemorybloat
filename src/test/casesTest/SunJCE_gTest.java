package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class SunJCE_gTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testadd271(){

		javax.crypto.SunJCE_g test=new javax.crypto.SunJCE_g();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.add(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("javax.crypto.SunJCE_g","add271",usedMemory);
	}

	@Test
	public void testelements294(){

		javax.crypto.SunJCE_g test=new javax.crypto.SunJCE_g();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.elements();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("javax.crypto.SunJCE_g","elements294",usedMemory);
	}

	@Test
	public void testimplies317(){

		javax.crypto.SunJCE_g test=new javax.crypto.SunJCE_g();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.implies(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("javax.crypto.SunJCE_g","implies317",usedMemory);
	}

