package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class SunJCE_jTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testhasMoreElements305(){

		javax.crypto.SunJCE_j test=new javax.crypto.SunJCE_j();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.hasMoreElements();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("javax.crypto.SunJCE_j","hasMoreElements305",usedMemory);
	}

	@Test
	public void testnextElement307(){

		javax.crypto.SunJCE_j test=new javax.crypto.SunJCE_j();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.nextElement();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("javax.crypto.SunJCE_j","nextElement307",usedMemory);
	}

