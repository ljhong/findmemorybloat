package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class SunJCE_lTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testadd270(){

		javax.crypto.SunJCE_l test=new javax.crypto.SunJCE_l();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.add(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("javax.crypto.SunJCE_l","add270",usedMemory);
	}

	@Test
	public void testelements304(){

		javax.crypto.SunJCE_l test=new javax.crypto.SunJCE_l();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.elements();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("javax.crypto.SunJCE_l","elements304",usedMemory);
	}

