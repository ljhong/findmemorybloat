package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class SystemTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testgetProperty(){

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			java.lang.System.getProperty(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.lang.System","getProperty",usedMemory);
	}

	@Test
	public void testgetProperty154(){

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			java.lang.System.getProperty(null,null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.lang.System","getProperty154",usedMemory);
	}

	@Test
	public void testloadLibrary(){

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		java.lang.System.loadLibrary(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.lang.System","loadLibrary",usedMemory);
	}

