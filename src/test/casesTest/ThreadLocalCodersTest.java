package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class ThreadLocalCodersTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testencoderFor(){

		sun.nio.cs.ThreadLocalCoders test=new sun.nio.cs.ThreadLocalCoders();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.encoderFor(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.nio.cs.ThreadLocalCoders","encoderFor",usedMemory);
	}

	@Test
	public void testdecoderFor(){

		sun.nio.cs.ThreadLocalCoders test=new sun.nio.cs.ThreadLocalCoders();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.decoderFor(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.nio.cs.ThreadLocalCoders","decoderFor",usedMemory);
	}

