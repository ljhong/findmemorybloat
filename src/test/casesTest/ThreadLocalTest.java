package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class ThreadLocalTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testget(){

		java.lang.ThreadLocal test=new java.lang.ThreadLocal();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			test.get();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.lang.ThreadLocal","get",usedMemory);
	}

	@Test
	public void testset83(){

		java.lang.ThreadLocal test=new java.lang.ThreadLocal();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.set(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.lang.ThreadLocal","set83",usedMemory);
	}

