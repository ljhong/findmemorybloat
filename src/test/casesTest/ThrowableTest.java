package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class ThrowableTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testinitCause(){

		java.lang.Throwable test=new java.lang.Throwable();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.initCause(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.lang.Throwable","initCause",usedMemory);
	}

	@Test
	public void testtoString20(){

		java.lang.Throwable test=new java.lang.Throwable();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.toString();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.lang.Throwable","toString20",usedMemory);
	}

