package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class TimeZoneTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testsetID(){

		java.util.TimeZone test=new java.util.TimeZone();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.setID(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.TimeZone","setID",usedMemory);
	}

	@Test
	public void testgetTimeZone(){

		java.util.TimeZone test=new java.util.TimeZone();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			test.getTimeZone(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.TimeZone","getTimeZone",usedMemory);
	}

