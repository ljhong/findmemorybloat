package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class TreeMap$KeyIteratorTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testnext50(){

		java.util.TreeMap$KeyIterator test=new java.util.TreeMap$KeyIterator();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.next();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.TreeMap$KeyIterator","next50",usedMemory);
	}

