package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class TreeMap$KeySetTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testiterator52(){

		java.util.TreeMap$KeySet test=new java.util.TreeMap$KeySet();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.iterator();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.TreeMap$KeySet","iterator52",usedMemory);
	}

