package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class TreeMapTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testnavigableKeySet(){

		java.util.TreeMap test=new java.util.TreeMap();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.navigableKeySet();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.TreeMap","navigableKeySet",usedMemory);
	}

