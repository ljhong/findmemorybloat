package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class TreeSetTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testiterator53(){

		java.util.TreeSet test=new java.util.TreeSet();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.iterator();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.TreeSet","iterator53",usedMemory);
	}

