package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class TrieIteratorTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testnext59(){

		sun.text.normalizer.TrieIterator test=new sun.text.normalizer.TrieIterator();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.next(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.text.normalizer.TrieIterator","next59",usedMemory);
	}

