package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class UCharacterIteratorTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testgetInstance(){

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			sun.text.normalizer.UCharacterIterator.getInstance(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.text.normalizer.UCharacterIterator","getInstance",usedMemory);
	}

