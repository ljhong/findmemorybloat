package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class UCharacterTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testdigit(){

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		sun.text.normalizer.UCharacter.digit(MemoryTester.LARGE_INT,MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.text.normalizer.UCharacter","digit",usedMemory);
	}

	@Test
	public void testgetType(){

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			sun.text.normalizer.UCharacter.getType(MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.text.normalizer.UCharacter","getType",usedMemory);
	}

	@Test
	public void testgetIntPropertyValue(){

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			sun.text.normalizer.UCharacter.getIntPropertyValue(MemoryTester.LARGE_INT,MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.text.normalizer.UCharacter","getIntPropertyValue",usedMemory);
	}

