package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class URINameTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testnameConstraint(){

		sun.security.x509.URIName test=new sun.security.x509.URIName();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.nameConstraint(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.security.x509.URIName","nameConstraint",usedMemory);
	}

