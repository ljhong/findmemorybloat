package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class URISyntaxExceptionTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testgetMessage16(){

		java.net.URISyntaxException test=new java.net.URISyntaxException();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			test.getMessage();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.net.URISyntaxException","getMessage16",usedMemory);
	}

