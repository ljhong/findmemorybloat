package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class URITest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testgetAuthority(){

		java.net.URI test=new java.net.URI();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			test.getAuthority();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.net.URI","getAuthority",usedMemory);
	}

	@Test
	public void testgetUserInfo(){

		java.net.URI test=new java.net.URI();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			test.getUserInfo();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.net.URI","getUserInfo",usedMemory);
	}

	@Test
	public void testgetPath(){

		java.net.URI test=new java.net.URI();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			test.getPath();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.net.URI","getPath",usedMemory);
	}

	@Test
	public void testgetQuery(){

		java.net.URI test=new java.net.URI();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			test.getQuery();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.net.URI","getQuery",usedMemory);
	}

	@Test
	public void testgetRawSchemeSpecificPart(){

		java.net.URI test=new java.net.URI();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			test.getRawSchemeSpecificPart();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.net.URI","getRawSchemeSpecificPart",usedMemory);
	}

	@Test
	public void testgetSchemeSpecificPart(){

		java.net.URI test=new java.net.URI();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			test.getSchemeSpecificPart();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.net.URI","getSchemeSpecificPart",usedMemory);
	}

