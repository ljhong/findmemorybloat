package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class US_ASCIITest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testnewDecoder(){

		sun.nio.cs.US_ASCII test=new sun.nio.cs.US_ASCII();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.newDecoder();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.nio.cs.US_ASCII","newDecoder",usedMemory);
	}

	@Test
	public void testnewEncoder(){

		sun.nio.cs.US_ASCII test=new sun.nio.cs.US_ASCII();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.newEncoder();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.nio.cs.US_ASCII","newEncoder",usedMemory);
	}

