package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class UTF16Test {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testappend46(){

		sun.text.normalizer.UTF16 test=new sun.text.normalizer.UTF16();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.append(null,MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.text.normalizer.UTF16","append46",usedMemory);
	}

	@Test
	public void testcharAt51(){

		sun.text.normalizer.UTF16 test=new sun.text.normalizer.UTF16();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.charAt(null,MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.text.normalizer.UTF16","charAt51",usedMemory);
	}

	@Test
	public void testcharAt56(){

		sun.text.normalizer.UTF16 test=new sun.text.normalizer.UTF16();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.charAt(null,MemoryTester.LARGE_INT,MemoryTester.LARGE_INT,MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.text.normalizer.UTF16","charAt56",usedMemory);
	}

