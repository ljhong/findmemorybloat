package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class UnicodeSetTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testcontains(){

		sun.text.normalizer.UnicodeSet test=new sun.text.normalizer.UnicodeSet();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.contains(MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.text.normalizer.UnicodeSet","contains",usedMemory);
	}

	@Test
	public void test_generatePattern(){

		sun.text.normalizer.UnicodeSet test=new sun.text.normalizer.UnicodeSet();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test._generatePattern(null,);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.text.normalizer.UnicodeSet","_generatePattern",usedMemory);
	}

	@Test
	public void testcomplement(){

		sun.text.normalizer.UnicodeSet test=new sun.text.normalizer.UnicodeSet();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.complement();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.text.normalizer.UnicodeSet","complement",usedMemory);
	}

	@Test
	public void testadd(){

		sun.text.normalizer.UnicodeSet test=new sun.text.normalizer.UnicodeSet();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.add(MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.text.normalizer.UnicodeSet","add",usedMemory);
	}

	@Test
	public void testadd58(){

		sun.text.normalizer.UnicodeSet test=new sun.text.normalizer.UnicodeSet();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.add(MemoryTester.LARGE_INT,MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.text.normalizer.UnicodeSet","add58",usedMemory);
	}

