package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class UnmappableCharacterExceptionTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testgetMessage14(){

		java.nio.charset.UnmappableCharacterException test=new java.nio.charset.UnmappableCharacterException();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			test.getMessage();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.nio.charset.UnmappableCharacterException","getMessage14",usedMemory);
	}

