package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class UnresolvedPermissionCollectionTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testadd269(){

		java.security.UnresolvedPermissionCollection test=new java.security.UnresolvedPermissionCollection();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.add(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.security.UnresolvedPermissionCollection","add269",usedMemory);
	}

	@Test
	public void testelements303(){

		java.security.UnresolvedPermissionCollection test=new java.security.UnresolvedPermissionCollection();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.elements();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.security.UnresolvedPermissionCollection","elements303",usedMemory);
	}

