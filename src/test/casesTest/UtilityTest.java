package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class UtilityTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testappendNumber(){

		sun.text.normalizer.Utility test=new sun.text.normalizer.Utility();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.appendNumber(null,MemoryTester.LARGE_INT,MemoryTester.LARGE_INT,MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.text.normalizer.Utility","appendNumber",usedMemory);
	}

	@Test
	public void testhex(){

		sun.text.normalizer.Utility test=new sun.text.normalizer.Utility();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.hex(MemoryTester.LARGE_INT,MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.text.normalizer.Utility","hex",usedMemory);
	}

	@Test
	public void testescapeUnprintable(){

		sun.text.normalizer.Utility test=new sun.text.normalizer.Utility();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.escapeUnprintable(null,MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.text.normalizer.Utility","escapeUnprintable",usedMemory);
	}

	@Test
	public void testhex54(){

		sun.text.normalizer.Utility test=new sun.text.normalizer.Utility();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.hex(MemoryTester.LARGE_INT,MemoryTester.LARGE_INT,null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.text.normalizer.Utility","hex54",usedMemory);
	}

	@Test
	public void testescape(){

		sun.text.normalizer.Utility test=new sun.text.normalizer.Utility();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.escape(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.text.normalizer.Utility","escape",usedMemory);
	}

	@Test
	public void testunescapeAt(){

		sun.text.normalizer.Utility test=new sun.text.normalizer.Utility();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.unescapeAt(null,null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.text.normalizer.Utility","unescapeAt",usedMemory);
	}

	@Test
	public void testskipWhitespace(){

		sun.text.normalizer.Utility test=new sun.text.normalizer.Utility();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.skipWhitespace(null,MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.text.normalizer.Utility","skipWhitespace",usedMemory);
	}

