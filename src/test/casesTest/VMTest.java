package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class VMTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testallowArraySyntax(){

		sun.misc.VM test=new sun.misc.VM();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.allowArraySyntax();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.misc.VM","allowArraySyntax",usedMemory);
	}

