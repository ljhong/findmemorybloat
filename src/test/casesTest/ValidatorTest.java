package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class ValidatorTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testvalidate(){

		sun.security.validator.Validator test=new sun.security.validator.Validator();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.validate(null,null,null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.security.validator.Validator","validate",usedMemory);
	}

	@Test
	public void testvalidate267(){

		sun.security.validator.Validator test=new sun.security.validator.Validator();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.validate(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.security.validator.Validator","validate267",usedMemory);
	}

