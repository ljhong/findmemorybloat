package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class Vector$1Test {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testnextElement(){

		java.util.Vector$1 test=new java.util.Vector$1();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.nextElement();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.Vector$1","nextElement",usedMemory);
	}

