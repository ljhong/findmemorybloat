package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class VectorTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testclone(){

		java.util.Vector test=new java.util.Vector();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.clone();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.Vector","clone",usedMemory);
	}

	@Test
	public void testset32(){

		java.util.Vector test=new java.util.Vector();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.set(MemoryTester.LARGE_INT,null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.Vector","set32",usedMemory);
	}

	@Test
	public void testget37(){

		java.util.Vector test=new java.util.Vector();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			test.get(MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.Vector","get37",usedMemory);
	}

	@Test
	public void testadd65(){

		java.util.Vector test=new java.util.Vector();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.add(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.Vector","add65",usedMemory);
	}

	@Test
	public void testtoArray(){

		java.util.Vector test=new java.util.Vector();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.toArray();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.Vector","toArray",usedMemory);
	}

	@Test
	public void testelementAt(){

		java.util.Vector test=new java.util.Vector();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.elementAt(MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.Vector","elementAt",usedMemory);
	}

	@Test
	public void testaddElement(){

		java.util.Vector test=new java.util.Vector();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.addElement(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.Vector","addElement",usedMemory);
	}

	@Test
	public void testremoveElementAt(){

		java.util.Vector test=new java.util.Vector();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.removeElementAt(MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.Vector","removeElementAt",usedMemory);
	}

	@Test
	public void testelements(){

		java.util.Vector test=new java.util.Vector();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.elements();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.Vector","elements",usedMemory);
	}

	@Test
	public void testremove176(){

		java.util.Vector test=new java.util.Vector();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.remove(MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.Vector","remove176",usedMemory);
	}

	@Test
	public void testcopyInto(){

		java.util.Vector test=new java.util.Vector();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.copyInto(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.Vector","copyInto",usedMemory);
	}

	@Test
	public void testtoArray289(){

		java.util.Vector test=new java.util.Vector();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.toArray(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.Vector","toArray289",usedMemory);
	}

