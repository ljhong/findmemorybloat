package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class WeakHashMapLeakCase2Test {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testter(){

		test.cases.WeakHashMapLeakCase2 test=new test.cases.WeakHashMapLeakCase2();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.ter(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("test.cases.WeakHashMapLeakCase2","ter",usedMemory);
	}

}