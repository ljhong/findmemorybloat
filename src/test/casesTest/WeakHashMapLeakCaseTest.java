package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class WeakHashMapLeakCaseTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testfoo(){

		test.cases.WeakHashMapLeakCase test=new test.cases.WeakHashMapLeakCase();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.foo(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("test.cases.WeakHashMapLeakCase","foo",usedMemory);
	}

}	@Test
	public void testfoo(){

		test.cases.WeakHashMapLeakCase test=new test.cases.WeakHashMapLeakCase();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.foo(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("test.cases.WeakHashMapLeakCase","foo",usedMemory);
	}

}