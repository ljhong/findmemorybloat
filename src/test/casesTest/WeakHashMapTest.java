package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class WeakHashMapTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testput290(){

		java.util.WeakHashMap test=new java.util.WeakHashMap();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.put(null,null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.WeakHashMap","put290",usedMemory);
	}

	@Test
	public void testget291(){

		java.util.WeakHashMap test=new java.util.WeakHashMap();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			test.get(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.WeakHashMap","get291",usedMemory);
	}

	@Test
	public void testput(){

		java.util.WeakHashMap test=new java.util.WeakHashMap();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.put(null,null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.WeakHashMap","put",usedMemory);
	}

}	@Test
	public void testput(){

		java.util.WeakHashMap test=new java.util.WeakHashMap();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.put(null,null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.WeakHashMap","put",usedMemory);
	}

}	@Test
	public void testput(){

		java.util.WeakHashMap test=new java.util.WeakHashMap();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.put(null,null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.WeakHashMap","put",usedMemory);
	}

}