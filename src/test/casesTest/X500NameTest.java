package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class X500NameTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testgetRFC2253CanonicalName(){

		sun.security.x509.X500Name test=new sun.security.x509.X500Name();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			test.getRFC2253CanonicalName();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.security.x509.X500Name","getRFC2253CanonicalName",usedMemory);
	}

	@Test
	public void testhashCode112(){

		sun.security.x509.X500Name test=new sun.security.x509.X500Name();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.hashCode();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.security.x509.X500Name","hashCode112",usedMemory);
	}

	@Test
	public void testequals116(){

		sun.security.x509.X500Name test=new sun.security.x509.X500Name();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.equals(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.security.x509.X500Name","equals116",usedMemory);
	}

	@Test
	public void testconstrains(){

		sun.security.x509.X500Name test=new sun.security.x509.X500Name();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.constrains(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.security.x509.X500Name","constrains",usedMemory);
	}

