package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class X509CRLEntryTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testequals(){

		java.security.cert.X509CRLEntry test=new java.security.cert.X509CRLEntry();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.equals(null);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.security.cert.X509CRLEntry","equals",usedMemory);
	}

