package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class ZipFile$1Test {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testavailable62(){

		java.util.zip.ZipFile$1 test=new java.util.zip.ZipFile$1();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.available();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.zip.ZipFile$1","available62",usedMemory);
	}

	@Test
	public void testclose68(){

		java.util.zip.ZipFile$1 test=new java.util.zip.ZipFile$1();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.close();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.zip.ZipFile$1","close68",usedMemory);
	}

