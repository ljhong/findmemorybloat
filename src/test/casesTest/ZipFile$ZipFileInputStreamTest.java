package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class ZipFile$ZipFileInputStreamTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testclose64(){

		java.util.zip.ZipFile$ZipFileInputStream test=new java.util.zip.ZipFile$ZipFileInputStream();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.close();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.zip.ZipFile$ZipFileInputStream","close64",usedMemory);
	}

	@Test
	public void testread72(){

		java.util.zip.ZipFile$ZipFileInputStream test=new java.util.zip.ZipFile$ZipFileInputStream();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.read(null,MemoryTester.LARGE_INT,MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.zip.ZipFile$ZipFileInputStream","read72",usedMemory);
	}

	@Test
	public void testread167(){

		java.util.zip.ZipFile$ZipFileInputStream test=new java.util.zip.ZipFile$ZipFileInputStream();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.read();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.zip.ZipFile$ZipFileInputStream","read167",usedMemory);
	}

