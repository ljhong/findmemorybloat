package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class ZipInputStreamTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testavailable(){

		java.util.zip.ZipInputStream test=new java.util.zip.ZipInputStream();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.available();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.zip.ZipInputStream","available",usedMemory);
	}

	@Test
	public void testclose67(){

		java.util.zip.ZipInputStream test=new java.util.zip.ZipInputStream();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.close();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.zip.ZipInputStream","close67",usedMemory);
	}

	@Test
	public void testread77(){

		java.util.zip.ZipInputStream test=new java.util.zip.ZipInputStream();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		test.read(null,MemoryTester.LARGE_INT,MemoryTester.LARGE_INT);
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("java.util.zip.ZipInputStream","read77",usedMemory);
	}

