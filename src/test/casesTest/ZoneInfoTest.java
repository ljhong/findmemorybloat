package test.casesTest;

import jqian.testing.MemoryTester;

import org.junit.Before;
import org.junit.Test;

public class ZoneInfoTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testgetLastRuleInstance(){

		sun.util.calendar.ZoneInfo test=new sun.util.calendar.ZoneInfo();

		long freeMemoryBefore=MemoryTester.getUsedMemory();
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++)
			test.getLastRuleInstance();
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;

		MemoryTester.assertMemoryConsumation("sun.util.calendar.ZoneInfo","getLastRuleInstance",usedMemory);
	}

