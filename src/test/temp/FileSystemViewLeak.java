package test.temp;

import javax.swing.filechooser.FileSystemView;

import jqian.testing.MemoryTester;

public class FileSystemViewLeak {
	public static void main(String [] args){
		 long freeMemoryBefore=MemoryTester.getUsedMemory();
		
		for(int i=0;i<MemoryTester.LARGE_LOOPNUM;i++){
			FileSystemView.getFileSystemView();
		}
		
		long freeMemoryAfter=MemoryTester.getUsedMemory();
		long usedMemory=freeMemoryBefore-freeMemoryAfter;
		MemoryTester.assertMemoryConsumation("javax.swing.filechooser.FileSystemView","getFileSystemView",usedMemory);
	}

}
