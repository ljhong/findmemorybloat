package test.temp;

import java.io.*;
import java.lang.ref.WeakReference;

import jqian.testing.MemoryTester;


class Data implements Serializable{
    int d=0;
}

public class ObjectOutputStreamLeak {
    
    public ObjectOutputStreamLeak() {
    }
    
    public static void main(String[] args) {
        ObjectOutputStream out=null;
        try {
            out = new java.io.ObjectOutputStream(
                    new java.io.FileOutputStream("leakexample"));
            
            long freeMemoryBefore=MemoryTester.getUsedMemory();
            
            Data d  = new Data();
                WeakReference<Data> ref = new WeakReference<Data>(d);
                              out.writeUnshared(d);
               d = null;
                if(ref.get()!=null){
                 	System.out.println("leak happened");
                }
            
            long freeMemoryAfter=MemoryTester.getUsedMemory();
    		long usedMemory=freeMemoryBefore-freeMemoryAfter;
    		MemoryTester.assertMemoryConsumation("test.cases.Case1","getObjects1",usedMemory);
            
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        
    }
}
