package test.temp;
import java.util.ArrayList;
import java.util.List;

import jqian.testing.MemoryTester;

public class SubStringLeakCase {

	    private static final int NUMBER_OF_BUFFERS = 15;
	    
	    private static final int BUFFER_SIZE = 256 * 1024;

	    private static final String DUMMY_TEXT = "Hello world! This is a memory leak!";
	    
	    private String longString;
	    
	    private int size;
	    
	    /** Creates a new instance of MemoryLeak */
	    public SubStringLeakCase() {
	        StringBuffer sb = new StringBuffer(BUFFER_SIZE);
	        int count = (BUFFER_SIZE / DUMMY_TEXT.length());
	        for (int i = 0; i < count; i++) {
	            sb.append(DUMMY_TEXT);
	            this.size += DUMMY_TEXT.length();
	        }
	        this.longString = sb.toString();
	    }
	    
	    public String getSubString() {
	        double rand1 = Math.random();
	        int begin = (int)Math.round((this.size - 10) * rand1);
	        int end = begin + 8;
	        return this.longString.substring(begin, end);
	    }
	    
	    public static void main(String[] args) {
	        List subStrings = new ArrayList(NUMBER_OF_BUFFERS);
	        long freeMemoryBefore=MemoryTester.getUsedMemory();
	        for (int i = 0; i < NUMBER_OF_BUFFERS; i++) {
	        	
	        	SubStringLeakCase leak = new SubStringLeakCase();
	            String subString = leak.getSubString();
	            
	        }
	        
	        long freeMemoryAfter=MemoryTester.getUsedMemory();
    		long usedMemory=freeMemoryBefore-freeMemoryAfter;
    		MemoryTester.assertMemoryConsumation("test.cases.Case1","getObjects1",usedMemory);

	    }
	    

	}

